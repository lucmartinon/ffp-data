# FFP Data - Tech doc

## Overview

overview of the data process:
![data_flow](data_flow.jpg)
This project is about normalising and merging data from different sources, that we call here dataset. Each dataset has
an ID (int) and a name. The data is stored in the `data` folder as `data/[folder]/[dataset_id]__[dataset_name].csv`,
with 2
underscore between the id and name.
Each dataset has a normaliser (in `src/normalise/dataset_normalisers`) and can have an extractor (
in `src/extract/extractors`).

### Extract

processes the data until the result is one clean CSV (meaning: 1 line of header, one information per column, etc.)
Sometimes there is a source datafile in the data/raw folder, sometimes the extract step consists in scrapping a website
or using an API to get the data. In case the source file is already a readable CSV,
then there is no need to create an extractor for it, you can simply call `extract [dataset_id]`, the generic extractor
will be used.

### Normalise

Each dataset has a Normaliser saved in `src/normalise/dataset_normalisers`.
In this class you will find a description of the columns from the source dataset, the static values that should be
applied to all lines, the method that will be used to parse the values to numeric, the id of the EPSG id of the
projection
that is used in the source file, etc. It can be seen as source file descriptor.

The idea is that the normaliser contains the parameters (the description of which column for example), but that the code
happens as much as possible in the abstract classes `Normaliser`, `SLNormaliser` and `MLNormaliser`.

There are 2 types of normalisers because there are 2 types of source files, that needs to be processed quite
differently.

#### Single line normalisers

They correspond to datasets in which each sampling is represented by one line. The PFAS concentrations are stored in
different columns, one per substance.
Typical example:

|    date    	|     matrix    	|    lat    	|     lon    	|             name             	| pfbs 	| pfhps 	| pfhxs 	| pfos 	|
|:----------:	|:-------------:	|:---------:	|:----------:	|:----------------------------:	|:----:	|:-----:	|:-----:	|:----:	|
| 14/02/2022 	| Surface water 	| 54,116628 	| -2,5110948 	| Fire marshal training ground 	|  1,2 	|   1,9 	|  25,5 	| 36,1 	|

#### Multiple lines normalisers

They correspond to datasets in which each sampling is represented by many lines. The PFAS concentrations are all in the
same column, with another column indicating the substance that is measured.

Typical example

|  date    	   |     matrix    	|    lat    	|     lon    	|             name             	| param 	| value 	|
|:------------:|:-------------:	|:---------:	|:----------:	|:----------------------------:	|:-----:	|:-----:	|
| 14/02/2022 	 | Surface water 	| 54,116628 	| -2,5110948 	| Fire marshal training ground 	|  pfbs 	|   1,2 	|
| 14/02/2022 	 | Surface water 	| 54,116628 	| -2,5110948 	| Fire marshal training ground 	| pfhps 	|   1,9 	|
| 14/02/2022 	 | Surface water 	| 54,116628 	| -2,5110948 	| Fire marshal training ground 	| pfhxs 	|  25,5 	|
| 14/02/2022 	 | Surface water 	| 54,116628 	| -2,5110948 	| Fire marshal training ground 	|  pfos 	|  36,1 	|

## Usage

### Install

Clone the repository,

```commandline
git clone https://gitlab.com/lucmartinon/ffp-data
cd ffp-data
```

Create virtual environment and activate it.

```commandline
virtualenv venv --python=python3
source venv/bin/activate
```

Install Python dependencies

```commandline
pip3 install -r requirements.txt
```

### Adding a new dataset

#### extract

* If there is a source file,
    * copy the source file to `data/raw`
    * Rename it so that there is a new id in the name, like `128__my_new_dataset.csv`
* If the source file is simple (csv with one line of header, excel file with one line of header, with the data in the
  first tab):
    * you can directly call `python main.py extract 128`. It will create a file with id 128 in `data/readable`
* If the source file is messy or if there is no source file (API or scrapping):
    * create the extractor: `python main.py create_extractor 128`
    * edit the file newly created `src/exract/extractors/128.py` (I find it usually nicer to write the extract code
      first in a jupyter notebook)
    * run the extraction: `python main.py extract 128`

#### normalise

Call `python main.py create_normaliser 128`
This will start a process in which the program will try to guess the types of the columns, and create the normaliser.
This process is interactive: the program will make guesses and ask the user to confirm them, or will ask the user when
it cannot guess. The two main tasks are:

**Identifying the columns**
This is done by a few guesses and by a dictionary of synonyms, stored in `data/settings/column_synonyms.csv`.
Single-line normalisers have multiple value columns (one per substance). In the normaliser they will all be marked as
type `SL_VALUE`
On the contrary, Multiple-line normalisers have only one value column marked as `ML_VALUE`.

**Identifying the different substances**
The substances names as they appear in the raw datasets are listed in `data/settings/substance_synonyms.csv`.

Once the normaliser is generated, you need to edit it to implement the `parse_value` function.
It's hopefully the only bit of code that needs to be done for each dataset.

Then you can normalise by calling `python main.py normalise 128`

### Run tests

Run quality checks on your normalised data by calling `python main.py check 128`

### Merge

Call `python main.py normalise merge`
This combines all normalised datasets into a single file (`public/full.parquet` and `public/full.csv`).
It also generates the static page with stats in the public folder, as well as the map data (which is optimised to be
lighter). You can immediately see your results by opening the static page `public/map.html` in your browser.
Check:

* that the points are where they are supposed to
* that the units are correct.
* for ML datasets: that the points have different values. If not, it meanst we kept one column that is unique per
  substance tested.

Once you're happy with your dataset, commit & push it to the project!

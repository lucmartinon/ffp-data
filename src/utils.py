import glob
import logging
import os
import time

import pandas as pd
import requests
from tqdm import tqdm

from src.constants.csv import RAW
from src.constants.exceptions import FppDataException

country_codes = {
    "AT": "Austria",
    "BE": "Belgium",
    "BG": "Bulgaria",
    "HR": "Croatia",
    "CY": "Cyprus",
    "CZ": "Czechia",
    "DK": "Denmark",
    "EE": "Estonia",
    "FI": "Finland",
    "FR": "France",
    "DE": "Germany",
    "GR": "Greece",
    "HU": "Hungary",
    "IE": "Ireland",
    "IT": "Italy",
    "LV": "Latvia",
    "LT": "Lithuania",
    "LU": "Luxembourg",
    "MT": "Malta",
    "NL": "Netherlands",
    "PO": "Poland",
    "PL": "Poland",
    "PT": "Portugal",
    "RO": "Romania",
    "SK": "Slovakia",
    "SI": "Slovenia",
    "ES": "Spain",
    "SE": "Sweden",
    "GB": "United Kingdom",
    "CH": "Switzerland",
    "NO": "Norway",
    "IS": "Iceland",
    "LI": "Liechtenstein",
}


def get_df(folder, dataset_id, header="infer"):
    fp = get_dataset_fp(folder, dataset_id)
    return pd.read_csv(fp, header=header)


def get_dataset_name(dataset_id):
    raw_fp = get_dataset_fp(RAW, dataset_id)
    return os.path.basename(raw_fp).split(".")[0].split('__')[1]


def get_dataset_fp(folder, dataset_id):
    pattern = f"{folder}/{dataset_id}__*"
    try:
        return glob.glob(pattern)[0]
    except IndexError:
        raise FppDataException(f"No file found in {folder} with id {dataset_id}")


def get_dataset_fn(folder, dataset_id):
    fp = get_dataset_fp(folder, dataset_id)
    return os.path.basename(fp)


def get_folder_ids(folder):
    pattern = f"{folder}/*__*"
    fps = sorted(glob.glob(pattern))
    return [os.path.basename(fp).split("__")[0] for fp in fps]


def download_file(url, fp):
    logging.info(f"Download file at url {url} to '{fp}'")

    with requests.get(url, stream=True) as r:
        r.raise_for_status()
        total_size_in_bytes = int(r.headers.get("content-length", 0))
        progress_bar = tqdm(total=total_size_in_bytes, unit="B", unit_scale=True)

        with open(fp, "wb") as f:
            for chunk in r.iter_content(1024):
                progress_bar.update(len(chunk))
                f.write(chunk)
        progress_bar.close()


def get_country_name(country_code):
    if country_code in country_codes:
        return country_codes[country_code]
    else:
        logging.error(f"Missing country with country_code: {country_code}")


def frame_it_with_errors(function):
    def framed(*args, **kwargs):
        start_time = time.time()
        errors = list(function(*args, **kwargs))
        end_time = time.time()
        msg = f"{function.__name__} took {end_time - start_time:.2f} seconds"
        logging.info(f"╔{'':═^100}╗")
        logging.info(f"║{msg:^100}║ ")
        for error in errors:
            logging.info(f"║{error:^100}║ ")
        logging.info(f"╚{'':═^100}╝")
        return errors

    return framed

import pandas as pd

from src.extract.extract import Extractor


class Extractor_57(Extractor):
    def _extract(self):
        df = self.get_raw_df()
        # there is a single column with values in ug/L, converting it, there is a column for the same substance in ng/L
        ng_col = "cC6O4 (CAS 1190931-27-1) (ng/L) [nanog./L] cC6O4"
        ug_col = "cC6O4 (CAS 1190931-27-1) (µg/L) [microg./L] cC6O4"
        df[ng_col] = df.apply(
            lambda r: r[ng_col]
            if not pd.isna(r[ng_col])
            else r[ug_col] * 1000
            if not pd.isna(r[ug_col])
            else None,
            axis=1,
        )
        df = df.drop(columns=[ug_col])
        df["Latitudine"] = df["Latitudine"].str.replace(" ", "")
        df["Longitudine"] = df["Longitudine"].str.replace(" ", "")

        self._save_readable_df(df)

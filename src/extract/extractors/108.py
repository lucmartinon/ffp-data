from src.extract.extract import Extractor


class Extractor_108(Extractor):

    def _extract(self):
        df = self.get_raw_df()
        df.columns = df.iloc[0]
        df = df.iloc[1:]
        df = df.transpose().reset_index()
        df.columns = df.iloc[0]
        df = df.iloc[1:]
        df.columns = ['name'] + list(df.columns[1:])
        # we get lat /lon from both WWTP from a google search, since we have the names of the WWTP
        # Psyttaleia: 37.94035 | 23.58797
        # Mytilini:   39.12931 | 26.54536
        df['lat'] = df['name'].apply(lambda name: 37.94035 if 'Psyttaleia' in name else 39.12931)
        df['lon'] = df['name'].apply(lambda name: 23.58797 if 'Psyttaleia' in name else 26.54536)
        df['matrix'] = df['name'].apply(lambda name: 'Waste water' if 'Influent' in name else 'Surface water')
        df = df.replace({'-': None})

        self._save_readable_df(df)

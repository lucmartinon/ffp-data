from src.extract.extract import Extractor


class Extractor_62(Extractor):
    def _extract(self):
        df = self.get_raw_df()
        # there is a weird line with duplicated headers
        df = df.drop([0])
        # All value columns are like "PFBA@LCMSMS@ng/L", we replace it by only the first part
        df.columns = [c.split("@")[0] for c in df.columns]
        self._save_readable_df(df)

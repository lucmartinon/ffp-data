from src.extract.extract import Extractor


class Extractor_111(Extractor):
    def _extract(self):
        df = self.get_raw_df()
        df.columns = df.iloc[0]
        df = df[1:]

        self._save_readable_df(df)

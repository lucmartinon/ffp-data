import pandas as pd

from src.extract.extract import Extractor


class Extractor_84(Extractor):
    def _extract(self):
        data = pd.read_excel(
            self.raw_fp, sheet_name="DADOS_PFOS_AGUA_SUPERF", skiprows=2
        )
        # deleting useless rows at beginning and end
        data = data.drop([0, 59, 60, 61, 62])
        stations = pd.read_excel(self.raw_fp, sheet_name="ESTAÇÕES")
        records = []

        station_col = None
        for c in data.columns:
            if c.startswith("Unnamed"):
                print(f"merging {c} to {station_col}")
                data[station_col] = data[c] + data[station_col].astype(str)
                data = data.drop(columns=[c])
            else:
                station_col = c
        for index, row in data.iterrows():
            for c in data.columns:
                if c != "DATA":
                    if not pd.isna(row[c]):
                        records.append(
                            {"station": c, "pfos": row[c], "date": row["DATA"]}
                        )
        recs = pd.DataFrame(records)
        df = pd.merge(
            left=recs,
            right=stations,
            how="left",
            right_on="ESTACÃO ÁGUA SUPERFICIAL",
            left_on="station",
        )
        df = df[~pd.isna(df["LATITUDE"])]

        self._save_readable_df(df)

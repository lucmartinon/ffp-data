import pandas as pd
from shapely import wkt

from src.constants.columns import *
from src.extract.extract import Extractor


class Extractor_9(Extractor):
    def _extract(self):
        url = "http://www.dov.vlaanderen.be/geoserver/pfas/wfs?request=GetFeature&service=WFS&version=1.1.0&typeName=pfas_vbo&outputFormat=csv"
        df = pd.read_csv(url)
        # calculating the centroid of each polygon
        df[COORD_X] = df.apply(get_x, axis=1)
        df[COORD_Y] = df.apply(get_y, axis=1)

        self._save_readable_df(df)


def get_center(polygon_str):
    p = wkt.loads(polygon_str)
    return p.centroid


def get_x(row):
    return get_center(row["geom"]).x


def get_y(row):
    return get_center(row["geom"]).y

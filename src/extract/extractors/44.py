import os

import pandas as pd

from src.extract.extract import Extractor
from src.utils import download_file


class Extractor_44(Extractor):

    def _extract(self):
        # if this url stops working, go to https://hcdc.hereon.de/campaign_db/#/download
        # then take all values for the first filters and only Per- and polyfluoroalkyl substances in the last filter
        url = "https://hcdc.hereon.de/geoserver/coastmap/ows?service=WFS&version=1.0.0&request=GetFeature&TypeName=coastmap:DISCOVER_DBDATA_PUBLIC&outputFormat=csv&cql_filter=(PARAMETERSITE_NAME=%27Atmosphere%27%20or%20PARAMETERSITE_NAME=%27Water%27%20or%20PARAMETERSITE_NAME=%27SPM%27%20or%20PARAMETERSITE_NAME=%27Sediment%27%20or%20PARAMETERSITE_NAME=%27Porewater%27)%20and%20(PARAMETERGROUP_NAME=%27Particulate%20contaminants%27%20or%20PARAMETERGROUP_NAME=%27Organic%20contaminants%27)%20and%20(PARAMETERSUBGROUP_NAME=%27Per-%20and%20polyfluoroalkyl%20substances%27)&maxFeatures=50000000"
        fp = os.path.join(self.raw_fp, 'tmp.csv')
        download_file(url, fp)

        cols = [
            "DATAVALUE",
            "PARAMETER",
            "UNITNAME",
            "LAT",
            "LON",
            "CAMPAIGN",
            "DATA_DATETIME",
            "DOI",
            "ORIGINAL_DATA",
            "QUALITY_INFO",
            "MEASUREMENT_DEPTH",
            "PARAMETERSITE_NAME",
            "STATIONID",
        ]

        df = pd.read_csv(fp, usecols=cols)

        df = df[df["PARAMETER"].str.contains("CAS: ")]
        df = df[df["QUALITY_INFO"].str.contains("SeaDataNet: 1")]
        df = df.drop(columns=["QUALITY_INFO"])

        self._save_readable_df(df)
        self._clean_dir()

from src.extract.extract import Extractor


class Extractor_36(Extractor):

    def _extract(self):
        df = self.get_raw_df()
        for c in df.columns:
            if c.endswith('_µg_kg'):
                df = df.rename(columns={c: c[:-6]})

        self._save_readable_df(df)

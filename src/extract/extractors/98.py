from src.extract.extract import Extractor


class Extractor_98(Extractor):
    def _extract(self):
        df = self.get_raw_df()
        # 98: watershed own testing campaign - the format is messy

        # we drop all pre ox values (3 - 24), then total lines (43 - 47)
        df = df.drop(list(range(3, 25)))
        df = df.drop(list(range(43, 48)))
        df = df.drop(columns="LOD/Units")
        df["Component"] = df["Component"].str.replace(" (Post Ox.)", "")

        # we transpose and name columns from the first line
        df = df.transpose().reset_index()
        df.columns = df.iloc[0]
        df = df[1:]

        # some lines have only the first column non empty, we remove them
        df = df.dropna(how="all", subset=[c for c in df.columns if c != "Component"])
        df

        # line 11 has 53 52'53"N  -59'-0.7"W instead of lat lon, replacing it manually
        df.loc[11, "Latitude"] = 53.881389
        df.loc[11, "Longtitude"] = -1.016472

        # some line have city names in longitude

        def is_number(s):
            try:
                float(s)
                return True
            except ValueError:
                return False

        df["City"] = df["Longtitude"].apply(lambda v: v if not is_number(v) else None)
        df["Longtitude"] = df["Longtitude"].apply(lambda v: v if is_number(v) else None)
        df["Latitude"] = df["Latitude"].apply(lambda v: v if is_number(v) else None)

        df = df.dropna(how="all", subset=["Latitude", "City"])
        self._save_readable_df(df)

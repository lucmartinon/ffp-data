import re

import pandas as pd

from src.constants.columns import *
from src.constants.csv import RAW
from src.extract.extract import Extractor
from src.utils import get_df


class Extractor_11(Extractor):
    def _extract(self):
        raw_df = get_df(RAW, 11, header=None)

        # first we duplicate the values on the first line if empty
        last_matrix = ""
        col = 0
        for v in raw_df.iloc[0]:
            if pd.isna(v):
                raw_df.loc[0, col] = last_matrix
            else:
                last_matrix = v
            col += 1

        # we replace compound name by the cas id if it's a single value
        raw_df[1] = raw_df.apply(
            lambda r: r[1] if "|" in str(r[3]) else str(r[3])[8:], axis=1
        )

        # then we keep only the column "Compound name" and drop the other compound descriptors
        to_drop = [0, 2, 3, 4, 5, 6]
        raw_df = raw_df.drop(raw_df.columns[to_drop], axis=1)

        raw_df.at[raw_df.index[-1], 1] = "pfas_sum"

        # et hop
        raw_df = raw_df.transpose()
        raw_df.at[1, 0] = "site_type"
        raw_df.at[1, 1] = "name"
        raw_df.at[1, 2] = "country"
        raw_df.at[1, 3] = "lat"
        raw_df.at[1, 4] = "lon"

        raw_df.reset_index(inplace=True, drop=True)
        raw_df.columns = raw_df.iloc[0]

        # Remove first row
        raw_df = raw_df[1:]

        def parse_name(long_string):
            # example value
            # UoA_LC-ESI-QTOF_Groundwater from Samorin Kalinkovo_Samorin_Slovakia_18.07.2019_JDS4_45599.xlsx
            # all string start with UoA_LC-ESI-QTOF_ and end with _JDS4_45599.xlsx so we remove it
            long_string = long_string[16:-16]
            if "from" in long_string:
                matrix = long_string.split(" from ")[0]
                geo_date = long_string.split(" from ")[1]
            else:
                # example without from
                # River water 18 Gonyu_Komarno_Slovakia_04.07.2019
                p = re.compile(r"(.*) \d\d (.*)")
                res = p.match(long_string)
                if res:
                    matrix = res.groups()[0]
                    geo_date = res.groups()[1]
                else:
                    matrix = "Sediment"
                    geo_date = "17.07.2019"

            date = geo_date[-10:]
            name = geo_date[:-11]
            return matrix, date, name

        # we dont need the matrix we have it already
        raw_df[DATE] = raw_df[NAME].apply(lambda x: parse_name(x)[1])
        raw_df[NAME] = raw_df[NAME].apply(lambda x: parse_name(x)[2])

        self._save_readable_df(raw_df)

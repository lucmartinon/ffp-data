import os

import pandas as pd

from src.extract.extract import Extractor


class Extractor_55(Extractor):
    def _extract(self):
        df_data = pd.read_csv(os.path.join(self.raw_fp, "DATA.csv"))
        df_stations = pd.read_csv(os.path.join(self.raw_fp, "STATIONS.csv"))
        df = pd.merge(
            how="left",
            left=df_data,
            right=df_stations,
            left_on="CODICE SIRAV PUNTO PRELIEVO",
            right_on="code",
        )

        df = df[
            df["TIPO DI ANALISI"].isin(
                [
                    "PERFLUORO_LC_MS",
                    "PERFLUORO_LCMS_NG_L_HFPO - DA",
                    "PERFLUORO_LCMS_NG_L_FTS",
                    "PFAS_CC6O4_FORMA_ACIDA_NG_L",
                    "2 - 4 - 5 - TFPAA",
                ]
            )
        ]
        self._save_readable_df(df)

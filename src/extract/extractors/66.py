import duckdb

from src.extract.extract import Extractor


class Extractor_66(Extractor):
    def _extract(self):
        con = duckdb.connect()
        con.sql("INSTALL spatial;LOAD spatial;")
        con.sql(
            f"create table stations as (SELECT * FROM st_read('{self.raw_fp}', layer = 'COORDINATES', open_options=['HEADERS=FORCE', 'FIELD_TYPES=AUTO']));"
        )
        con.sql(
            f"create table data as (SELECT * FROM st_read('{self.raw_fp}', layer = 'DATA', open_options=['HEADERS=FORCE', 'FIELD_TYPES=AUTO']));"
        )

        con.sql(
            f""" copy (
        select COLUMNS(* EXCLUDE (Field26,Field25, Field11,Field12,Field13,Field14,Field15,Field16,Field17,Field18,Field19,Field20,Field21,Field22,Field23,Field24))
        from data
            left join stations using( "station code" )
            ) to '{self.readable_csv_fp}'
        """
        )

import os

import pandas as pd

from src.extract.extract import Extractor
from src.utils import download_file


class Extractor_128(Extractor):

    def _extract(self):
        url = 'https://data.bs.ch/api/explore/v2.1/catalog/datasets/100066/exports/csv?lang=en&refine=gruppe%3A%22Perfluorierte%20Tenside%22'

        fp = os.path.join(self.raw_fp, '100066.csv')
        if not os.path.exists(fp):
            download_file(url, fp)
        df = pd.read_csv(fp, sep=';')

        df['lat'] = df['geo_point_2d'].str.split(', ', expand=True)[0]
        df['lon'] = df['geo_point_2d'].str.split(', ', expand=True)[1]
        df = df[~pd.isna(df['cas_bezeichnung'])]

        df = df.drop(columns=[
            'probentyp',
            'geo_point_2d', 'x_coord', 'y_coord',
            'probenahmedauer', 'reihenfolge',
            'bg', 'wert_num', 'auftragnr',
            'probennr', 'resultatnummer', 'automatische_auswertung',
            'parameter', 'allgemeine_parametergruppe',

        ])

        self._save_readable_df(df)
        self._clean_dir()

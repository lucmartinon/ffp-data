import json

import pandas as pd
import requests

from src.extract.extract import Extractor


class Extractor_7(Extractor):
    @property
    def dataset_name(self):
        return "Norway_municipal_fire_training_sites"

    def _extract(self):
        url = "https://www.arcgis.com/sharing/rest/content/items/204b4728685942b293880364b58a2ec2/data?f=json"
        data = json.loads(requests.get(url).text)
        features = data["operationalLayers"][0]["featureCollection"]["layers"][0][
            "featureSet"
        ]["features"]
        records = [f["attributes"] for f in features]
        df = pd.DataFrame(records)
        self._save_readable_df(df)

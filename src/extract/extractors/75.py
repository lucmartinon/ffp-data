import pandas as pd

from src.extract.extract import Extractor


class Extractor_75(Extractor):
    def _extract(self):
        df = pd.read_csv(self.raw_fp, encoding="ISO-8859-1", sep=";")

        self._save_readable_df(df)

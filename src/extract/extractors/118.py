import pandas as pd

from src.extract.extract import Extractor


class Extractor_118(Extractor):
    def _extract(self):
        dfs = []
        pfas_list = ["PFOA", "PFOS", "PFHxA", "PFHpA", "PFHxS", "PFDA", "PFNA", "PFPeA"]
        for pfas in pfas_list:
            df = pd.read_excel(self.raw_fp, sheet_name=pfas)
            df.columns = df.iloc[0]
            df = df[1:]
            df["param"] = pfas
            dfs.append(df)

        sites = pd.read_excel(self.raw_fp, sheet_name="Sites")

        df = pd.merge(
            how="left",
            left=pd.concat(dfs),
            right=sites,
            left_on="no_code_site",
            right_on="no_code-station",
        )

        self._save_readable_df(df)

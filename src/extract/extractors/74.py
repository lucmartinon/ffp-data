from src.extract.extract import Extractor


class Extractor_74(Extractor):
    def _extract(self):
        df = self.get_raw_df()
        df["Est"] = df["Est"].str.replace(",", ".")
        df["Nord"] = df["Nord"].str.replace(",", ".")

        self._save_readable_df(df)

import pandas as pd

from src.extract.extract import Extractor


class Extractor_110(Extractor):
    def _extract(self):
        df = self.get_raw_df()
        df["species"] = df["species"].ffill(axis=0)
        df["povodí"] = df["povodí"].ffill(axis=0)
        df["Locality"] = df["Locality"].ffill(axis=0)
        df_loc = df[["Unnamed: 9", "Unnamed: 10"]]
        df_loc = df_loc.dropna()
        loc_map = {}
        for rec in df_loc.to_records():
            loc_map[rec["Unnamed: 9"]] = rec["Unnamed: 10"]
        df["Locality"] = df["Locality"].map(loc_map)
        df = df.drop(columns=["Unnamed: 9", "Unnamed: 10", "Suma"])
        df = df[~pd.to_numeric(df["sample N°"], errors="coerce").isnull()]

        self._save_readable_df(df)

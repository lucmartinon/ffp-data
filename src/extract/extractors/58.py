import pandas as pd

from src.extract.extract import Extractor


class Extractor_58(Extractor):
    def _extract(self):
        df = pd.read_excel(self.raw_fp, sheet_name="DATI_2021_CI_FLUVIALI", skiprows=9)
        self._save_readable_df(df)

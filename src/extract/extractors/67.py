import pandas as pd

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.extract.extract import Extractor


class Extractor_67(Extractor):
    def _extract(self):
        df_sw = pd.read_excel(self.raw_fp, sheet_name="Acque_Superficiali")
        df_gw = pd.read_excel(self.raw_fp, sheet_name="Acque_Sotterranee")
        df_sw[MATRIX] = Matrix.SURFACE_WATER
        df_gw[MATRIX] = Matrix.GROUNDWATER

        df = pd.concat([df_gw, df_sw])

        self._save_readable_df(df)

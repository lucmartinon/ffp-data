import pandas as pd

from src.extract.extract import Extractor


class Extractor_106(Extractor):

    def _extract(self):
        df = pd.read_excel(self.raw_fp, skiprows=11)
        df = df.drop(columns=['Σ alle PFAS [µg/kg]', 'Σ 8 PFAS [µg/kg]'])
        for c in df.columns:
            if df[c].eq(0).all():
                df = df.drop(columns=[c])
        self._save_readable_df(df)

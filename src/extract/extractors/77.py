import pandas as pd

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.extract.extract import Extractor


class Extractor_77(Extractor):
    def _extract(self):
        fish = pd.read_excel(self.raw_fp, sheet_name="fish_data")
        sw = pd.read_excel(self.raw_fp, sheet_name="surface_water_data")
        fish[MATRIX] = Matrix.BIOTA
        sw[MATRIX] = Matrix.SURFACE_WATER
        df = pd.concat([fish, sw])
        df["Result"] = df["Result"].astype(str)
        df["Result"] = df.apply(value, axis=1)
        self._save_readable_df(df)


def value(row):
    if row["Flag"] == 782:
        return "<" + row["Result"]
    return row["Result"]

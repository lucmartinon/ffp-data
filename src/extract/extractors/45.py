import asyncio
import json
import logging
import time

import nest_asyncio
import pandas as pd
from pyppeteer import launch

from src.extract.extract import Extractor

url = "https://sumpfas.ime.fraunhofer.de"
xpath_data_tab = "/html/body/nav/div/ul/li[5]/a"
xpath_dl_button = '//*[@id="download_DF"]'


class Extractor_45(Extractor):
    def _extract(self):

        nest_asyncio.apply()

        responses, csv_url = asyncio.get_event_loop().run_until_complete(pyppeteer_scrape())
        logging.info("Hopefully scrapping done")

        resp = None
        for r in responses:
            if "addCircleMarker" in r["response"]["payloadData"]:
                json_str = r["response"]["payloadData"][9:-2]
                json_str = json_str.replace('\\"', '"')
                resp = json.loads(json_str)
                break
        lats = resp["custom"]["leaflet-calls"]["calls"][0]["args"][0]
        lons = resp["custom"]["leaflet-calls"]["calls"][0]["args"][1]

        df = pd.read_csv(csv_url, sep=" ")
        df["lat"] = lats
        df["lon"] = lons
        for c in df.columns:
            if "dTOP" in c or c.endswith("_sum"):
                df = df.drop(columns=[c])
                logging.info(f"dropped columns {c}")

        self._save_readable_df(df)
        self._clean_dir()


async def pyppeteer_scrape():
    """
    Scrapes the fraunhofer map. It loads the first url, listens to the socket communications.
    Then it filters the responses to find the one that contains the lat and lon of the map dots
    Finally, it goes to the data tab, get the download link and downloads the csv .
    This needs to be in a separate function because of the async stuff.

    :return: a tuple with the socket responses and the csv url
    """
    responses = []
    browser = await launch(headless=True, args=["--no-sandbox"], autoClose=False)

    def printResponse(response):
        responses.append(response)

    page = await browser.newPage()
    await page.goto(url)

    cdp = await page.target.createCDPSession()
    await cdp.send("Network.enable")
    await cdp.send("Page.enable")

    cdp.on(
        "Network.webSocketFrameReceived", printResponse
    )  # Calls printResponse when a websocket is received
    cdp.on(
        "Network.webSocketFrameSent", printResponse
    )  # Calls printResponse when a websocket is sent

    [dl_tab] = await page.xpath(xpath_data_tab)
    await dl_tab.click()
    logging.info("clicked data tab")
    time.sleep(4)

    [dl_button] = await page.xpath(xpath_dl_button)
    href = await page.evaluate('(element) => element.getAttribute("href")', dl_button)
    csv_url = f"{url}/{href}"

    return responses, csv_url

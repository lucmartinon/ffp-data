import pandas as pd

from src.constants.columns import *
from src.extract.extract import Extractor


class Extractor_78(Extractor):
    def _extract(self):
        df = self.get_raw_df()
        for i in df.index:
            if pd.isna(df["Coordinates"][i]):
                df.at[i, "Coordinates"] = df.at[i - 1, "Coordinates"]

        df[[LAT, LON]] = df["Coordinates"].str.split(", ", n=1, expand=True)
        df = df.drop(columns=["Coordinates"])
        self._save_readable_df(df)

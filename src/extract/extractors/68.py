import re

import pandas as pd

from src.extract.extract import Extractor


class Extractor_68(Extractor):
    def _extract(self):
        df = pd.read_excel(self.raw_fp, sheet_name="ACQUE SUPERFICIALI")

        def rename_col(col_name):
            matches = re.findall(r"\d{2,7}-\d{2}-\d", col_name)
            if len(matches) > 0:
                cas = matches[0]
                if cas == "335-46-4":
                    # they have a typo
                    cas = "355-46-4"
                if "ramificati" in col_name:
                    return cas + " branched"
                if "lineare" in col_name:
                    return cas + " linear"
                return cas
            return col_name

        df.columns = [rename_col(c) for c in df.columns]
        self._save_readable_df(df)

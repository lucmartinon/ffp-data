import json

import pandas as pd
import requests
from tqdm import tqdm

from src.extract.extract import Extractor


class Extractor_16(Extractor):
    @property
    def dataset_name(self):
        return "Flanders DOV"

    def _extract(self):
        types = [
            "pfas:pfas_analyseresultaten",
            "pfas:lantis_bodem_metingen",
            "pfas:lantis_gw_metingen_publiek",
            "pfas:lucht_gas_metingen",
            "pfas:lucht_zwevendstof_metingen",
            "waterbodems:pfas_meetpunten_fcs",
            "pfas:pfas_oppwater",
            "pfas:pfas_biota",
        ]
        dfs = []
        for t in types:
            dfs.append(download_layer(t))
        df = pd.concat(dfs)

        df = df[df["parameter"] != ""]

        self._save_readable_df(df)


def download_layer(type):
    base_url = f"https://www.dov.vlaanderen.be/geoserver/wfs?request=GetFeature&service=WFS&version=1.1.0&typeName={type}&outputFormat=json"
    r = requests.get(base_url)
    d = json.loads(r.text)
    count = len(d["features"])
    limit = d["totalFeatures"]
    print(f"{type}: {limit} rows")

    all_features = []
    start_index = 0
    with tqdm(
            total=limit,
            position=0,
            leave=True,
            bar_format="{l_bar}{bar:60}{r_bar}{bar:-10b}",
            desc=f"Download {type}",
    ) as pbar:
        while start_index < limit:
            r = requests.get(base_url + f"&startindex={start_index}")
            d = json.loads(r.text)
            all_features.extend(d["features"])
            pbar.update(count)
            start_index += count
    records = [f["properties"] for f in all_features]
    df = pd.DataFrame(records)
    df["data_type"] = type
    df.columns = [c.lower() for c in df.columns]
    if "x" in df.columns:
        df = df.rename(columns={})

    df = df.rename(
        columns={
            "datum_bemonstering": "datum",
            "eenheid": "meeteenheid",
            "waarde": "meetwaarde",
            "x": "x_ml72",
            "y": "y_ml72",
        }
    )

    return df

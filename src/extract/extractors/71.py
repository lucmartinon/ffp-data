import pandas as pd

from src.extract.extract import Extractor


class Extractor_71(Extractor):
    def _extract(self):
        df_data = self.get_raw_df()
        df_stations = pd.read_excel(
            self.raw_fp, sheet_name="Anagrafica stat. PFAS 2021 DEF"
        )
        df = pd.merge(
            how="left",
            left=df_data,
            right=df_stations,
            left_on="Codice Stazione",
            right_on="CODICE_STAZIONE",
        )
        df = df.drop(columns=["CODICE_STAZIONE"])
        df = df[~pd.isna(df["TIPO_STAZIONE"])]

        self._save_readable_df(df)

from src.extract.extract import Extractor


class Extractor_63(Extractor):
    def _extract(self):
        df = self.get_raw_df()

        # there are 6 substances, each one with a block of 5 columns
        for i in range(6):
            val_col_index = 6 + 5 * i
            val_col = df.columns[val_col_index]
            loq_col = df.columns[val_col_index + 3]
            note_col = df.columns[val_col_index + 4]
            param = df.iloc[0, val_col_index]

            def get_value(row):
                if row[note_col] == "inf. LdQ":
                    return f"<{row[loq_col]}"
                else:
                    return row[val_col]

            df[param] = df.apply(get_value, axis=1)

        df = df.drop([0, 1])
        for c in df.columns:
            if c == "Parametro" or c.startswith("Unnamed"):
                df = df.drop(columns=[c])
        self._save_readable_df(df)

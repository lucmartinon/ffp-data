import pandas as pd

from src.extract.extract import Extractor


class Extractor_60(Extractor):
    def _extract(self):
        df = pd.read_excel(
            self.raw_fp, sheet_name="VALORI ANALITICI_ACQUE SOTT", skiprows=11
        )
        self._save_readable_df(df)

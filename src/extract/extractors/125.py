import pandas as pd

from src.extract.extract import Extractor


class Extractor_125(Extractor):
    def _extract(self):
        raw_df = pd.read_excel(self.raw_fp, skiprows=5)

        raw_df.columns = [_.replace("\n", " ") for _ in raw_df.columns]

        self._save_readable_df(raw_df)

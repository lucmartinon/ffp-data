from src.extract.extract import Extractor


class Extractor_104(Extractor):

    def _extract(self):
        df = self.get_raw_df()
        df = df.drop(0)
        df = df.drop(1)
        df = df.drop(2)
        df = df.transpose().reset_index()
        df.columns = df.loc[0]
        df = df.drop(0)
        df = df.drop(1)
        df = df.drop(2)
        df = df.drop(3)
        df = df.drop(columns=['Unnamed: 0', df.columns[-1]])
        df.columns = ['lat', 'lon', 'Depth (m)', 'PFOA', 'PFOS']
        df['lat'] = df['lat'].ffill()
        df['lon'] = df['lon'].ffill()

        self._save_readable_df(df)

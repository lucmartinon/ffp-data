import json
import os
import zipfile

import duckdb
import pandas as pd
import requests
from tqdm import tqdm

from src.constants.csv import SUBSTANCES_FP
from src.extract.extract import Extractor
from src.utils import download_file


class Extractor_28(Extractor):
    def _extract(self):
        par_url = "https://static.data.gouv.fr/resources/resultats-du-controle-sanitaire-de-leau-du-robinet/20230601-072914/eaurob-ref-20230601.zip"
        par_fp = os.path.join(self.raw_fp, "eaurob-ref-20230601.zip")
        if not os.path.exists(par_fp):
            download_file(par_url, par_fp)

        with zipfile.ZipFile(par_fp, "r") as zip_ref:
            zip_ref.extract("PAR_20230601.csv", self.raw_fp)

        with duckdb.connect() as con:
            pfas_params = con.sql(f"""
            select cdparametre , casparam
            from '{self.raw_fp}/PAR_*.csv' eaurob
            join '{SUBSTANCES_FP}' substances on substances."CAS Number" = eaurob.casparam
            where casparam is not null
            """).fetchall()
        code_params = [_[0] for _ in pfas_params]
        cas_map = {}
        for row in pfas_params:
            cas_map[row[0]] = row[1]

        bss_ids = set()
        for code in tqdm(code_params, desc='Listing stations'):
            bss_ids.update(get_bss_ids(code))

        data = []
        bss_ids = list(bss_ids)
        with tqdm(total=len(bss_ids)) as pbar:
            for bss_id in bss_ids:
                pbar.set_description(f'Getting data for station {bss_id.ljust(25, " ")}')
                data.extend(get_station_data(code_params, bss_id))
                pbar.update(1)
        print(f'Done. In total {len(data)} analysis')

        df = pd.DataFrame(data)
        df['cas'] = df['code_param'].map(cas_map)
        self._save_readable_df(df)
        self._clean_dir()


def get_bss_ids(code):
    url = 'https://ades.eaufrance.fr/Recherche/GetPointEau'
    params = {
        'type': 4,
        'filter': f"parametre%3D{code}",
        'rows': 20000,
        'start': 0
    }
    res = requests.post(url, params)
    ids = []
    for p in json.loads(res.text)['pointEaux']:
        label = p['Properties']['label']
        if ' ' in label:
            ids.append(label.split(' ')[1][1:-1])
    return set(ids)


def get_station_data(code_params, bss_id, page=1, retry=False):
    # fields
    size = 20000
    hubeau_url = "https://hubeau.eaufrance.fr/api/v1/qualite_nappes/analyses"
    params = {
        'size': size,
        'code_param': ",".join([str(_) for _ in code_params]),
        'bss_id': bss_id,
        'format': 'json',
        'fields': 'bss_id,code_bss,precision_coordonnees,longitude,latitude,altitude,nom_commune_actuel,'
                  'date_debut_prelevement,code_param,resultat,code_remarque_analyse,nom_remarque_analyse,'
                  'code_unite,nom_unite',
        'page': page
    }
    try:
        res = json.loads(requests.get(hubeau_url, params).text)
        if res['count'] > 20000:
            print(f'Station {bss_id} has over 20k analysis, this is a problem!!')

        data = res['data']
        if res['next']:
            data.extend(get_station_data(code_params, bss_id, page=page + 1))

        return data
    except json.decoder.JSONDecodeError:
        if retry:
            raise Exception(f'There is a problem for station {bss_id}')
        # probably a temp error, retrying with same params
        return get_station_data(code_params, bss_id, page)

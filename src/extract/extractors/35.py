import pandas as pd

from src.extract.extract import Extractor


class Extractor_35(Extractor):
    def _extract(self):
        raw_df = pd.read_excel(self.raw_fp, skiprows=2)

        raw_df = raw_df.drop(columns=["Unnamed: 0"])
        raw_df.columns = [_.replace("\n", " ") for _ in raw_df.columns]

        self._save_readable_df(raw_df)

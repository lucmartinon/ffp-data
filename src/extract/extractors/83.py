import pandas as pd

from src.extract.extract import Extractor


class Extractor_83(Extractor):
    def _extract(self):
        raw_df = self.get_raw_df()
        # the first raw is only telling us that all values are in ng/g
        raw_df = raw_df.drop(0)
        raw_df["E"] = raw_df["E"].str.replace(",", ".")
        raw_df["N"] = raw_df["N"].str.replace(",", ".")
        raw_df["E"] = pd.to_numeric(raw_df["E"])
        raw_df["N"] = pd.to_numeric(raw_df["N"])
        self._save_readable_df(raw_df)

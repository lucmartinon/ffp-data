import pandas as pd

from src.extract.extract import Extractor


class Extractor_22(Extractor):
    def _extract(self):
        df = pd.read_excel(self.raw_fp, sheet_name="Ark1")
        df["CommonName"] = df.apply(
            lambda row: row["CommonName"]
            if not pd.isna(row["CommonName"])
            else row["parameter"],
            axis=1,
        )

        self._save_readable_df(df)

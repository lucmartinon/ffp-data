import logging
import os
import re

import pandas as pd
import requests
from bs4 import BeautifulSoup

from src.extract.extract import Extractor
from src.utils import download_file


class Extractor_54(Extractor):
    def _extract(self):
        url = "https://www.arpa.veneto.it/dati-ambientali/open-data/idrosfera/concentrazione-di-sostanze-perfluoroalchiliche-pfas-nelle-acque-prelevate-da-arpav"
        soup = BeautifulSoup(requests.get(url).text, features="html.parser")
        for a in soup.find_all('a'):
            if a['href'].startswith('/dati-ambientali/open-data/file-e-allegati/'):
                data_url = "https://www.arpa.veneto.it" + a["href"]
                break

        ods_fp = os.path.join(self.raw_fp, "Water Samplings Veneto.ods")
        download_file(data_url, ods_fp)

        raw_df = pd.read_excel(ods_fp, engine="odf")
        raw_df.columns = [_.replace("/l", "/L") for _ in raw_df.columns]

        # some columns have weird triple spaces
        raw_df.columns = [c.replace('   ', ' ') for c in raw_df.columns]

        # converting everythin to ng/l
        to_drop = []
        for c in raw_df.columns:
            if "µg/L" in c:
                micro_col = c
                nano_col = micro_col.replace("µg/L", "ng/L")
                if nano_col in raw_df.columns:
                    raw_df[nano_col] = raw_df.apply(
                        lambda row: sum_nano_and_micro(row, nano_col, micro_col), axis=1
                    )
                    to_drop.append(micro_col)
                else:
                    logging.warning(
                        f"weird, we have col {micro_col} but not {nano_col}"
                    )

            # we remove columns that just indicate presence absence of substances
            if "Presente / Assente" in c:
                to_drop.append(c)
        logging.info("converted micrograms columns and sum them")
        raw_df = raw_df.drop(columns=to_drop)
        logging.info("dropped micrograms columns")
        logging.info(raw_df.columns)

        new_cols = []
        for c in raw_df.columns:
            logging.info(f"{c} > {get_new_col_name(c)}")
            new_cols.append(get_new_col_name(c))
        raw_df.columns = new_cols
        self._save_readable_df(raw_df)
        self._clean_dir()


def get_new_col_name(c):
    if "(ng/L)" not in c:
        return c

    if "CAS" in c:
        return c
    if c.startswith("Cl-PFPECA"):
        return c[:15]

    c = c.replace("isomerolineare", " lineare")
    c = c.replace("  ", " ")
    acronym = re.search(r"\(ng/L\) ?([^ ]+)", c)
    if acronym:
        acr = acronym.groups()[0]
        if "total" in c:
            return acr
        if "ramificati" in c:
            return acr + " branched"
        if "lineare" in c:
            return acr + " linear"
        return acr
    return c


def sum_nano_and_micro(row, nano_col, micro_col):
    nano_val = val(row[nano_col])
    micro_val = val(row[micro_col])
    if micro_val == 0:
        return row[nano_col]
    else:
        if nano_val == 0:
            return micro_val
        else:
            return nano_val + (1000 * micro_val)


def val(val_str: str):
    if pd.isna(val_str):
        return 0
    if str(val_str).startswith("<"):
        return 0
    return float(val_str)

from src.extract.extract import Extractor


class Extractor_107(Extractor):

    def _extract(self):
        df = self.get_raw_df()
        df = df.drop(0)
        df.loc[1, 'Analyte'] = 'name'
        df = df.transpose().reset_index()
        df.columns = df.iloc[0]
        df = df.drop(0)
        df['lat'] = df['Google map'].str.extract(r'\@(\d{2}\.\d{6,7})\,')
        df['lon'] = df['Google map'].str.extract(r'\@\d{2}\.\d{6,7}\,(\d{2}\.\d{6,7})\,')
        df = df.drop(columns=['Google map', 'Analyte'])

        self._save_readable_df(df)

from src.extract.extract import Extractor


class Extractor_99(Extractor):
    def _extract(self):
        df = self.get_raw_df()
        df.columns = [c.replace(" Results", "") for c in df.columns]

        self._save_readable_df(df)

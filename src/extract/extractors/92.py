import duckdb

from src.extract.extract import Extractor


class Extractor_92(Extractor):

    def _extract(self):
        duckdb.sql(f"""
copy(
    select 
        *,
        string_split(geo_point_2d, ', ')[1] lat,
        string_split(geo_point_2d, ', ')[2] lon
    from '{self.raw_fp}' where gruppe = 'Perfluorierte Tenside' 
) to '{self.readable_csv_fp}' 
        """)

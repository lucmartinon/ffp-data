import json
import logging

import pandas as pd
import requests
from tqdm.auto import tqdm

from src.extract.extract import Extractor


class Extractor_29(Extractor):
    def _extract(self):
        data = []
        codes_station = list_code_stations()
        print(f"Found {len(codes_station)} stations")
        for code in tqdm(codes_station):
            data.extend(get_station_data(code))
        df = pd.DataFrame(data)
        df['cas'] = df['code_parametre'].apply(get_cas)

        # see https://www.sandre.eaufrance.fr/definition/ALQ/2.2/RqAna
        df = df[~df['code_remarque'].isin([0, 3])]
        df = df[df['symbole_unite'] != 'Unité inconnue']
        df = df[df['resultat'] > 0]
        self._save_readable_df(df)


def list_code_stations(page=1):
    size = 2000
    url = "https://hubeau.eaufrance.fr/api/v2/qualite_rivieres/station_pc"
    params = {
        'size': size,
        'format': 'json',
        'page': page,
        'fields': 'code_station',
        'code_groupe_parametres': 68
    }
    r = requests.get(url, params)
    res = json.loads(r.text)
    if 'field_error' in res:
        print(res['field_error'])

    codes = [_['code_station'] for _ in res['data']]
    if res['next']:
        codes.extend(list_code_stations(page=page + 1))

    return codes


def get_station_data(code_station):
    url = 'https://hubeau.eaufrance.fr/api/v2/qualite_rivieres/analyse_pc'
    size = 20000
    params = {
        'size': size,
        'code_groupe_parametres': 68,
        'code_station': code_station,
        'fields': 'code_support,libelle_support,code_station,libelle_station,uri_station,longitude,latitude,code_parametre,libelle_parametre,code_fraction,libelle_fraction,date_prelevement,resultat,code_remarque,mnemo_remarque,code_unite,symbole_unite',
    }
    res = json.loads(requests.get(url, params).text)
    if res['count'] > size:
        logging.error('oooops, we have a station with more than 20k result, we need to change this')
    return res['data']


code_cas_map = {}


def get_cas(code_param):
    global code_cas_map
    if code_param not in code_cas_map:
        url = f"https://api.sandre.eaufrance.fr/referentiels/v1/par.json?outputSchema=SANDREv4&filter=%3CFilter%3E%3CIS%3E%3CField%3ECdParametre%3C%2FField%3E%3CValue%3E{code_param}%3C%2FValue%3E%3C%2FIS%3E%3C%2FFilter%3E"
        res = json.loads(requests.get(url).text)
        code_cas_map[code_param] = res['REFERENTIELS']['Referentiel']['Parametre'][0]['ParametreChimique'][
            'CdCASSubstanceChimique']

    return code_cas_map[code_param]

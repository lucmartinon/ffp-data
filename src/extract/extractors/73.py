import pandas as pd

from src.extract.extract import Extractor


class Extractor_73(Extractor):
    def _extract(self):
        df1 = pd.read_excel(self.raw_fp, sheet_name="MAS_PFAS")
        df2 = pd.read_excel(self.raw_fp, sheet_name="MAT_PFAS")
        df3 = pd.read_excel(self.raw_fp, sheet_name="MAR_PFAS")
        df = pd.concat([df1, df2, df3])
        df = df[~pd.isna(df["PARAMETRO_CAS"])]

        self._save_readable_df(df)

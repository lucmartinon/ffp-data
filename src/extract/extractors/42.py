import pandas as pd

from src.extract.extract import Extractor


class Extractor_42(Extractor):
    def _extract(self):
        raw_df = pd.read_csv(self.raw_fp, skiprows=5)
        raw_df.columns = [_.replace("\n", " ").replace("  ", " ") for _ in raw_df.columns]
        self._save_readable_df(raw_df)

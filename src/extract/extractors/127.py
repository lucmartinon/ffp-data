import pandas as pd

from src.extract.extract import Extractor


class Extractor_127(Extractor):
    def _extract(self):
        df = pd.read_excel(self.raw_fp, sheet_name="ACQUE SOTTERRANEE")

        self._save_readable_df(df)

import pandas as pd

from src.extract.extract import Extractor


class Extractor_18(Extractor):
    def _extract(self):
        sheets_dfs = pd.read_excel(self.raw_fp, sheet_name=None)
        dfs = [df for sheet, df in sheets_dfs.items() if sheet != "WWTP"]
        df = pd.concat(dfs)

        df["CASnr"] = df["CASnr"].apply(add_dashes)
        self._save_readable_df(df)


def add_dashes(cas_id):
    cas_id = str(cas_id)
    return f"{cas_id[:-3]}-{cas_id[-3:-1]}-{cas_id[-1:]}"

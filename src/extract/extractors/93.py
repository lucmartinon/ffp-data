from src.extract.extract import Extractor


class Extractor_93(Extractor):

    def _extract(self):
        df = self.get_raw_df()
        df.columns = df.loc[9]
        df_lim = df.loc[10]
        df = df.loc[13:]
        df = df.drop(columns=['Substance'])
        for c in df.columns[2:]:
            lod = df_lim[c] if c != 'PFOSA' else '0.005'
            df[c] = df[c].replace(0, f'<{lod}')

        self._save_readable_df(df)

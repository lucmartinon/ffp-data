import pandas as pd

from src.extract.extract import Extractor


class Extractor_90(Extractor):
    def _extract(self):
        data = pd.read_excel(self.raw_fp, sheet_name="Table of the results")
        stations = pd.read_excel(self.raw_fp, sheet_name="Table of the sampling sites")

        df = pd.merge(left=data, right=stations, on="ID")
        self._save_readable_df(df)

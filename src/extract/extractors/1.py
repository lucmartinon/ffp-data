import os
import zipfile

import duckdb

from src.extract.extract import Extractor
from src.utils import download_file


class Extractor_1(Extractor):

    def _extract(self):
        dl_url = 'https://sdi.eea.europa.eu/datashare/s/sDZMm9YpGB3TdQ9/download?path=%2F&files=User%20friendly%20.csv%20file&downloadStartSecret=xf3ctmxilgo'
        fp = os.path.join(self.raw_fp, 'data.csv.zip')
        if not os.path.exists(fp):
            download_file(dl_url, fp)

        with zipfile.ZipFile(fp, 'r') as zf:
            zf.extractall(self.raw_fp)

        csv_air_fp = os.path.join(self.raw_fp, 'User friendly .csv file', 'F1_4*.csv')
        csv_water_fp = os.path.join(self.raw_fp, 'User friendly .csv file', 'F2_4*.csv')

        activity_codes = [
            '4(a)(xi)',
            '4(a)(viii)',
            '4(a)(vi)',
            '4(a)(x)',
            '4(a)(ix)',
            '1(a)',
            '9(a)',
            '9(b)',
            '6(a)',
            '6(b)',
            '2(e)(i)',
            '2(f)',
            '2(c)(iii)',
            '5(b)',
            '5(c)',
            '5(f)',
            '5(a)',
            '5(g)'
        ]
        activity_codes_str = ','.join([f"'{_}'" for _ in activity_codes])

        df = duckdb.sql(f"""
select
    FacilityInspireID, 
    mode(facilityName) facilityName, 
    mode(eprtrSectorName) eprtr_sector, 
    mode(EPRTRAnnexIMainActivityCode) eprtr_activity_code, 
    mode(EPRTRAnnexIMainActivityLabel) eprtr_activity, 
    mode(City) city, 
    mode(countryName) country, 
    max(Longitude) Longitude, max(Latitude) Latitude,
from 
    (
    (select  * from '{csv_air_fp}') 
union by name
    (select  * from '{csv_water_fp}') 
)    
where EPRTRAnnexIMainActivityCode in ({activity_codes_str})
group by all
"""
                        ).df()

        # we add the sectore from the nice codes
        act_sector_map = {
            'Chemical installations for the production on an industrial scale of basic organic chemicals: Basic plastic materials (polymers, synthetic fibres and cellulose-based fibres)': 'Manufacture of plastics in primary forms',
            'Chemical installations for the production on an industrial scale of basic organic chemicals: Dyes and pigments': 'Finishing of textiles',
            'Chemical installations for the production on an industrial scale of basic organic chemicals: Halogenic hydrocarbons': 'Manufacture of other organic basic chemicals',
            'Chemical installations for the production on an industrial scale of basic organic chemicals: Surface-active agents and surfactants': 'Manufacture of other organic basic chemicals',
            'Chemical installations for the production on an industrial scale of basic organic chemicals: Synthetic rubbers': 'Manufacture of rubber and plastic products',
            'Independently operated industrial waste-water treatment plants which serve one or more activities covered in annex 1 of Regulation 166/2006': 'Sewerage',
            'Industrial plants for the production of paper and board and other primary wood products (such as chipboard, fibreboard and plywood)': 'Manufacture of articles of paper and paperboard',
            'Industrial plants for the production of pulp from timber or similar fibrous materials': 'Manufacture of pulp, paper and paperboard',
            'Installation for the production of non-ferrous crude metals from ore, concentrates or secondary raw materials by metallurgical, chemical or electrolytic processes': 'Manufacture of other fabricated metal products n.e.c.',
            'Installations for surface treatment of metals and plastic materials using an electrolytic or chemical process': 'Treatment and coating of metals',
            'Installations for the disposal of non-hazardous waste': 'Treatment and disposal of non-hazardous waste',
            'Installations for the incineration of non-hazardous waste in the scope of Directive 2000/76/EC of the European Parliament and of the Council of 4 December 2000 on the incineration of waste': 'Treatment and disposal of non-hazardous waste',
            'Installations for the processing of ferrous metals, Application of protective fused metal coats': 'Treatment and coating of metals',
            'Installations for the recovery or disposal of hazardous waste': 'Treatment and disposal of hazardous waste',
            'Mineral oil and gas refineries': 'Manufacture of refined petroleum products',
            'Plants for the pre-treatment (operations such as washing, bleaching, mercerisation) or dyeing of fibres or textiles': 'Finishing of textiles',
            'Plants for the tanning of hides and skins': 'Tanning and dressing of leather; dressing and dyeing of fur',
            'Urban waste-water treatment plants': 'Sewerage'
        }
        df['sector'] = df['eprtr_activity'].map(act_sector_map)

        self._save_readable_df(df)
        self._clean_dir()

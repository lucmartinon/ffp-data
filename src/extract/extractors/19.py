import pandas as pd

from src.extract.extract import Extractor


class Extractor_19(Extractor):
    def _extract(self):
        df = pd.read_excel(self.raw_fp, sheet_name="WWTP")
        self._save_readable_df(df)

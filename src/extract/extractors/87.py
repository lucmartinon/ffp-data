import pandas as pd

from src.extract.extract import Extractor


class Extractor_87(Extractor):
    def _extract(self):
        df1 = pd.read_excel(self.raw_fp, sheet_name="raw data 1")
        df2 = pd.read_excel(self.raw_fp, sheet_name="raw data 2")
        df3 = pd.read_excel(self.raw_fp, sheet_name="raw data 3")

        df1.columns = [_.replace("\n", " ").strip() for _ in df1.columns]
        df2.columns = [_.replace("\n", " ").strip() for _ in df2.columns]
        df3.columns = [_.replace("\n", " ").strip() for _ in df3.columns]

        df = pd.concat([df1, df2, df3])

        df = df.dropna(subset="Town")
        df = df[~df["Town"].str.contains("doi.org")]

        df = df.dropna(how="all", axis=1)
        self._save_readable_df(df)

import pandas as pd

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.extract.extract import Extractor


class Extractor_14(Extractor):
    def _extract(self):
        tw_df = pd.read_excel(self.raw_fp, sheet_name="Tabelle 47 TW PFAS")
        tw_df = tw_df.transpose()
        tw_df.columns = tw_df.iloc[0]
        tw_df = tw_df[1:]
        tw_df[NAME] = tw_df.index
        tw_df.loc["Wurzingerbrunnen", NAME] = "Brunnen Lebring"
        tw_df[CITY] = tw_df[NAME].apply(lambda v: v.split(" ")[1])
        tw_df[MATRIX] = Matrix.DRINKING_WATER

        col_rm = {
            "Perfluorbutansäure": "PFBA",
            "Perfluorpentansäure": "PFPeA",
            "Perfluorhexansäure PFHxA": "PFHxA",
            "Perfluorheptansäure": "PFHpA",
            "Perfluoroctansäure PFOA": "PFOA",
            "Perfluornonansäure PFNA": "PFNA",
            "Perfluordecansäure": "PFDA",
            "Perfluorundecansäure": "PFUnDA",
            "Perfluordodecansäure": "PFDoA",
            "Perfluorbutansulfonsäure PFBS": "PFBS",
            "Perfluorpentansulfonsäure": "PFPeS",
            "Perfluorohexansulfonsäure PFHxS": "PFHxS",
            "Perfluoroheptansulfonsäure": "PFHpS",
            "Perfluoroctansulfonsäure PFOS": "PFOS",
            "Perfluornonansulfonsäure": "PFNS",
            "Perfluordecansulfonsäure": "PFDS",
            "4:2-Fluortelomersulfonsäure": "4:2 FTS",
            "6:2-Fluortelomersulfonsäure": "6:2 FTS",
            "8:2- Fluortelomersulfonsäure": "8:2 FTS",
        }
        tw_df.columns = [col_rm[c] if c in col_rm else c for c in tw_df.columns]

        self._save_readable_df(tw_df)

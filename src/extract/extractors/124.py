import logging
import os
import zipfile

import duckdb
import requests

from src.constants.csv import SUBSTANCES_FP
from src.extract.extract import Extractor
from src.utils import download_file

api_url = "https://www.data.gouv.fr/api/1/datasets/resultats-du-controle-sanitaire-de-leau-du-robinet/"


class Extractor_124(Extractor):
    def _extract(self):
        res = requests.get(api_url).json()
        for resource in res["resources"]:
            fp = os.path.join(self.raw_fp, resource["title"])
            if not os.path.exists(fp):
                download_file(resource["url"], fp)
        logging.info("All files downloaded! ")

        # we need to filter year file if there are some month files
        zip_files = [fn for fn in os.listdir(self.raw_fp) if fn.endswith(".zip")]
        years_with_monthly_files = {fn[7:11] for fn in zip_files if len(fn) == 17}
        # if there are monthly files, we don't unzip the year file
        zip_files_to_skip = [f"eaurob-{year}.zip" for year in years_with_monthly_files]

        csvs_dir = os.path.join(self.raw_fp, "csvs")
        if not os.path.exists(csvs_dir):
            os.mkdir(csvs_dir)
        csvs_fns = sorted([fn for fn in os.listdir(csvs_dir) if fn.endswith(".csv")])
        if len(csvs_fns) == 0:
            for zip_fn in sorted(zip_files):
                if zip_fn not in zip_files_to_skip:
                    with zipfile.ZipFile(os.path.join(self.raw_fp, zip_fn), "r") as zf:
                        zf.extractall(csvs_dir)
                    logging.info(f"extracted files from {zip_fn}")
        logging.info("All zip files extracted!")

        duckdb.sql(
            f"""
        with pfas as (
            select casparam, cdparametre, libminparametre
            from '{csvs_dir}/PAR_*.csv' eaurob
            join '{SUBSTANCES_FP}' substances on substances."CAS Number" = eaurob.casparam
            where casparam is not null
        ),
        prel as (
            select
            referenceprel, dateprel, heureprel, coord_x, coord_y, nomcommune, distrlib, inae
            from read_csv_auto('{csvs_dir}/*_PLV_*')
        )
        select
            prel.*,
            pfas.*,
            rqana,
            cdunitereferencesiseeaux
        from read_csv_auto('{csvs_dir}/*_RES_*') res
            join pfas using(cdparametre)
            left join prel using(referenceprel)
        where rqana is not null and rqana not in ('0', 'N.M.')
        """
        ).to_csv(self.readable_csv_fp)
        logging.info(f"Extracted info with DuckDB to [{self.readable_csv_fp}]")
        self._clean_dir()

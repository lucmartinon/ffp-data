import pandas as pd

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.extract.extract import Extractor


class Extractor_126(Extractor):
    def _extract(self):
        biota_df = pd.read_excel(self.raw_fp, sheet_name="Tabelle 71 LM PFAS")
        biota_df = biota_df.transpose()
        biota_df.columns = biota_df.iloc[0]
        biota_df = biota_df[1:]
        biota_df[CITY] = biota_df["Betriebsort"].apply(lambda v: v.split(" ")[1])
        biota_df[MATRIX] = Matrix.BIOTA
        biota_df[YEAR] = biota_df.index.str[0:4]

        biota_df[NAME] = biota_df["Lebensmittel"] + " " + biota_df["Betriebsort"]
        self._save_readable_df(biota_df)

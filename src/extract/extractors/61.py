import logging

import pandas as pd
from sodapy import Socrata

from src import synonyms_utils
from src.extract.extract import Extractor


class Extractor_61(Extractor):

    @property
    def dataset_name(self):
        return "IT_Surface_Water_SVG"

    def _extract(self):
        # xls_fp = os.path.join(RAW, f"{self.fn}.xlsx")
        client = Socrata("www.dati.friuliveneziagiulia.it", None)
        logging.info(
            "start download from the Socrata api (this takes a while, as there are 700k rows)"
        )
        records = client.get("q8d6-8b5n", limit=20000000)
        df = pd.DataFrame.from_records(records)
        print(df.shape)
        logging.info("done")
        pfas_cas_list = synonyms_utils.get_pfas_cas_list()
        df = df[df["cas"].isin(pfas_cas_list)]
        self._save_readable_df(df)

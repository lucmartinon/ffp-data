import logging
import os

import geopandas as gpd

from src.extract.extract import Extractor
from src.utils import download_file, get_country_name


class Extractor_2(Extractor):
    def _extract(self):
        # todo once in a while: check if there is a newer file than this one https://sdi.eea.europa.eu/catalogue/srv/eng/catalog.search#/metadata/3a69dbaf-15d8-443d-a0ff-fa2c4bb8ab0d
        # if yes, change the url and re extract
        url = "https://sdi.eea.europa.eu/datashare/s/tzibdYEiGrCKLst/download"
        folder = "eea_data"
        zip_fn = f"{folder}.zip"

        zip_fp = os.path.join(self.raw_fp, zip_fn)
        download_file(url, zip_fp)
        import zipfile

        with zipfile.ZipFile(zip_fp, "r") as zip_f:
            zip_f.extractall(self.raw_fp)
        logging.info(f"unzipped to {self.raw_fp}")

        for fn in os.listdir(self.raw_fp):
            if os.path.isdir(os.path.join(self.raw_fp, fn)):
                gdb_fp = os.path.join(
                    self.raw_fp, fn, "GDB", "UWWTD_TreatmentPlants_public.gdb"
                )
                layer = "UWWTD_TreatmentPlants"
                gdf = gpd.read_file(gdb_fp, layer=layer)
                logging.info(f"loaded file {gdb_fp}")

                interresting_cols = [
                    "uwwCode",
                    "uwwName",
                    "uwwLatitude",
                    "uwwLongitude",
                    "uwwCapacity",
                    "repCode",
                    "uwwNUTS",
                    "uwwBeginLife",
                    "uwwEndLife",
                    "uwwInspireIDFacility",
                    "rptMStateKey",
                ]
                df = gdf[interresting_cols]
                df["repCode"] = df["repCode"].str.strip()
                df["country"] = df["rptMStateKey"].apply(get_country_name)
                # TODO in the FPP project, there was a filter on something to keep only 2500 WWTP.
                # As I dont find the logic, leaving them all for now.
                self._save_readable_df(df)

        self._clean_dir()

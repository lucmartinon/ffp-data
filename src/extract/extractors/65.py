import duckdb

from src.extract.extract import Extractor


class Extractor_65(Extractor):
    def _extract(self):
        con = duckdb.connect()
        con.sql("INSTALL spatial;LOAD spatial;")
        con.sql(
            f"create table stations as (SELECT * FROM st_read('{self.raw_fp}', layer = 'COORDINATES', open_options=['HEADERS=FORCE', 'FIELD_TYPES=AUTO']));"
        )
        con.sql(
            f"create table data as (SELECT * FROM st_read('{self.raw_fp}', layer = 'DATA', open_options=['HEADERS=FORCE', 'FIELD_TYPES=AUTO']));"
        )

        con.sql(
            f""" copy (
        select
        "POINT CODE", substance as substance, date, value, unit, "Station' Name", Location, "Hydric Body Code", "Hydric Body Name", "River", "Hydrographic Basin", "Municipality", "Coordinate X WGS84", "Coordinate Y WGS84"
        from data
            left join stations using ("point code")
            ) to '{self.readable_csv_fp}'
        """
        )

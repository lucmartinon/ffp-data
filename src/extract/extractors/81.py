from src.extract.extract import Extractor


class Extractor_81(Extractor):
    def _extract(self):
        df = self.get_raw_df()
        df["Latitute"] = df["Latitute"].str.replace(" N", "")
        df["Longitude"] = df["Longitude"].str.replace(" E", "")

        self._save_readable_df(df)

import pandas as pd

from src.extract.extract import Extractor


class Extractor_30(Extractor):
    def _extract(self):
        all_columns = self.get_raw_df().columns
        # columns are in 3 blocks:
        #  * A common info, cols 0 to 14
        #  * B first campaign, cols 15 to 28
        #  * C second campaign, cols 29 to 42

        # loading A + B
        df1 = pd.read_excel(self.raw_fp, usecols=all_columns[:29])
        df1.columns = df1.iloc[0]
        df1 = df1[1:]

        # loading A + B
        df2 = pd.read_excel(
            self.raw_fp, usecols=list(all_columns[:15]) + list(all_columns[29:])
        )
        df2.columns = df2.iloc[0]
        df2 = df2[1:]

        df = pd.concat([df1, df2])

        for c in df.columns:
            if c.endswith("\n (µg/L)"):
                # there are explanations in the excel, tab "légendes"
                df[c] = df[c].replace(
                    {"<LD*": "<0,002", "<LD": "<0,001", "Traces": "<0,004"}
                )
                df = df.rename(columns={c: c[:-8]})

        df = df.drop(columns=["∑ PFC (µg/L)"])
        df["Date prélèvement"] = df.apply(
            lambda r: r["Date prélèvement"]
            if not pd.isna(r["Date prélèvement"])
            else r["Date réception"],
            axis=1,
        )

        self._save_readable_df(df)

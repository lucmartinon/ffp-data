import pandas as pd

from src.extract.extract import Extractor


class Extractor_85(Extractor):
    def _extract(self):
        data = pd.read_excel(self.raw_fp, sheet_name="DADOS_PFOS_BIOTA", skiprows=2)
        # deleting useless rows at the end
        data = data.drop(list(range(12, 20)))
        stations = pd.read_excel(self.raw_fp, sheet_name="ESTAÇÕES")
        records = []

        for index, row in data.iterrows():
            for c in data.columns:
                if c != "DATA":
                    if not pd.isna(row[c]):
                        records.append(
                            {"station": c, "pfos": row[c], "date": row["DATA"]}
                        )
        recs = pd.DataFrame(records)
        df = pd.merge(
            left=recs,
            right=stations,
            how="left",
            right_on="ESTAÇÃO BIOTA-PEIXES",
            left_on="station",
        )
        df = df[~pd.isna(df["LATITUDE"])]

        self._save_readable_df(df)

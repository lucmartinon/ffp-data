import os
import zipfile

import duckdb

from src import synonyms_utils
from src.extract.extract import Extractor
from src.utils import download_file


class Extractor_94(Extractor):
    def _extract(self):
        url = "https://environment.data.gov.uk/api/file/download?fileDataSetId=909441cf-aae0-457e-a581-e933050a3ff1&fileName=Water_quality_monitoring_data_GCMS_LCMS_Semiquantitative.zip"
        zip_fp = os.path.join(self.raw_fp, "uk_quality.zip")
        if not os.path.exists(zip_fp):
            download_file(url, zip_fp)

        print("Zip file here")
        with zipfile.ZipFile(zip_fp, "r") as zf:
            zf.extractall(self.raw_fp)
        print("unzipped")

        cas_ids = [_.replace("-", "") for _ in synonyms_utils.get_pfas_cas_list()]
        cas_ids_sql_list = "(" + ", ".join(cas_ids) + ")"
        out_fp = self.readable_csv_fp
        q = f"copy (SELECT * from '{self.raw_fp}/*Screening.csv' where cas_number in {cas_ids_sql_list}) to '{out_fp}'"
        duckdb.sql(q)
        self._clean_dir()

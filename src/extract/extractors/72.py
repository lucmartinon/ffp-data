import pandas as pd

from src.extract.extract import Extractor


class Extractor_72(Extractor):
    def _extract(self):
        df_data = self.get_raw_df()
        df_stations = pd.read_excel(self.raw_fp, sheet_name="Statione")
        df = pd.merge(
            how="left",
            left=df_data,
            right=df_stations,
            left_on="codStaz",
            right_on="cod_pnt",
        )
        df = df.drop(columns=["cod_pnt"])

        pfas_params = [
            "Acido perfluorobutanoico (PFBA)",
            "Acido perfluorobutansolfonico (PFBS)",
            "Acido Perfluorodecanoico (PFDeA)",
            "ACIDO PERFLUOROEPTANOICO (PFHpA)",
            "Acido perfluoroesanoico (PFHxA)",
            "ACIDO PERFLUOROOTTANOICO( PFOA)",
            "ACIDO PERFLUOROOTTANSOLFONICO (PFOS) E SUOI SALI",
            "Acido perfluoropentanoico (PFPeA)",
            "Acido Perfluoroundecanoico (PFUnA)",
        ]
        df = df[df["Parametro"].isin(pfas_params)]

        self._save_readable_df(df)

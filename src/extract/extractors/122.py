import json

import pandas as pd
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

from src.extract.extract import Extractor


class Extractor_122(Extractor):

    def _extract(self):
        url_water = "https://geodata.environnement.brussels/api/geodata/postgis/soil/mv_pfas_resultats_eau/"
        url_soil = "https://geodata.environnement.brussels/api/geodata/postgis/soil/mv_pfas_resultats/"
        water_features = json.loads(requests.get(url_water).text)["features"]
        soil_features = json.loads(requests.get(url_soil).text)["features"]

        values = []
        for feature in tqdm(water_features):
            values.extend(parse_feature("Groundwater", feature))

        for feature in tqdm(soil_features):
            values.extend(parse_feature("Soil", feature))
        df = pd.DataFrame(values)
        print(df.shape)
        df = df[df["date"] != ""]
        print(df.shape)

        self._save_readable_df(df)

    @property
    def has_raw_source_file(self):
        return False

    @property
    def dataset_name(self):
        return "Brussels_atlas"


def parse_feature(matrix, feature):
    html_field = "html_fr_eau" if matrix == "Groundwater" else "html_fr_sol"
    somme_html_field = (
        "html_fr_eau_somme" if matrix == "Groundwater" else "html_fr_sol_somme"
    )
    soup = BeautifulSoup(feature["properties"][html_field], "html.parser")
    rows = soup.findAll("tr")  # first one is headers
    headers = rows.pop(0)
    unit = headers.findAll("th")[2].text[1:-1]
    values = []

    affectaction = (
        BeautifulSoup(feature["properties"]["html_fr_affectation"], "html.parser")
        .find("td")
        .text
    )
    name = None
    if feature["properties"][somme_html_field] is not None:
        th = BeautifulSoup(feature["properties"][somme_html_field], "html.parser").find(
            "th", string="Nom forage "
        )
        name = th.findNext("td").text
    date = (
        BeautifulSoup(feature["properties"]["html_fr_date"], "html.parser")
        .find("td")
        .text
    )

    # some dates are like '26/01/0023' instead of '26/01/2023'
    date = date.replace("/00", "/20")

    for row in rows:
        tds = row.findAll("td")
        values.append(
            {
                "Dossier": tds[0].text,
                "parameter": tds[1].text,
                "value": tds[2].text,
                "sampling_point_id": tds[3].text,
                "date": date,
                "deapth (min/max)": tds[5].text,
                "unit": unit,
                "affectation_zone": affectaction,
                "name": name,
                "matrix": matrix,
                "L72_x": feature["geometry"]["coordinates"][0],
                "L72_y": feature["geometry"]["coordinates"][1],
            }
        )

    return values

import abc
import glob
import logging
import os
import shutil
import time

import pandas as pd

from src.constants.csv import RAW, READABLE
from src.constants.exceptions import FppDataException
from src.constants.settings import API_REFRESH_PERIOD_DAYS, FILES_TO_IGNORE
from src.utils import get_dataset_name, get_dataset_fn


class Extractor(metaclass=abc.ABCMeta):
    dataset_id: int

    skip_rows = None
    excel_sheet_name = None

    def __init__(self, dataset_id):
        self.dataset_id = dataset_id

    def get_raw_df(self):
        extension = self.raw_fp.split(".")[-1]
        if extension == "csv":
            df = pd.read_csv(self.raw_fp, skiprows=self.skip_rows)

        elif extension in ["xls", "xlsx"]:
            if self.excel_sheet_name:
                df = pd.read_excel(
                    self.raw_fp, sheet_name=self.excel_sheet_name, skiprows=self.skip_rows
                )
            else:
                df = pd.read_excel(self.raw_fp, skiprows=self.skip_rows)
        else:
            raise FppDataException(f"Not sure how to get a df from file {fp}")
        df.columns = [_.replace("\n", " ").replace("  ", " ") for _ in df.columns]
        return df

    def _extract(self):
        self._save_readable_df(self.get_raw_df())

    def extract(self, force=False):
        if force or self.should_extract():
            self._extract()
        else:
            logging.info(f"Skipping [{self.dataset_id}] {self.dataset_name}")

    def should_extract(self):
        # if there is no readable file, we should always extract
        if not os.path.exists(self.readable_csv_fp):
            return True

        if self.has_raw_source_file:
            # if there is a source file, we extract only if newer than the readable file
            raw_update_time = os.path.getmtime(self.raw_fp)
            readable_update_time = os.path.getmtime(self.readable_csv_fp)

            # if the source file is a folder, we take the last update of the files within
            if os.path.isdir(self.raw_fp):
                raw_update_time = max(
                    [os.path.getmtime(os.path.join(self.raw_fp, _)) for _ in os.listdir(self.raw_fp) if
                     _ not in FILES_TO_IGNORE])

            return raw_update_time > readable_update_time
        else:
            # if there is no source file, it is an API extractor, we extract if the readable file is older than 30 days
            diff = time.time() - os.path.getmtime(self.readable_csv_fp)
            return diff / 86400 > API_REFRESH_PERIOD_DAYS

    def _save_readable_df(self, df):
        df.to_csv(self.readable_csv_fp, index=False)

    @property
    def has_raw_source_file(self):
        globs = glob.glob(f"{RAW}/{self.dataset_id}__*")
        if len(globs) == 0:
            return False
        elif len(globs) == 1:
            fp = globs[0]
            if os.path.isdir(fp):
                return len([_ for _ in os.listdir(fp) if _ not in FILES_TO_IGNORE]) > 0
            else:
                return True
        return False

    @property
    def is_generic_extractor(self):
        return self.__class__ == Extractor

    @property
    def dataset_name(self):
        return get_dataset_name(self.dataset_id)

    @property
    def csv_fn(self):
        return f"{self.dataset_id}__{self.dataset_name}.csv"

    @property
    def raw_fp(self):
        return os.path.join(RAW, get_dataset_fn(RAW, self.dataset_id))

    @property
    def readable_csv_fp(self):
        return os.path.join(READABLE, f"{self.dataset_id}__{self.dataset_name}.csv")

    def _clean_dir(self):
        if not os.path.isdir(self.raw_fp):
            raise FppDataException(
                "Trying to clean something that is not a directory, what are you doing?"
            )

        for fn in os.listdir(self.raw_fp):
            if fn != ".gitkeep":
                fp = os.path.join(self.raw_fp, fn)
                if os.path.isdir(fp):
                    shutil.rmtree(fp)
                else:
                    os.remove(fp)
                logging.info(f"Deleted {fp}")


def get_extractor(dataset_id) -> Extractor:
    try:
        mod = __import__(
            f"src.extract.extractors.{dataset_id}",
            fromlist=[f"Extractor_{dataset_id}"],
        )
        return getattr(mod, f"Extractor_{dataset_id}")(dataset_id)
    except ModuleNotFoundError:
        # the specific extractor doesn't exist, we create a simple extractor if we find a file in data/raw
        return Extractor(dataset_id)

import os

#
EXCLUDED_FNS = ['__init__.py', '__pycache__', '.DS_Store', '.gitkeep']

# FOLDERS
DATA = "data"
RAW = os.path.join(DATA, "raw")
OLD = os.path.join(DATA, "old")
READABLE = os.path.join(DATA, "readable")
EXTRACTORS = os.path.join('src', 'extract', 'extractors')
NORMALISERS = os.path.join('src', 'normalise', 'dataset_normalisers')

NORMALISED = os.path.join(DATA, "normalised")
PUBLIC = "public"

# PUBLIC CSVs
FULL_CSV_FP = os.path.join(PUBLIC, "full.csv")
FULL_PARQUET_FP = os.path.join(PUBLIC, "full.parquet")
MAP_CSV_FP = os.path.join(PUBLIC, "map.csv")
INDEX_FP = os.path.join(PUBLIC, "index.html")
STATS_FP = os.path.join(PUBLIC, "stats.html")
STATS_MD_FP = os.path.join(PUBLIC, "stats.md")
CLUSTERS_FP = os.path.join(PUBLIC, "clusters.csv")
SOURCES_FP = os.path.join(PUBLIC, "sources.csv")
DICTIONARY_FP = os.path.join(PUBLIC, "dictionary.csv")

# skeletons_fps
SKEL_DIR = os.path.join("src", "add_dataset", "skeletons")
NORMALISER_SKEL_FP = os.path.join(SKEL_DIR, "normaliser.txt")
EXTRACTOR_SKEL_FP = os.path.join(SKEL_DIR, "extractor.txt")

# settings
SETTINGS_DIR = os.path.join("data", "settings")
COLUMN_SYNONYMS_FP = os.path.join(SETTINGS_DIR, "column_synonyms.csv")
SUBSTANCE_SYNONYMS_FP = os.path.join(SETTINGS_DIR, "substance_synonyms.csv")
SUBSTANCES_FP = os.path.join(SETTINGS_DIR, "substances.csv")
KNOWN_CITIES = os.path.join(SETTINGS_DIR, "cities.csv")

# GOOGLE DOCS IDS (public version of it)
INDEX_GDOC_ID = "1mstZfd7W4v0skbTgcyZ31AqR576DcKvqmMOXE4Z1h6g"
INDEX_GDOC_SHEET_NAME = "index"

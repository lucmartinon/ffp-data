from src.constants import value_constants as VC

CATEGORY = "category"
LAT = "lat"
LON = "lon"
SOURCE_TYPE = "source_type"
SOURCE_TEXT = "source_text"
SOURCE_URL = "source_url"
NAME = "name"
CITY = "city"
COUNTRY = "country"
TYPE = "type"
SECTOR = "sector"
MATRIX = "matrix"
DATASET_ID = "dataset_id"
DATASET_NAME = "dataset_name"
DATE = "date"
YEAR = "year"

# details column: will have all values marked as KEEP columns, in a json field
DETAILS = "details"

# value columns
PFAS_SUM = "pfas_sum"
PFAS_VALUES = "pfas_values"

# the 16 info columns, non value ones
BASIC_COLUMNS = [
    CATEGORY,
    LAT,
    LON,
    SOURCE_TYPE,
    SOURCE_TEXT,
    SOURCE_URL,
    NAME,
    CITY,
    COUNTRY,
    TYPE,
    SECTOR,
    MATRIX,
    DATASET_ID,
    DATASET_NAME,
    DATE,
    YEAR,
]

# columns that are in the final full format
FULL_FORMAT_COLUMNS = BASIC_COLUMNS + [DETAILS, PFAS_VALUES, PFAS_SUM]

# other columns types not from the final data format
KEEP = "keep"
DROP = "drop"
COORD_X = "coord_x"
COORD_Y = "coord_y"
UNIT = "unit"
SL_VALUE = "sl_value"
UK_GRID_REFERENCE = "uk_grid_reference"

# and for multi lines datasets
ML_VALUE = "ml_value"
PARAM = "param"

# all columns but DETAILS
VALID_SOURCE_COLUMNS = BASIC_COLUMNS + [
    KEEP,
    DROP,
    COORD_X,
    COORD_Y,
    UNIT,
    SL_VALUE,
    ML_VALUE,
    PARAM,
    PFAS_SUM,
    UK_GRID_REFERENCE,
]

# columns that can be added as static values for an entire dataset
STATIC_COLUMNS = [
    SOURCE_TYPE,
    SOURCE_TEXT,
    SOURCE_URL,
    NAME,
    TYPE,
    SECTOR,
    MATRIX,
    COUNTRY,
    CATEGORY,
    YEAR,
]

MANDATORY_COLUMNS = [TYPE, NAME]
MANDATORY_COLUMNS_KNOWN = [MATRIX, YEAR, PFAS_SUM]
NON_NULL_COLUMNS = [TYPE]

NUMERICAL_COLUMNS = [LAT, LON, YEAR, COORD_X, COORD_Y, PFAS_SUM]

# columns where the value is from a list, defined as Enums in value_constants.py (VC)
LIST_COLUMNS = {
    TYPE: VC.Type,
    SOURCE_TYPE: VC.SourceType,
    MATRIX: VC.Matrix,
    COUNTRY: VC.Country,
    SECTOR: VC.Sector,
    CATEGORY: VC.Category,
}

# columns of the PFAS_VALUES items
CAS_ID = "cas_id"
SUBSTANCE = "substance"
ISOMER = "isomer"
VALUE = "value"
LESS_THAN = "less_than"

from enum import Enum


class Category(str, Enum):
    PRESUMPTIVE = "Presumptive"
    KNOWN = "Known"
    KNOWN_PFAS_USER = "Known PFAS user"


class SourceType(str, Enum):
    FOI = "FOI"
    COMPANY_WEBSITE = "Company website"
    EMAIL_FROM_THE_COMPANY = "Email from the company"
    SCIENTIFIC_ARTICLE = "Scientific article"
    AUTHORITIES = "Authorities"
    EUROPEAN_ENVIRONMENT_AGENCY = "European Environment Agency"
    OSINT = "OSINT"
    PRESS_INQUIRY = "Press inquiry"
    OWN_SAMPLING_CAMPAIGN = "Own sampling campaign"


class Type(str, Enum):
    INDUSTRIAL_SITE = "Industrial site"
    WASTE_MANAGEMENT_SITE = "Waste management site"
    AIRPORT = "Airport"
    MILITARY_SITE = "Military site"
    SAMPLING_LOCATION = "Sampling location"
    FIREFIGHTING_INCIDENT_TRAINING = "Firefighting incident / training"
    PFAS_MANUFACTURING_FACILITY = "PFAS manufacturing facility"
    OTHER = "Other"


class Sector(str, Enum):
    PREPARATION_AND_SPINNING_OF_TEXTILE_FIBRES = (
        "Preparation and spinning of textile fibres"
    )
    WEAVING_OF_TEXTILES = "Weaving of textiles"
    FINISHING_OF_TEXTILES = "Finishing of textiles"
    MANUFACTURE_OF_CARPETS_AND_RUGS = "Manufacture of carpets and rugs"
    MANUFACTURE_OF_NONWOVENS_AND_ARTICLES_MADE_FROM_NONWOVENS_EXCEPT_APPAREL = (
        "Manufacture of non-wovens and articles made from non-wovens, except apparel"
    )
    MANUFACTURE_OF_OTHER_TECHNICAL_AND_INDUSTRIAL_TEXTILES = (
        "Manufacture of other technical and industrial textiles"
    )
    TANNING_AND_DRESSING_OF_LEATHER_DRESSING_AND_DYEING_OF_FUR = (
        "Tanning and dressing of leather; dressing and dyeing of fur"
    )
    MANUFACTURE_OF_PULP_PAPER_AND_PAPERBOARD = (
        "Manufacture of pulp, paper and paperboard"
    )
    MANUFACTURE_OF_ARTICLES_OF_PAPER_AND_PAPERBOARD = (
        "Manufacture of articles of paper and paperboard"
    )
    OTHER_PRINTING = "Other printing"
    PREPRESS_AND_PREMEDIA_SERVICES = "Pre-press and pre-media services"
    MANUFACTURE_OF_REFINED_PETROLEUM_PRODUCTS = (
        "Manufacture of refined petroleum products"
    )
    MANUFACTURE_OF_OTHER_ORGANIC_BASIC_CHEMICALS = (
        "Manufacture of other organic basic chemicals"
    )
    MANUFACTURE_OF_PLASTICS_IN_PRIMARY_FORMS = (
        "Manufacture of plastics in primary forms"
    )
    MANUFACTURE_OF_PAINTS_VARNISHES_AND_SIMILAR_COATINGS_PRINTING_INK_AND_MASTICS = "Manufacture of paints, varnishes and similar coatings, printing ink and mastics"
    MANUFACTURE_OF_SOAP_AND_DETERGENTS_CLEANING_AND_POLISHING_PREPARATIONS = (
        "Manufacture of soap and detergents, cleaning and polishing preparations"
    )
    MANUFACTURE_OF_OTHER_CHEMICAL_PRODUCTS_NEC = (
        "Manufacture of other chemical products n.e.c."
    )
    MANUFACTURE_OF_RUBBER_AND_PLASTIC_PRODUCTS = (
        "Manufacture of rubber and plastic products"
    )
    MANUFACTURE_OF_RUBBER_PRODUCTS = "Manufacture of rubber products"
    MANUFACTURE_OF_OTHER_FABRICATED_METAL_PRODUCTS_NEC = (
        "Manufacture of other fabricated metal products n.e.c."
    )
    TREATMENT_AND_COATING_OF_METALS = "Treatment and coating of metals"
    MANUFACTURE_OF_ELECTRONIC_COMPONENTS = "Manufacture of electronic components"
    MANUFACTURE_OF_LOADED_ELECTRONIC_BOARD = "Manufacture of loaded electronic board"
    MANUFACTURE_OF_OPTICAL_INSTRUMENTS_AND_PHOTOGRAPHIC_EQUIPMENT = (
        "Manufacture of optical instruments and photographic equipment"
    )
    MANUFACTURE_OF_OTHER_ELECTRICAL_EQUIPMENT = (
        "Manufacture of other electrical equipment"
    )
    MANUFACTURE_OF_OFFICE_MACHINERY_AND_EQUIPMENT = "Manufacture of office machinery and equipment (except computers and peripheral equipment)"
    COLLECTION_OF_HAZARDOUS_WASTE = "Collection of hazardous waste"
    TREATMENT_AND_DISPOSAL_OF_NON_HAZARDOUS_WASTE = (
        "Treatment and disposal of non-hazardous waste"
    )
    TREATMENT_AND_DISPOSAL_OF_HAZARDOUS_WASTE = (
        "Treatment and disposal of hazardous waste"
    )
    WHOLESALE_OF_CHEMICAL_PRODUCTS = "Wholesale of chemical products"
    SEWERAGE = "Sewerage"


class Matrix(str, Enum):
    SEWERAGE = "Sewerage"
    SOIL = "Soil"
    SURFACE_WATER = "Surface water"
    GROUNDWATER = "Groundwater"
    WASTEWATER = "Wastewater"
    DRINKING_WATER = "Drinking water"
    SEA_WATER = "Sea water"
    BIOTA = "Biota"
    COMPOST = "Compost"
    SLUDGE = "Sludge"
    SEDIMENT = "Sediment"
    LEACHATE = "Leachate"
    SEPTAGE = "Septage"
    UNKNOWN = "Unknown"
    ATMOSPHERE = "Atmosphere"
    RAINWATER = "Rainwater"


class Country(str, Enum):
    AUSTRIA = "Austria"
    BELGIUM = "Belgium"
    BULGARIA = "Bulgaria"
    CROATIA = "Croatia"
    CYPRUS = "Cyprus"
    CZECHIA = "Czechia"
    DENMARK = "Denmark"
    ESTONIA = "Estonia"
    FINLAND = "Finland"
    FRANCE = "France"
    GERMANY = "Germany"
    GREECE = "Greece"
    HUNGARY = "Hungary"
    IRELAND = "Ireland"
    ITALY = "Italy"
    LATVIA = "Latvia"
    LITHUANIA = "Lithuania"
    LUXEMBOURG = "Luxembourg"
    MALTA = "Malta"
    NETHERLANDS = "Netherlands"
    POLAND = "Poland"
    PORTUGAL = "Portugal"
    ROMANIA = "Romania"
    SLOVAKIA = "Slovakia"
    SLOVENIA = "Slovenia"
    SPAIN = "Spain"
    SWEDEN = "Sweden"
    UNITED_KINGDOM = "United Kingdom"
    SWITZERLAND = "Switzerland"
    NORWAY = "Norway"
    ICELAND = "Iceland"
    LIECHTENSTEIN = "Liechtenstein"
    ANDORRA = "Andorra"
    SERBIA = "Serbia"
    UKRAINE = "Ukraine"
    FAROE_ISLANDS = "Faroe Islands"
    BALTIC_SEA = "Baltic Sea"
    NORTH_SEA = "North Sea"
    GULF_OF_BOTHNIA = "Gulf of Bothnia"
    FINNISH_GULF = "Finnish Gulf"
    KATTEGAT = "Kattegat"
    SKAGERRAK = "Skagerrak"


COUNTRY_NAMES_REPLACE_MAP = {
    "Czech Republic": Country.CZECHIA
}

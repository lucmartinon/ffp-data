import os
from enum import Enum

from src.constants.csv import NORMALISER_SKEL_FP, NORMALISERS
from src.normalise.ml_normaliser import MLNormaliser
from src.normalise.sl_normaliser import SLNormaliser


class NormaliserType(Enum):
    SL = SLNormaliser
    ML = MLNormaliser


def create_normaliser(dataset_id, class_name, class_mod, py_params, replace_map=None):
    """
    Creates an SL normaliser from the params.
    :param class_mod:
    :param class_name:
    :param dataset_id:
    :param py_params: the params that will be inserted in the skeleton
    :return:
    """

    class_variables = ""
    for k, v in py_params.items():
        if isinstance(v, int):
            class_variables += f"\n    {k} = {v}"

        elif isinstance(v, str):
            class_variables += f"\n    {k} = '{v}'"

        elif k == "columns":
            # we only have dict where all values are str
            lines = [f'"{kk}": {vv.upper()}' for kk, vv in v.items()]
            lines_str = ",\n        ".join(lines)
            class_variables += f"\n    {k} = {{\n        {lines_str}\n    }}"

        elif k == "static_values":
            # we only have dict where all values are str
            lines = [f'{kk.upper()}: "{vv}"' for kk, vv in v.items()]
            lines_str = ",\n        ".join(lines)
            class_variables += f"\n    {k} = {{\n        {lines_str}\n    }}"

        elif isinstance(v, dict):
            # we only have dict where all values are str
            lines = [f'"{kk}": "{vv}"' for kk, vv in v.items()]
            lines_str = ",\n        ".join(lines)
            class_variables += f"\n    {k} = {{\n        {lines_str}\n    }}"

    if len(replace_map.keys()) > 0:
        replacements = []
        for field, map in replace_map.items():
            lines = [
                f'"{k}": {field.capitalize()}.{v.upper().replace(" ", "_")}'
                for k, v in map.items()
            ]
            lines_str = ",\n            ".join(lines)
            replacements.append(
                f"{field.upper()}: {{\n            {lines_str}\n         }}"
            )

        replacement_str = ",\n        ".join(replacements)
        class_variables += (
            f"\n\n    replace_map = {{\n        {replacement_str}\n    }}"
        )

    with open(NORMALISER_SKEL_FP) as f:
        skeleton = f.read()

    skeleton = skeleton.replace("{dataset_id}", str(dataset_id))
    skeleton = skeleton.replace("{class_name}", str(class_name))
    skeleton = skeleton.replace("{class_fn}", str(class_mod))
    skeleton = skeleton.replace("{class_variables}", class_variables)

    with open(os.path.join(NORMALISERS, f"{dataset_id}.py"), "w") as f:
        f.write(skeleton)

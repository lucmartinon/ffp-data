from colorist import Color, effect_dim


def num_input(input_msg):
    try:
        return int(get_input(input_msg))
    except ValueError:
        print("Should be an int")
        return num_input(input_msg)


def validated_input(prompt, valid_values: [str]):
    inp = get_input(prompt)
    if inp in [str(_) for _ in valid_values] or inp in valid_values:
        return inp
    else:
        valid_values_list = "\n".join(valid_values)
        print(
            f"Not a valid value. Please enter one of the following: \n{valid_values_list}"
        )
        return validated_input(prompt, valid_values)


def confirm_choice(prompt: str):
    res = get_input(prompt).lower()
    if res in ["yes", "y"]:
        return True
    return False


def pink(msg):
    return f"{Color.MAGENTA}{msg}{Color.OFF}"


def print_color(msg, color: Color):
    print(f"{color}{msg}{Color.OFF}")


def print_info(msg):
    effect_dim(msg, color=Color.YELLOW)


def get_input(prompt):
    if not prompt.endswith("\n"):
        prompt += "\n"
    return input(prompt)

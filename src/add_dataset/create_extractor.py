import os

from src.constants.csv import EXTRACTOR_SKEL_FP, EXTRACTORS


def create_extractor(dataset_id):
    with open(EXTRACTOR_SKEL_FP) as f:
        skeleton = f.read()

    skeleton = skeleton.replace("{dataset_id}", str(dataset_id))

    fp = os.path.join(EXTRACTORS, f"{dataset_id}.py")
    with open(fp, "w") as f:
        f.write(skeleton)

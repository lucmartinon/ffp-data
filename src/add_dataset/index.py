import logging
import os
import ssl
from enum import Enum

import gspread
import pandas as pd
from gspread import SpreadsheetNotFound, WorksheetNotFound

from src.constants.csv import INDEX_GDOC_ID, RAW, INDEX_GDOC_SHEET_NAME
from src.services import drive_service
from src.services.gspread_service import download_csv_from_spreadsheet_url

ssl._create_default_https_context = ssl._create_unverified_context

index = None


# interesting index columns index_columns
class IC(str, Enum):
    COUNTRY = "country"
    ID = "ID"
    CATEGORY = "category"
    NAME = "Name"
    FINAL_DATA = "Link to final data"
    SOURCE_DATA = "Drive"
    SOURCE_TYPE = "source_type"
    SOURCE_TEXT = "source_text"
    SOURCE_URL = "source_url"
    NEW_ID = "new id"


def get_index():
    global index
    if index is None:
        url = f"https://docs.google.com/spreadsheets/d/{INDEX_GDOC_ID}/gviz/tq?tqx=out:csv&sheet={INDEX_GDOC_SHEET_NAME}"
        index_df = pd.read_csv(url)
    return index_df.to_records()


def get_index_dataset(line_number):
    return get_index()[line_number - 2]


def get_dataset_by_name(dataset_name):
    for d in get_index():
        if d[IC.NAME].strip() == dataset_name:
            return d
    logging.warning(f"couldn't find any dataset with name [{dataset_name}]")


def get_dataset_by_new_id(new_id):
    for d in get_index():
        if d[IC.NEW_ID] == new_id:
            return d
    logging.warning(f"couldn't find any dataset with new id [{new_id}]")


def download_all():
    for dataset in get_index():
        d_str = f"[{dataset[IC.NEW_ID]}]: {dataset[IC.NAME]}"
        ds_index_name = dataset[IC.NAME].strip()

        fp = os.path.join(RAW, f"{dataset[IC.NEW_ID]}__{ds_index_name}.csv")
        if dataset[IC.NEW_ID] != "":
            if not os.path.exists(fp):
                if len(dataset[IC.SOURCE_DATA].split("/")) >= 5:
                    try:
                        download_csv_from_spreadsheet_url(dataset[IC.SOURCE_DATA], fp)
                        logging.info(f"{d_str}: downloaded")
                        continue
                    except gspread.exceptions.APIError as e:
                        if e.response.json()["error"]["code"] == 400:
                            continue
                        logging.info(f"{d_str}: not a google spreadsheet")
                    except (SpreadsheetNotFound, WorksheetNotFound):
                        logging.info(f"{d_str}: not found")
                        continue

                    drive_service.download_file_to_fp(dataset[IC.SOURCE_DATA], fp)

            else:
                logging.info(f"{d_str}: already there")

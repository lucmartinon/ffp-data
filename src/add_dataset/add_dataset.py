import os

from tabulate import tabulate

import src.constants.value_constants as vc
from src.add_dataset.create_normaliser import create_normaliser
from src.add_dataset.index import IC, get_dataset_by_new_id, get_index_dataset
from src.add_dataset.input_utils import (
    confirm_choice,
    get_input,
    num_input,
    pink,
    print_info,
    validated_input,
)
from src.constants.columns import *
from src.constants.csv import RAW, READABLE, NORMALISERS
from src.services.gspread_service import download_csv_from_spreadsheet_url
from src.synonyms_utils import get_static_values, infer_columns
from src.utils import get_dataset_name, get_df


def add_from_index():
    line_number = num_input(
        """
In the index, what is the line number of the dataset?
"""
    )
    dataset = get_index_dataset(line_number)
    ds_index_name = dataset[IC.NAME].strip()

    if not confirm_choice(
            f"the dataset at line {line_number} is dataset with name [{ds_index_name}]. "
            f"Is this correct? (Y/N)"
    ):
        return

    dataset_id = num_input(
        f"The id of the dataset in the Index is [{dataset[IC.ID]}], what should the new index be?"
    )

    source = num_input(
        """
OK, where is the raw data?
    1 - in [Link to final data]
    2 - in [Drive]
    """
    )

    fp = os.path.join(RAW, f"{dataset_id}__{ds_index_name}.csv")
    if source == 1:
        download_csv_from_spreadsheet_url(dataset[IC.FINAL_DATA], fp)
        print_info("retrieved df from column [Link to final data]")
    elif source == 2:
        download_csv_from_spreadsheet_url(dataset[IC.SOURCE_DATA], fp)
        print_info("retrieved df from column [Drive]")

    print_info(
        f"Raw data downloaded to {fp}. "
        f"Now you can create an extractor or directly a normaliser if the file is simple. "
    )


def add_normaliser(dataset_id):
    dataset_name = get_dataset_name(dataset_id)
    dataset = get_dataset_by_new_id(dataset_id)

    normaliser_type = num_input(
        f"Creating a normaliser for dataset [{dataset_name}], id [{dataset_id}], great!"
        f"What type of normaliser should we create?\n"
        f"1 - Single line normaliser\n"
        f"2 - Multi lines normaliser"
    )
    if normaliser_type == 1:
        class_name = "SLNormaliser"
        class_mod = "sl_normaliser"

    if normaliser_type == 2:
        class_name = "MLNormaliser"
        class_mod = "ml_normaliser"

    df = get_df(READABLE, dataset_id)

    columns = infer_columns(df.columns)

    py_params = {"columns": columns}
    if DATE in columns.values():
        date_col = [k for k, v in columns.items() if v == DATE][0]
        print_info("Let's try to guess the date format. Here are a few date examples")
        print_info(df.head(10)[date_col])
        formats = [
            "mixed",
            "%d/%m/%Y",
            "%m/%d/%Y",
            "%d.%m.%Y",
            "%Y-%m-%d",
            "%Y-%m-%d %H:%M:%S",
            "ISO8601",
        ]
        print(
            tabulate(
                {"Choice": range(len(formats)), "Column Type": formats},
                headers="keys",
                tablefmt="rounded_outline",
            )
        )
        choice = validated_input(
            "Pick one or leave empty to enter a custom date format",
            valid_values=list(range(len(formats))) + [""],
        )
        if choice != "":
            py_params["date_format"] = formats[int(choice)]
        else:
            py_params["date_format"] = get_input("Please enter a custom date format:")

    replace_map = {}
    if MATRIX in columns.values():
        matrix_col = [k for k, v in columns.items() if v == MATRIX][0]
        possible_values = [_ for _ in vc.Matrix]
        if not df[matrix_col].isin(possible_values).all():
            print_info(
                "You have a matrix column where not all values are known, let's map them!"
            )
            replace_map[MATRIX] = {}
            for val in df[matrix_col].unique():
                if val in possible_values:
                    replace_map[MATRIX][val] = val
                else:
                    mat = validated_input(f"What is {pink(val)}?", possible_values)
                    replace_map[MATRIX][val] = mat

    py_params["static_values"] = get_static_values(columns, dataset)

    if COORD_X in columns.values():
        proj_epsg_id = num_input(
            "You have a column marked as COORD_X. What is the corresponding EPSG_ID?"
        )
        py_params["proj_epsg_id"] = proj_epsg_id

    print_info(f"The normaliser will be created with ID [{dataset_id}]")

    create_normaliser(
        dataset_id, class_name, class_mod, py_params=py_params, replace_map=replace_map
    )
    print_info(
        f"The normaliser was created, see src/normalise/dataset_normalisers/{dataset_id}.py"
    )


def get_next_dataset_id():
    existing_ids = sorted(
        [
            int(_.split(".")[0])
            for _ in os.listdir(NORMALISERS)
            if not _.startswith("__")
        ]
    )
    if len(existing_ids) > 0:
        return existing_ids[-1] + 1
    else:
        return 0

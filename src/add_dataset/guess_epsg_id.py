import logging

import pandas as pd
import pyproj
from tabulate import tabulate

from src.add_dataset import input_utils
from src.constants.columns import *
from src.normalise.get_normaliser import get_normaliser
from src.services.geoapify import get_geoapify_info

usual_suspects = {
    25884,
    3059,
    2154,
    3006,
    3065,
    3346,
    4230,
    9793,
    23033,
    25832,
    25833,
    31370,
    # les italiens
    4265,
    3003,
    3004,
    4806,
    26591,
    26592,
    23032,
    23034,
    4937,
    4258,
    25834,
    25834,
    3034,
    3035,
    27700,  # UK eastings and northings
    2056  # la Suisse
}


def guess_epsg_id(dataset_id):
    logging.info(f"trying to find EPSG id for dataset {dataset_id}")
    normaliser = get_normaliser(dataset_id)
    df = pd.read_csv(normaliser.readable_fp)
    x, y, col_name, col_city = None, None, None, None
    for c in df.columns:
        if normaliser.columns[c] == COORD_X:
            x = c

        if normaliser.columns[c] == COORD_Y:
            y = c

        if normaliser.columns[c] == CITY:
            col_city = c

        if normaliser.columns[c] == NAME:
            col_name = c

    guess_row(df, x, y, col_city, col_name, 0)


def guess_row(df, x, y, col_city, col_name, row):
    results = []
    for epsg_id in usual_suspects:
        res = project(epsg_id, df, x, y, 0)
        if res:
            results.append(res)

    desc = f"We tried with row [{row}]:"
    if col_city:
        desc += f" City: [{df.iloc[0][col_city]}]"
    if col_name:
        desc += f" Name: [{df.iloc[0][col_name]}]"
    logging.info(desc)

    print(
        tabulate(
            {
                "epsg_id": [_[0] for _ in results],
                "Country": [_[1] for _ in results],
                "City": [_[2] for _ in results],
                "Link": [_[3] for _ in results],
            },
            headers="keys",
            tablefmt="rounded_outline",
        )
    )
    if input_utils.confirm_choice("Run for next row?"):
        guess_row(df, x, y, col_city, col_name, row + 1)


def project(epsg_id, df, x, y, row):
    proj = pyproj.Transformer.from_crs(epsg_id, 4326, always_xy=True)

    coord_x, coord_y = df.iloc[row][x], df.iloc[row][y]
    lon, lat = proj.transform(coord_x, coord_y)
    if lat > 90 or lon > 90:
        logging.info(
            f"With EPSG ID {epsg_id}, the values are clearly incorrect, skipping"
        )
        return None

    logging.info(
        f"With EPSG ID {epsg_id}, the first values {coord_x}, {coord_y} correspond to {lat}, {lon}"
    )

    city, country = get_geoapify_info(lat, lon, save=False)
    gmaps_url = f"https://www.google.fr/maps/place/{lat},{lon}"
    return epsg_id, country, city, gmaps_url

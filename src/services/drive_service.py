import io
import logging
import os

import pandas as pd
from google.auth.exceptions import RefreshError
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from googleapiclient.http import MediaFileUpload, MediaIoBaseDownload

# +
# If modifying these scopes, delete the file token.json.
SCOPES = [
    "https://www.googleapis.com/auth/drive",
    "https://www.googleapis.com/auth/drive.readonly",
]

credentials_fp = "secret/drive_credentials.json"
token_fp = "secret/token.json"


# -


def get_drive_service(retrying=False):
    """Shows basic usage of the Drive v3 API.
    Prints the names and ids of the first 10 files the user has access to.
    """
    creds = None
    # The file token.json stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(token_fp):
        creds = Credentials.from_authorized_user_file("secret/token.json", SCOPES)
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            try:
                creds.refresh(Request())
            except RefreshError:
                os.remove(token_fp)
                if not retrying:
                    return get_drive_service(retrying=True)

        else:
            flow = InstalledAppFlow.from_client_secrets_file(credentials_fp, SCOPES)
            creds = flow.run_local_server(port=0)
        # Save the credentials for the next run
        with open("secret/token.json", "w") as token:
            token.write(creds.to_json())

    try:
        service = build("drive", "v3", credentials=creds, cache_discovery=False)
        return service

    except HttpError as error:
        print(f"An error occurred: {error}")


def upload_csv(fp, fn, file_id=None, folder_id=None):
    try:
        service = get_drive_service()
        file_metadata = {"name": fn}
        if file_id is None and folder_id is not None:
            file_metadata["parents"] = [folder_id]

        media = MediaFileUpload(fp, mimetype="file/csv")
        if file_id is None:
            file = (
                service.files()
                .create(
                    body=file_metadata,
                    media_body=media,
                    fields="id",
                    supportsAllDrives=True,
                )
                .execute()
            )
        else:
            file = (
                service.files()
                .update(
                    body=file_metadata,
                    media_body=media,
                    fields="id",
                    supportsAllDrives=True,
                    fileId=file_id,
                )
                .execute()
            )

    except HttpError as error:
        logging.error(f"An error occurred: {error}")
        file = None
    logging.info(
        f"{fn} uploaded, link to file is: https://drive.google.com/file/d/{file.get('id')}/view?usp=share_link"
    )
    return file.get("id")


def get_df_from_csv_url(csv_url):
    file_id = csv_url.split("/")[5]
    file = download_file(file_id)
    return pd.read_csv(io.StringIO(file.getvalue().decode("utf-8")), sep=",")


def download_file(file_id):
    try:
        # create drive api client
        service = get_drive_service()

        # pylint: disable=maybe-no-member
        request = service.files().get_media(fileId=file_id)
        file = io.BytesIO()
        downloader = MediaIoBaseDownload(file, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()

    except HttpError as error:
        logging.error(f"An error occurred: {error}")
        file = None

    return file


def download_file_to_fp(url, fp):
    file_id = url.split("/")[5]
    with open(fp, "wb") as out_f:
        with download_file(file_id) as file:
            out_f.write(file.getvalue())


# +
def get_last_modified(file_id):
    try:
        service = get_drive_service()
        file = (
            service.files()
            .get(fileId=file_id, fields="modifiedTime", supportsAllDrives=True)
            .execute()
        )

    except HttpError as error:
        print(f"An error occurred: {error}")
        file = None

    return file.get("modifiedTime")


# -


def get_parent_id(file_id):
    try:
        service = get_drive_service()
        file = (
            service.files()
            .get(fileId=file_id, fields="parents", supportsAllDrives=True)
            .execute()
        )

    except HttpError as error:
        print(f"An error occurred: {error}")
        file = None

    return file.get("parents")


def get_sub_folders(folder_id):
    q = f"parents in '{folder_id}' and mimeType = 'application/vnd.google-apps.folder'"
    results = (
        get_drive_service()
        .files()
        .list(
            q=q,
            pageSize=100,
            fields="nextPageToken, files(id, name, modifiedTime)",
            supportsAllDrives=True,
        )
        .execute()
    )
    items = results.get("files", [])
    results = [sf["id"] for sf in items]
    for sf in items:
        print(f'getting folders of {sf["name"]}')
        results.extend(get_sub_folders(sf["id"]))
    return results


def save_df(df, fn=None, file_id=None, folder_id=None, upload=False, fp=None):
    if fp is None and fn is not None:
        fp = f"datasets_notebooks/data/{fn}"

    if fn is None and fp is not None:
        fn = os.path.basename(fp)

    df.to_csv(fp, index=False, float_format="%g")
    logging.info(f"df saved to {fp}")

    if upload:
        if file_id is not None:
            upload_csv(fp=fp, fn=fn, file_id=file_id)
        elif folder_id is not None:
            file_id = upload_csv(fp=fp, fn=fn, folder_id=folder_id)
            logging.info(f"File uploaded for first time, file_id is: {file_id}")
        else:
            logging.info("cannot upload if file_id and folder_id are both None")

    return file_id

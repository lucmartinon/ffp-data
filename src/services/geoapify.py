import logging
import math

import pandas as pd
import requests

from src.constants.columns import *
from src.constants.csv import KNOWN_CITIES
from src.settings import COORDINATE_PRECISION

GEOAPIFY_REVERSE_URL = "https://api.geoapify.com/v1/geocode/reverse"
GEOAPIFY_SEARCH_URL = "https://api.geoapify.com/v1/geocode/search"
HEADERS = {"Accept": "application/json"}

cities_df = pd.read_csv(KNOWN_CITIES)
cities_df.set_index([LAT, LON], inplace=True)


def get_geoapify(lat, lon):
    resp = requests.get(
        GEOAPIFY_REVERSE_URL,
        headers=HEADERS,
        params={"lat": lat, "lon": lon, "apiKey": "abbee90dc10c4bc8ba4231dd9acab82d"},
    )
    return resp.json()


def get_geoapify_info(lat, lon, save=True):
    logging.info(f"Getting city from GeoApify for [{lat}, {lon}]")
    r = get_geoapify(lat, lon)
    city, country = None, None
    if "features" in r and len(r["features"]) > 0:
        if "city" in r["features"][0]["properties"]:
            city = r["features"][0]["properties"]["city"]
        elif "district" in r["features"][0]["properties"]:
            city = r["features"][0]["properties"]["district"]
        elif "county" in r["features"][0]["properties"]:
            city = r["features"][0]["properties"]["county"]
        else:
            logging.info(f'no "city" in properties for lat/lon: {lat}, {lon}')

        if "country" in r["features"][0]["properties"]:
            country = r["features"][0]["properties"]["country"]
    else:
        logging.debug(f"problem retrieving city for lat/lon: {lat}, {lon}")

    if country and city:
        # add to known cities
        add_known_cities(lat, lon, city, country)

    return city, country


def find_from_name(name):
    logging.info(f"Getting geoloc from string [{name}]")

    resp = requests.get(
        GEOAPIFY_SEARCH_URL,
        headers=HEADERS,
        params={"text": name, "apiKey": "d548c5ed24604be6a9dd0d989631f783"},
    )
    return resp.json()


def get_city(row):
    if not pd.isna(row[CITY]):
        return row[CITY]

    elif (
        not pd.isna(row[LAT])
        and not pd.isna(row[LON])
        and not math.isinf(row[LAT])
        and not math.isinf(row[LON])
    ):
        file_city = get_saved_city(row[LAT], row[LON])
        if file_city:
            return file_city
        return get_geoapify_info(row[LAT], row[LON])[0]


def get_country(row):
    if not pd.isna(row[COUNTRY]):
        return row[COUNTRY]

    elif (
        not pd.isna(row[LAT])
        and not pd.isna(row[LON])
        and not math.isinf(row[LAT])
        and not math.isinf(row[LON])
    ):
        file_city = get_saved_city(row[LAT], row[LON])
        if file_city:
            return file_city
        return get_geoapify_info(row[LAT], row[LON])[1]


def get_saved_city(lat, lon):
    global cities_df
    coord = (round(lat, COORDINATE_PRECISION), round(lon, COORDINATE_PRECISION))
    if coord in cities_df.index:
        return cities_df.loc[coord][CITY].strip()
    else:
        return None


def add_known_cities(lat, lon, city, country):
    coord = (round(lat, COORDINATE_PRECISION), round(lon, COORDINATE_PRECISION))
    global cities_df
    cities_df.loc[coord, :] = [city, country]
    cities_df.to_csv(KNOWN_CITIES)

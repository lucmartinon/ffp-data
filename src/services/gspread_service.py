import csv
import logging
import os

import gspread
import pandas as pd
from dotenv import load_dotenv

from src import constants

load_dotenv()

CREDENTIALS_FP = os.getenv("CREDENTIALS_FP")
SA = None


def get_service_account():
    global SA
    if SA is None:
        SA = gspread.service_account(filename=CREDENTIALS_FP)
    return SA


def get_ss_from_ss_url(url):
    id = url.split("/")[5]
    return get_service_account().open_by_key(id)


def get_ws_from_spreadsheet_url(url):
    id = url.split("/")[5]
    ss = get_service_account().open_by_key(id)
    gid = 0
    if "#gid=" in url:
        gid = url.split("=")[-1]
    return ss.get_worksheet_by_id(int(gid))


def download_csv_from_spreadsheet_url(url, fp):
    ws = get_ws_from_spreadsheet_url(url)
    with open(fp, "w") as f:
        writer = csv.writer(f)
        writer.writerows(ws.get_all_values())


def get_df_from_spreadsheet_url(url):
    ws = get_ws_from_spreadsheet_url(url)
    records = ws.get_all_records(value_render_option="UNFORMATTED_VALUE")
    return pd.DataFrame(records)


def get_ss_records(gdoc_id: str, gid: int = 0):
    ss = get_service_account().open_by_key(gdoc_id)
    ws = ss.get_worksheet_by_id(int(gid))
    return ws.get_all_records(value_render_option="UNFORMATTED_VALUE")


def get_dataset_ws():
    dataset_ss = get_service_account().open_by_key(constants.datasets_index_doc_id)
    return dataset_ss.get_worksheet_by_id(0)


def import_csv(fp, file_id, folder_id):
    if file_id is None:
        import_new_csv(fp, folder_id=folder_id)
    else:
        reimport_csv(fp, file_id=file_id)


def import_new_csv(fp, folder_id):
    fn = fp.split("/")[-1] if "/" in fp else fp
    if fn.endswith(".csv"):
        fn = fn[:-4]
    file = get_service_account().create(fn, folder_id)
    logging.info(f"created file with id: {file.id}")
    reimport_csv(fp, file.id)


def reimport_csv(fp, file_id):
    with open(fp) as f:
        csv_content = f.read()
        get_service_account().import_csv(file_id=file_id, data=csv_content)


def get_first_sheet_gid(file_id):
    ss = get_service_account().open_by_key(file_id)
    return ss.worksheets()[0].id


def freeze_top_row(file_id, gid):
    if gid is None:
        gid = get_first_sheet_gid(file_id)
    ss = get_service_account().open_by_key(file_id)
    ws = ss.get_worksheet_by_id(int(gid))
    ws.freeze(rows=1)


def add_filters(file_id, gid):
    if gid is None:
        gid = get_first_sheet_gid(file_id)
    ss = get_service_account().open_by_key(file_id)
    ws = ss.get_worksheet_by_id(int(gid))
    ws.set_basic_filter()

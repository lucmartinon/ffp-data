import re

import requests

search_url = "https://pubchem.ncbi.nlm.nih.gov/rest/pug/concepts/name/JSON"
compound_url = "https://pubchem.ncbi.nlm.nih.gov/sdq/sdqagent.cgi?infmt=json&outfmt=json&query={%22select%22:%22*%22,%22collection%22:%22compound%22,%22where%22:{%22ands%22:[{%22cid%22:%22<compound_id>%22}]},%22order%22:[%22cid,asc%22],%22start%22:1,%22limit%22:10,%22width%22:1000000,%22listids%22:0}"


def get_cas_id(to_search):
    coupound_id = get_coumpound_id(to_search)
    if coupound_id:
        coupound = get_compound(coupound_id)
        synonyms = coupound["cmpdsynonym"].split("|")
        for syn in synonyms:
            if re.match(r"^\d{2,7}-\d{2}-\d$", syn):
                return syn


def get_compound(compound_id):
    r = requests.get(url=compound_url.replace("<compound_id>", str(compound_id)))
    res = r.json()
    if "SDQOutputSet" in res and len(res["SDQOutputSet"]) > 0:
        output_set = res["SDQOutputSet"][0]
        if "rows" in output_set and len(output_set["rows"]) > 0:
            return output_set["rows"][0]


def get_coumpound_id(to_search):
    r = requests.get(url=search_url, params={"name": to_search})
    res = r.json()
    if "ConceptsAndCIDs" in res and "CID" in res["ConceptsAndCIDs"]:
        return res["ConceptsAndCIDs"]["CID"][0]


def get_name(to_search):
    coupound_id = get_coumpound_id(to_search)
    coupound = get_compound(coupound_id)
    return coupound["cmpdname"]

import json
import logging
import re

import pandas as pd
from tabulate import tabulate

from src.add_dataset.input_utils import (
    confirm_choice,
    get_input,
    pink,
    print_info,
    validated_input,
)
from src.constants.columns import *
from src.constants.columns import FULL_FORMAT_COLUMNS
from src.constants.csv import COLUMN_SYNONYMS_FP, SUBSTANCE_SYNONYMS_FP, SUBSTANCES_FP
from src.constants.exceptions import FppDataException
from src.constants.value_constants import Type
from src.services import pubchem

column_synonyms = pd.read_csv(COLUMN_SYNONYMS_FP)
substance_synonyms = pd.read_csv(SUBSTANCE_SYNONYMS_FP)
substances = pd.read_csv(SUBSTANCES_FP)

sorted_s = substance_synonyms["synonym"].str.len().sort_values().index
substance_synonyms = substance_synonyms.reindex(sorted_s)
substance_synonyms = substance_synonyms.set_index("synonym", verify_integrity=True)

IGNORE_CAS_ID = "IGNORE"

# TODO put that in a real test
for c in column_synonyms["column"].unique():
    if c not in VALID_SOURCE_COLUMNS:
        print(c)
    assert c in VALID_SOURCE_COLUMNS
column_synonyms = column_synonyms.set_index("synonym", verify_integrity=True)
substances = substances.set_index("CAS Number", verify_integrity=True)


def get_pfas_cas_list():
    return list(substances.index)


def get_substance_name(cas_id, search_pubchem=False):
    if cas_id in substances.index:
        return substances.loc[cas_id]["Chemical Name"]
    elif search_pubchem:
        return f"Not (yet) in our substance DB, PubChem name is [{pubchem.get_name(cas_id)}]"
    if cas_id != IGNORE_CAS_ID:
        logging.error(f"Unknown substance [{cas_id}]")


def get_substance_acronym(cas_id):
    if cas_id in substances.index:
        return substances.loc[cas_id]["Acronym"]
    else:
        return None


def add_column_synonym(synonym, column):
    column_synonyms.loc[synonym] = column
    column_synonyms.to_csv(COLUMN_SYNONYMS_FP)
    print_info(f"   columns titled {pink(synonym)} will now be seen as {pink(column)}")


def add_substance_synonym(synonym, cas_id, isomer):
    substance_synonyms.loc[synonym] = [cas_id, isomer]
    substance_synonyms.to_csv(SUBSTANCE_SYNONYMS_FP)
    print_info(f"   New substance synonym: [{synonym}] > ({cas_id}, {isomer})")


def add_substance(cas_id, name):
    substances.loc[cas_id] = [
        name,
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
    ]
    substances.to_csv(SUBSTANCES_FP)
    print_info(f"   New substance: [{cas_id}] > ({name})")


def register_substance_synonym(synonym):
    synonym = synonym.strip()
    cas_id = None

    guesses = guess_cas_id(synonym)
    if len(guesses) > 0:
        print(f"From local data, {pink(synonym)} could correspond to:")
        cas_id = chose_cas_in_table(guesses)

    if cas_id is None:
        pubchem_cas_id = pubchem.get_cas_id(synonym)

        if pubchem_cas_id:
            print(f"After searching in Pub Chem, {pink(synonym)} could correspond to:")
            cas_id = chose_cas_in_table([pubchem_cas_id])
            if cas_id is not None and cas_id not in substances.index:
                if confirm_choice(
                    f"Are you sure you want to add a new substance with CAS {pink(cas_id)}?"
                ):
                    name = pubchem.get_name(cas_id)
                    add_substance(cas_id, name)
                else:
                    cas_id = None
        else:
            print(f"No substance in pubchem called {pink(synonym)}")
    if cas_id is None:
        cas_id = input_cas_id(synonym)

    isomer = ""
    if (
        "branch" in synonym.lower()
        or "lin" in synonym.lower()
        or "ramif" in synonym.lower()
    ):
        isomer = validated_input(
            "Please enter the isomer (linear or branched), or leave empty",
            valid_values=["linear", "branched", ""],
        )
    add_substance_synonym(synonym, cas_id, isomer)


def chose_cas_in_table(possible_cas_ids):
    substances = [get_substance_name(g, search_pubchem=False) for g in possible_cas_ids]
    acronyms = [get_substance_acronym(g) for g in possible_cas_ids]
    possible_cas_ids = possible_cas_ids + [IGNORE_CAS_ID]
    print(
        tabulate(
            {
                "Choice": range(len(possible_cas_ids)),
                "CAS ID": possible_cas_ids,
                "Substance": substances + [""],
                "Acronym": acronyms + [""],
            },
            headers="keys",
            tablefmt="rounded_outline",
        )
    )

    choice = validated_input(
        "Pick one or enter nothing to manually enter a CAS ID",
        valid_values=[str(_) for _ in list(range(len(possible_cas_ids)))] + [""],
    )
    if choice != "":
        cas_id = possible_cas_ids[int(choice)]
        return cas_id
    return None


def check_all_substances_identificable(param_serie):
    param_serie = pd.Series(param_serie.str.strip().unique())
    nips = []
    no_syn_list = param_serie[
        param_serie.apply(identify_substance) == (None, None, None)
    ]
    for subst in no_syn_list:
        subst = str(subst).strip()
        nips.append(subst)

    if len(nips) > 0:
        print(f"{len(nips)} unknown substance names, this will be fun!")
    for nip in nips:
        register_substance_synonym(nip)


def identify_substance(substance_str):
    substance_str = str(substance_str)
    substance_str = substance_str.strip()
    if substance_str in substance_synonyms.index:
        syn = substance_synonyms.loc[substance_str]
        cas_id = syn[CAS_ID]
        isomer = syn[ISOMER]
        if pd.isna(isomer):
            isomer = None

        # substance will be like [PFOS] or [PFOS_branched] if there is an acronym,
        # else it will be like "Methyl Perfluorotetradecanoate [branched]"
        substance = get_substance_acronym(cas_id)
        if not pd.isna(isomer):
            substance = f"{substance}_{isomer}"
        if substance is None:
            substance = get_substance_name(cas_id)
            if not pd.isna(isomer):
                substance = f"{substance} [{isomer}]"

        return cas_id, isomer, substance
    else:
        cas_re_matches = re.findall(r"\d{2,7}-\d{2}-\d", substance_str)
        if len(cas_re_matches) == 1:
            cas = cas_re_matches[0]
            if cas in substances.index:
                return cas, None, get_substance_acronym(cas)

        cas_no_dash_re_matches = re.findall(r"^\d{5,10}$", substance_str)
        if len(cas_no_dash_re_matches) == 1:
            cas_no_dash = cas_no_dash_re_matches[0]
            cas = f"{cas_no_dash[:-3]}-{cas_no_dash[-3:-1]}-{cas_no_dash[-1:]}"
            if cas in substances.index:
                return cas, None, get_substance_acronym(cas)

    parenthesis = re.findall(r"\(.+\)$", substance_str)
    if len(parenthesis) == 1:
        parenthesis_content = parenthesis[0][1:-1]
        # logging.info(f"No result for {substance_str}, trying with parenthesis: {parenthesis_content}")
        return identify_substance(str(parenthesis_content))
    return None, None, None


def guess_cas_id(synonym):
    synonym = synonym.strip()
    # First we try to make a proposition if we find a partial match
    guesses = set()

    for cas_id in substances.index:
        acronym = substances.loc[cas_id]["Acronym"]
        if not pd.isna(acronym) and acronym.lower() in synonym.lower():
            guesses.add(cas_id)
    for k in substance_synonyms.index:
        cas_id = substance_synonyms.loc[k][CAS_ID]
        if str(k).lower() in synonym.lower():
            guesses.add(cas_id)
    return list(guesses)


def input_cas_id(synonym):
    cas_id = get_input(
        f"What is the {pink('CAS-ID')} of substance called {pink(synonym)} (or type IGNORE)?"
    )
    if cas_id in substances.index or cas_id == IGNORE_CAS_ID:
        return cas_id
    else:
        if confirm_choice(
            f"Are you sure you want to add a new substance with CAS {pink(cas_id)}?"
        ):
            name = pubchem.get_name(cas_id)
            add_substance(cas_id, name)
            return cas_id
        return input_cas_id(synonym)


def identify_column(col):
    if col.lower() in FULL_FORMAT_COLUMNS + [COORD_X, COORD_Y]:
        return col.lower()
    if col in substance_synonyms.index:
        return SL_VALUE
    if col in column_synonyms.index:
        return column_synonyms.loc[col]["column"]
    if re.match(r"^\d{2,7}-\d{2}-\d$", col):
        if col in substances.index:
            return SL_VALUE


def guess_column(col):
    if "date" in col:
        return DATE

    guesses = guess_cas_id(col)
    if len(guesses) > 0:
        return SL_VALUE

    for vc in FULL_FORMAT_COLUMNS:
        if vc in col:
            return vc


def infer_columns(columns) -> dict:
    res = {}
    non_identified = []
    for c in columns:
        identified = identify_column(c)
        if identified:
            res[c] = identified
        else:
            non_identified.append(c)

    print_info(
        f"we identified {len(res)}/{len(columns)} columns. We will try to guess the rest."
    )

    for nic in non_identified:
        guessed = guess_column(nic)
        if guessed and confirm_choice(
            f"We guessed column {pink(nic)} to be {pink(guessed)}, is that correct? Y/N"
        ):
            res[nic] = guessed
        else:
            custom = validated_input(
                f"We couldn't guess the column for {pink(nic)}. "
                'Please enter the column, or "drop" or "keep"\n',
                VALID_SOURCE_COLUMNS,
            )
            res[nic] = custom

        if res[nic] == SL_VALUE or confirm_choice(
            f"Should we add {pink(nic)} as a synonym for {pink(res[nic])} for future datasets? Y/N"
        ):
            add_column_synonym(synonym=nic, column=res[nic])

        if res[nic] == SL_VALUE:
            if identify_substance(nic)[0] is None:
                register_substance_synonym(nic)

    print(
        tabulate(
            {"Raw name": res.keys(), "Column Type": [_.upper() for _ in res.values()]},
            headers="keys",
            tablefmt="rounded_outline",
        )
    )

    if not confirm_choice(
        "We inferred all columns as marked in the pretty table above, are they correct? Y/N"
    ):
        raise FppDataException("Column not correctly inferred, stopping")
    return res


def get_static_values(columns: dict, dataset):
    possible_static_columns = set(STATIC_COLUMNS) - set(columns.values())
    # if there is a column date, don't offer to put a year static value
    if DATE in columns.values():
        possible_static_columns = possible_static_columns - {YEAR}

    static_columns = {}
    for psc in possible_static_columns:
        if dataset is not None and psc in dataset and dataset[psc].strip() != "":
            static_columns[psc] = dataset[psc]
        else:
            if psc == NAME:
                sv = "Sampling location"
            if psc == TYPE:
                sv = Type.SAMPLING_LOCATION.value
            elif psc == SECTOR:
                sv = ""
            elif psc in MANDATORY_COLUMNS:
                sv = get_input(f"Please enter a static value for {pink(psc)}:")
            else:
                if psc in LIST_COLUMNS:
                    sv = validated_input(
                        f"Please enter a static value for {pink(psc)}, or nothing if unwanted:",
                        valid_values=[e.value for e in LIST_COLUMNS[psc]] + [""],
                    )
                else:
                    sv = get_input(
                        f"Please enter a static value for {pink(psc)}, or nothing if unwanted:"
                    )
            if sv != "":
                static_columns[psc] = sv

    print_info(
        f"Good. Static values are as follow: \n{json.dumps(static_columns, indent=4)}"
    )

    return static_columns

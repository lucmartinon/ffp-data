import glob
import logging
import os.path

import pandas as pd

from src.add_dataset.index import IC, get_index_dataset
from src.constants.columns import *
from src.constants.csv import OLD
from src.normalise.sl_normaliser import SLNormaliser
from src.synonyms_utils import identify_column


class OldNormaliser(SLNormaliser):
    @property
    def dataset_id(self):
        return self._dataset_id

    @property
    def dataset_name(self):
        return f"[OLD] {self.index_dataset[IC.NAME]}".replace("/", "-")

    @property
    def csv_fn(self):
        return f"{self.dataset_id}__{self.dataset_name}.csv"

    def __init__(self, dataset_id):
        self._dataset_id = dataset_id
        self.index_dataset = get_index_dataset(dataset_id + 2)
        self.old_id = self.index_dataset[IC.ID]
        pattern = os.path.join(OLD, f"{self.old_id}_from_*")
        self.old_fp = glob.glob(pattern)[0]
        logging.info(f"For [{dataset_id}], old fp is {self.old_fp}")
        self.raw_df = pd.read_csv(self.old_fp)
        self.raw_df = self.raw_df.drop(columns=["Unnamed: 0"])
        self.columns = {}
        for c in self.raw_df.columns:
            if c.lower() in FULL_FORMAT_COLUMNS + [COORD_X, COORD_Y]:
                self.columns[c] = c.lower()
            else:
                identified_col_type = identify_column(c)
                if identified_col_type == SL_VALUE:
                    self.columns[c] = SL_VALUE
                else:
                    self.columns[c] = KEEP

        self.static_values = {}
        for c in [COUNTRY, CATEGORY, SOURCE_TYPE, SOURCE_TEXT, SOURCE_URL]:
            if self.index_dataset[c] not in ["", "Europe"]:
                self.static_values[c] = self.index_dataset[IC(c).value]

        super().__init__()

    @property
    def readable_fp(self):
        return self.old_fp

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

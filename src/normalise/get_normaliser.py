import logging

from src.normalise.normaliser import Normaliser
from src.normalise.old_normaliser import OldNormaliser


def get_normaliser(dataset_id, return_old_normaliser=True) -> Normaliser:
    try:
        mod = __import__(
            f"src.normalise.dataset_normalisers.{dataset_id}",
            fromlist=[f"Normaliser_{dataset_id}"],
        )
        return getattr(mod, f"Normaliser_{dataset_id}")()

    except ModuleNotFoundError:
        if return_old_normaliser:
            logging.info(f"No normaliser for id [{dataset_id}], using old_normaliser")
            return OldNormaliser(dataset_id)

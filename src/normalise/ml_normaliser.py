import logging
from abc import ABCMeta

import pandas as pd

from src.constants.columns import *
from src.normalise.normaliser import Normaliser
from src.synonyms_utils import (
    IGNORE_CAS_ID,
    check_all_substances_identificable,
    identify_substance,
)

# since it is a temp column for grouping, not putting it in the columns files.
PFAS_VALUE = "pfas_value"


# from src.pfas_helper.pfas_identifier import identify_pfas


class MLNormaliser(Normaliser, metaclass=ABCMeta):
    # unit is used to convert values, unless there is a unit column
    unit: str

    def _rename_cols(self):
        to_rename = {}
        for k, v in self.columns.items():
            if v in set(VALID_SOURCE_COLUMNS) - {KEEP, DROP} and v != k:
                to_rename[k] = v
        if len(to_rename.keys()) > 0:
            self.df = self.df.rename(columns=to_rename)
            logging.info(f"Renamed columns: {to_rename}")

    def _preprocess(self):
        self.df = self.raw_df.copy()

    def _parse_value_from_row(self, row):
        return self._parse_value(row[ML_VALUE])

    def _generate_pfas_value_from_row(self, row):
        param = row[PARAM]
        unit = row[UNIT] if UNIT in row else ""
        float_val, less_than = self._parse_value_from_row(row)
        float_val, less_than, final_unit = self._convert_value(
            float_val, less_than, unit
        )

        cas_id, isomer, substance = identify_substance(param)
        return self._pfas_value(
            cas_id, final_unit, isomer, substance, float_val, less_than
        )

    def _transform(self):
        if self.raw_df is None:
            logging.error("no df to normalise")
            return None
        logging.info(f"Raw data: {self.df.shape[0]} rows")

        self._check_columns()
        self._rename_cols()
        self._map_field_values()

        check_all_substances_identificable(self.df[PARAM].astype(str))
        logging.info(f"All substances in [{PARAM}] can be identified.")
        for param in self.df[PARAM].unique():
            cas_id, _, _ = identify_substance(param)
            if cas_id == IGNORE_CAS_ID:
                self.df = self.df[self.df[PARAM] != param]
        logging.info(f"After getting rid of SUM params: {self.df.shape[0]} rows")

        # drop_duplicate keeps the first value, here the highest since we sorted descending.
        self.df = self.df.sort_values(ML_VALUE, ascending=False)

        possible_dup_col = [COORD_X, COORD_Y, DATE, PARAM, LAT, LON, MATRIX, UNIT]
        dup_col = [_ for _ in possible_dup_col if _ in self.df.columns]
        self.df = self.df.drop_duplicates(dup_col)
        logging.info(
            f"After only keeping the highest values for coord / substance / date / coord / matrix: {self.df.shape[0]} rows"
        )

        # we create the json object
        self.df[PFAS_VALUE] = self.df.apply(self._generate_pfas_value_from_row, axis=1)

        # we remove line with pfas_value None
        self.df = self.df[~pd.isna(self.df[PFAS_VALUE])]

        logging.info(f"After removing null values: {self.df.shape[0]} rows")

        self._drop_useless_cols()

        group_by = []
        for k, v in self.columns.items():
            if v not in [DROP, PARAM, ML_VALUE]:
                if k in self.df.columns:
                    group_by.append(k)
                else:
                    group_by.append(v)

        logging.info(f"grouping by {group_by}")
        grouped_df = self.df.groupby(group_by, dropna=False).agg(
            pfas_values=(PFAS_VALUE, list)
        )

        grouped_df = grouped_df.reset_index()
        logging.info(f"After grouping:  {grouped_df.shape[0]}")

        self.df = grouped_df
        self._add_static_cols()
        self._calculate_sum()
        self._jsonize_pfas_values()
        self._adapt_coordinates()
        self._numerise_lat_lon()
        self._parse_date()
        self._jsonize_keep_cols()
        self._drop_unit_col()
        self._add_missing_cols()
        self._normalise_country_names()
        self.save_normalised_df()
        return self.df

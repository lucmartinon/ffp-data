import logging
from abc import ABCMeta

import pandas as pd

from src.constants.columns import *
from src.normalise.normaliser import Normaliser


class SLNormaliser(Normaliser, metaclass=ABCMeta):
    proj_epsg_id = None
    date_format = None

    def _preprocess(self):
        self.df = self.raw_df.copy()

    def _transform(self):
        if self.raw_df is None:
            logging.error("no df to normalise")
            return None

        logging.info(f"Raw data: {self.df.shape[0]} rows")
        self._check_columns()
        self._drop_useless_cols()
        self._rename_cols()
        self._map_field_values()
        self._add_static_cols()

        # Known datasets
        val_cols = []
        for k, v in self.columns.items():
            if v == SL_VALUE:
                # it could be that the column was already renamed
                c = k if k in self.df.columns else v
                val_cols.append(c)
        logging.info("Parsed numerical values")
        self.df = self.df[
            (self.df[CATEGORY] != VC.Category.KNOWN)
            | (~pd.isnull(self.df[val_cols]).all(1))
            ]
        logging.info(
            f"After removing known rows without values: {self.df.shape[0]} rows"
        )

        if UNIT not in self.columns.values() and self.unit is None:
            self.unit = "ng/l"
            logging.info("No unit provided, we assume it is ng/L")

        # pfas_values
        def list_values(row):
            values = []
            for c in [_ for _ in self.columns.keys() if self.columns[_] == SL_VALUE]:
                if not pd.isna(row[c]):
                    unit = row[UNIT] if UNIT in row else None
                    pv = self._generate_pfas_value_from_value_str(
                        row[c], c, row_unit=unit
                    )
                    if pv:
                        values.append(pv)
            return values

        self.df[PFAS_VALUES] = self.df.apply(list_values, axis=1)

        for col, col_type in self.columns.items():
            if col_type == SL_VALUE:
                self.df = self.df.drop(columns=[col])
        logging.info("jsonized VALUE columns into pfas_values and dropped them.")

        if UNIT not in self.df.columns:
            self.df[UNIT] = self.unit

        self._calculate_sum()
        self._jsonize_pfas_values()

        self._adapt_coordinates()
        self._numerise_lat_lon()
        self._parse_date()
        self._jsonize_keep_cols()
        self._drop_unit_col()
        self._fill_missing_cities()
        self._normalise_country_names()
        self.save_normalised_df()
        return self.df

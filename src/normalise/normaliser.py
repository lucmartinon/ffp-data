import abc
import json
import logging
import os

import OSGridConverter
import pandas as pd
import pyproj
import pytz
from OSGridConverter.base import OSGridError

from src.constants.columns import *
from src.constants.csv import NORMALISED, READABLE
from src.constants.exceptions import FppDataException
from src.constants.value_constants import COUNTRY_NAMES_REPLACE_MAP
from src.extract.extract import get_extractor
from src.normalise.normalise_utils import get_multiplier_from_unit
from src.services.geoapify import get_city
from src.synonyms_utils import identify_substance
from src.utils import get_dataset_name


class Normaliser(metaclass=abc.ABCMeta):
    dataset_id: int
    columns: dict
    static_values: dict
    date_format: str = None
    proj_epsg_id: int = None
    proj_transformer = None
    unit: str = None
    replace_map: dict = None

    fill_missing_cities: bool = True

    raw_df: pd.DataFrame = None
    df: pd.DataFrame

    @property
    def dataset_name(self):
        try:
            return get_dataset_name(self.dataset_id)
        except FppDataException as e:
            return get_extractor(self.dataset_id).dataset_name

    @property
    def csv_fn(self):
        return f"{self.dataset_id}__{self.dataset_name}.csv"

    @property
    def normalised_fp(self):
        return os.path.join(NORMALISED, self.csv_fn)

    @property
    def readable_fp(self):
        return os.path.join(READABLE, self.csv_fn)

    @property
    def should_normalise(self):
        return not os.path.exists(self.normalised_fp) or os.path.getmtime(self.normalised_fp) < os.path.getmtime(
            self.readable_fp)

    @property
    def dataset_id(self):
        cname = self.__class__.__name__
        if "_" in cname:
            return int(cname.split("_")[1])

    @staticmethod
    @abc.abstractmethod
    def _parse_value(value_str: str) -> (float, bool):
        """
        :param value_str:
        :return: a tuple containing
            the parsed values as a float
            a boolean indicating if the value is below LOQ
        """
        pass

    def _parse_and_convert_value(self, value_str, row_unit=None):
        float_val, less_than = self._parse_value(value_str)
        return self._convert_value(float_val, less_than, row_unit)

    def _convert_value(self, float_val, less_than, row_unit=None):
        if float_val:
            unit_to_use = row_unit if self.unit is None else self.unit
            multiplier, final_unit = get_multiplier_from_unit(unit_to_use)
            return multiplier * float_val, less_than, final_unit
        return None, None, None

    def _pfas_value(self, cas_id, unit, isomer, substance, float_val, less_than):
        pv = {CAS_ID: cas_id, UNIT: unit, ISOMER: isomer, SUBSTANCE: substance}

        if float_val:
            if less_than:
                pv[LESS_THAN] = float_val
                pv[VALUE] = None

            else:
                pv[LESS_THAN] = None
                pv[VALUE] = float_val
            return pv
        else:
            return None

    def _generate_pfas_value_from_value_str(
            self, value_str, substance_name, row_unit=None
    ):
        cas_id, isomer, substance = identify_substance(substance_name)
        if cas_id is None:
            raise FppDataException(f"Substance [{substance_name}] is unknown!")
        float_val, less_than, final_unit = self._parse_and_convert_value(
            str(value_str), row_unit
        )
        # print(f"{value_str} {row_unit} > {float_val} {final_unit}")

        return self._pfas_value(
            cas_id, final_unit, isomer, substance, float_val, less_than
        )

    def _read_raw_df(self):
        if self.raw_df is None:
            self.raw_df = pd.read_csv(self.readable_fp, low_memory=False)

    @abc.abstractmethod
    def _preprocess(self):
        pass

    @abc.abstractmethod
    def _transform(self):
        pass

    def _check_columns(self):
        for c in self.df.columns:
            if c not in self.columns:
                logging.error(f"Column [{c}] in readable csv is not described in the normaliser")

        for c, v in self.columns.items():
            if c not in self.df.columns:
                logging.error(f"Column [{c}] is described in the normaliser but not in the readable csv")

    def normalise(self, force=False):
        if self.should_normalise or force:
            logging.info(f"Normalising [{self.dataset_id}] {self.dataset_name}")
            self._read_raw_df()
            self._preprocess()
            self._transform()
            logging.info(f"Normalised [{self.dataset_id}] {self.dataset_name}: {self.df.shape[0]} rows")
            return self.df
        else:
            logging.info(f"Skipping [{self.dataset_id}] {self.dataset_name}")

    def _map_field_values(self):
        # see example of field_values_maps in normaliser 124
        if self.replace_map:
            for field, map in self.replace_map.items():
                self.df[field] = self.df[field].map(map)
                logging.info(f"Mapped values of field [{field}] using the provided map")

    def _drop_useless_cols(self):
        # drop useless columns
        to_drop = [c for c in self.columns if self.columns[c] == DROP]
        self.df = self.df.drop(columns=to_drop)
        if len(to_drop) > 0:
            logging.info(f"dropped useless columns: {to_drop}")

    def _drop_unit_col(self):
        # drop useless columns
        if UNIT in self.df.columns:
            self.df = self.df.drop(columns=[UNIT])
            logging.info("dropped unit column (info is in pfas_values now)")

    def _rename_cols(self):
        to_rename = {}
        for k, v in self.columns.items():
            if v in set(VALID_SOURCE_COLUMNS) - {KEEP, ML_VALUE, SL_VALUE} and v != k:
                to_rename[k] = v
        if len(to_rename.keys()) > 0:
            self.df = self.df.rename(columns=to_rename)
            logging.info(f"Renamed columns: {to_rename}")

    def _add_static_cols(self):
        self.df["dataset_id"] = self.dataset_id
        self.df["dataset_name"] = self.dataset_name

        if self.static_values:
            for k, v in self.static_values.items():
                if k not in self.df.columns:
                    self.df[k] = v
                    logging.info(f"Added static col {k}: {v}")

    def _adapt_coordinates(self):
        # adapt coordinates
        if self.proj_epsg_id:
            self.df[LAT] = self.df.apply(
                lambda row: self._transform_coord(row)[1], axis=1
            )
            self.df[LON] = self.df.apply(
                lambda row: self._transform_coord(row)[0], axis=1
            )
            logging.info(
                f"Transposed coordinates to GPS, using EPSG ID [{self.proj_epsg_id}]"
            )

        if UK_GRID_REFERENCE in self.df.columns:

            def get_ll(uk_grid_ref):
                try:
                    return OSGridConverter.grid2latlong(uk_grid_ref)
                except OSGridError:
                    logging.info(f"problem for {uk_grid_ref}")
                    return None

            def get_lat(uk_grid_ref):
                ll = get_ll(uk_grid_ref)
                if ll:
                    return ll.latitude

            def get_lon(uk_grid_ref):
                ll = get_ll(uk_grid_ref)
                if ll:
                    return ll.longitude

            self.df[LAT] = self.df[UK_GRID_REFERENCE].map(get_lat)
            self.df[LON] = self.df[UK_GRID_REFERENCE].map(get_lon)
            # we rename the column back to its original name and mark the column as KEEP
            uk_col_name = [
                c for c, t in self.columns.items() if t == UK_GRID_REFERENCE
            ][0]
            self.df = self.df.rename(columns={UK_GRID_REFERENCE: uk_col_name})
            self.columns[uk_col_name] = KEEP

    def _transform_coord(self, row) -> (float, float):
        if self.proj_transformer is None:
            self.proj_transformer = pyproj.Transformer.from_crs(
                self.proj_epsg_id, 4326, always_xy=True
            )
        if not pd.isna(row[COORD_X]) and not pd.isna(row[COORD_Y]):
            latlon = self.proj_transformer.transform(row[COORD_X], row[COORD_Y])
            return round(latlon[0], 5), round(latlon[1], 5)
        return None, None

    def _parse_date(self):
        # date
        if DATE in self.df.columns:
            example = self.df.iloc[0][DATE]
            if self.df.dtypes[DATE] == "int64":
                # If dates are an int type, it's an Excel format
                self.df[DATE] = pd.to_datetime(
                    self.df[DATE], unit="D", origin="1899-12-30"
                )
                logging.info(
                    f"converted date to datetime based on excel format, exemple first row "
                    f"{example} > {self.df.iloc[0][DATE]}"
                )

            else:
                example = self.df.iloc[0][DATE]
                self.df[DATE] = pd.to_datetime(self.df[DATE], format=self.date_format)
                logging.info(
                    f"converted date to datetime, exemple {example} > {self.df.iloc[0][DATE]}"
                )
            if YEAR not in self.df.columns:
                self.df[YEAR] = pd.DatetimeIndex(self.df[DATE], tz=pytz.utc).year
                logging.info("Added column year based on date column")

    def _details(self, row):
        details = {}
        for col, col_type in self.columns.items():
            if col_type == KEEP:
                if not pd.isna(row[col]):
                    details[col] = row[col]

        for col in [COORD_X, COORD_Y]:
            if col in row:
                details[col] = row[col]
        return json.dumps(details)

    def _jsonize_keep_cols(self):
        self.df[DETAILS] = self.df.apply(self._details, axis=1)

        # removing KEEP columns
        for col, col_type in self.columns.items():
            if col_type == KEEP:
                self.df = self.df.drop(columns=[col])

        for col in [COORD_X, COORD_Y]:
            if col in self.df.columns:
                self.df = self.df.drop(columns=[col])

        logging.info("jsonized KEEP columns in new col [details] ; then removed them")

    def _calculate_sum(self):
        self.df = self.df[
            (self.df[CATEGORY] != VC.Category.KNOWN)
            | (self.df[PFAS_VALUES].map(len) > 0)
            ]
        logging.info(f"After removing lines with no values:  {self.df.shape[0]}")

        # first we complete list of values so that all non isomers values are there
        self.df[PFAS_VALUES] = self.df[PFAS_VALUES].apply(self._complete_pfas_values)

        # then we sum all non isomers values.
        self.df[PFAS_SUM] = self.df[PFAS_VALUES].apply(sum_pfas_values)
        pass

    def _jsonize_pfas_values(self):
        self.df[PFAS_VALUES] = self.df[PFAS_VALUES].apply(json.dumps)

    def _add_missing_cols(self):
        # adding missing standard columns
        for c in FULL_FORMAT_COLUMNS:
            if c not in self.df.columns:
                self.df[c] = None
                logging.info(f"Added missing column [{c}]")

    def _numerise_lat_lon(self):
        if LAT in self.df.columns:
            self.df[LAT] = numerise_series(self.df[LAT])
            self.df[LON] = numerise_series(self.df[LON])

    def _fill_missing_cities(self):
        if CITY not in self.df.columns:
            self.df[CITY] = None
        if self.fill_missing_cities:
            if LAT in self.df.columns:
                self.df[CITY] = self.df.apply(lambda row: get_city(row), axis=1)
                logging.info("Added missing cities")

    def save_normalised_df(self):
        if self.df is not None:
            self.df = self.df.round(6)
            self.df.to_csv(self.normalised_fp, index=False, date_format="%Y-%m-%d")
        else:
            logging.info("Cannot save: there is no normalised_df")

    def _complete_pfas_values(self, pfas_values: list):
        isomers, non_isomers_cas_ids = [], []
        for pv in pfas_values:
            if not pd.isna(pv[ISOMER]):
                # we take only the ones with values above LOQ
                if pv[VALUE]:
                    isomers.append(pv)
            else:
                non_isomers_cas_ids.append(pv[CAS_ID])

        for pv in isomers:
            if pv[CAS_ID] not in non_isomers_cas_ids:
                sum_val = sum(
                    [_[VALUE] for _ in isomers if _[VALUE] and _[CAS_ID] == pv[CAS_ID]]
                )
                sum_pv = self._pfas_value(
                    cas_id=pv[CAS_ID],
                    unit=pv[UNIT],
                    isomer=None,
                    substance=pv[SUBSTANCE].split("_")[0],
                    float_val=sum_val,
                    less_than=False,
                )
                pfas_values.append(sum_pv)
                # so that it gets added only once
                non_isomers_cas_ids.append(pv[CAS_ID])
        return pfas_values

    def _normalise_country_names(self):
        self.df[COUNTRY] = self.df[COUNTRY].replace(COUNTRY_NAMES_REPLACE_MAP)


def sum_pfas_values(pfas_values: list):
    if len(pfas_values) == 0:
        return None
    return sum(
        [_[VALUE] for _ in pfas_values if pd.isna(_[ISOMER]) and not pd.isna(_[VALUE])]
    )


def numerise_series(s: pd.Series):
    try:
        return pd.to_numeric(s)
    except ValueError:
        return pd.to_numeric(s.str.replace(",", "."))

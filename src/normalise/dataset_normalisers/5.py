from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_5(SLNormaliser):
    static_values = {CATEGORY: "Presumptive", SOURCE_TYPE: "OSINT"}
    columns = {
        "category": CATEGORY,
        "lat": LAT,
        "lon": LON,
        "type": TYPE,
        "name": NAME,
        "city": CITY,
        "country": COUNTRY,
        "sector": SECTOR,
        "Activity": KEEP,
        "site": KEEP,
        "source_type": SOURCE_TYPE,
        "source_text": SOURCE_TEXT,
        "source_url": SOURCE_URL,
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

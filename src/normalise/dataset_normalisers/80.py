import logging

import pandas as pd
import pyproj

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_80(MLNormaliser):
    proj_epsg_id = 3346
    columns = {
        "Monitoring station": KEEP,
        "Latitude": LAT,
        "Result": ML_VALUE,
        "Unit": UNIT,
        "Place of sampling": NAME,
        "Longtitude": LON,
        "Date, time of sampling": DATE,
        "Sample type": KEEP,
        "Group of substances": DROP,
        "Substances": PARAM,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        CATEGORY: "Known",
        SOURCE_TEXT: "Lithuanian Environmental Protection Agency",
        SOURCE_URL: "https://aad.lrv.lt/",
        TYPE: "Sampling location",
        MATRIX: "Biota",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Lithuania",
    }

    # we implement the coord adapt because the column is a mix of lat / lon and coord in 3346

    def _adapt_coordinates(self):
        # adapt coordinates
        if self.proj_epsg_id:
            self.df[LAT] = self.df[LAT].str.replace(",", ".")
            self.df[LON] = self.df[LON].str.replace(",", ".")
            self.df[LAT] = pd.to_numeric(self.df[LAT])
            self.df[LON] = pd.to_numeric(self.df[LON])

            self.df["lonlat"] = self.df.apply(
                lambda row: self._transform_coord(row), axis=1
            )
            self.df[LON] = self.df["lonlat"].str[0]
            self.df[LAT] = self.df["lonlat"].str[1]
            self.df = self.df.drop(columns=["lonlat"])

            logging.info(
                f"Transposed coordinates to GPS, using EPSG ID [{self.proj_epsg_id}]"
            )

    def _transform_coord(self, row) -> (float, float):
        if self.proj_transformer is None:
            self.proj_transformer = pyproj.Transformer.from_crs(
                self.proj_epsg_id, 4326, always_xy=True
            )

        if row[LAT] > 90 and row[LON] > 90:
            lonlat = self.proj_transformer.transform(row[LON], row[LAT])
            return round(lonlat[0], 5), round(lonlat[1], 5)
        return row[LON], row[LAT]

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_66(SLNormaliser):
    columns = {
        "STATION CODE": KEEP,
        "Year": YEAR,
        "Sampling Date": DATE,
        "PFPeA (ng/l)": SL_VALUE,
        "PFHxA (ng/l)": SL_VALUE,
        "PFHxS (ng/l)": SL_VALUE,
        "PFNA (ng/l)": SL_VALUE,
        "PFBS (ng/l)": SL_VALUE,
        "PFOA (ng/l)": SL_VALUE,
        "PFOS (ng/l)": SL_VALUE,
        "PFBA (ng/l)": SL_VALUE,
        "PFHpA (ng/l)": SL_VALUE,
        "PFDeA (ng/l)": SL_VALUE,
        "PFUnA (ng/l)": SL_VALUE,
        "PFDoA (ng/l)": SL_VALUE,
        "PFHpS (ng/l)": SL_VALUE,
        "PFOA - isomeri ramificati (ng/l)": SL_VALUE,
        "PFOS - isomeri ramificati (ng/l)": SL_VALUE,
        "HFPO-DA (ng/l)": SL_VALUE,
        "4:2 FTS  (ng/l)": SL_VALUE,
        "6:2 FTS (ng/l)": SL_VALUE,
        "8:2 FTS (ng/l)": SL_VALUE,
        "cC604 (ng/l)": SL_VALUE,
        "Location": NAME,
        "Type": KEEP,
        "Municipality": CITY,
        "Hydric Body Code": KEEP,
        "Coordinate X WGS84": COORD_X,
        "Coordinate Y WGS84": COORD_Y,
        "Type of use": KEEP,
        "Hydrogeological complex": KEEP,
        "Groundwater Body Name": KEEP,
    }
    date_format = "%Y-%m-%d"
    static_values = {
        SOURCE_TYPE: "Authorities",
        SOURCE_URL: "Press request",
        COUNTRY: "Italy",
        SOURCE_TEXT: "ARPA Umbria",
        MATRIX: "Groundwater",
        CATEGORY: "Known",
        TYPE: "Sampling location",
    }
    proj_epsg_id = 3065

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

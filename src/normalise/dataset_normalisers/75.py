from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_75(MLNormaliser):
    columns = {
        "Codice stazione": KEEP,
        "Data prelievo": DATE,
        "Valore": ML_VALUE,
        "UM": UNIT,
        "Ambiente": KEEP,
        "Matrice": MATRIX,
        "Nome stazione": NAME,
        "Codice campione": DROP,
        "Sostanza": PARAM,
        "coordinate x (LONG)": LON,
        "coordinate y (LAT)": LAT,
        "Unnamed: 11": DROP,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        CATEGORY: "Known",
        SOURCE_URL: "Press request",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Italy",
        TYPE: "Sampling location",
        SOURCE_TEXT: "ARPA Sardegna",
    }

    replace_map = {MATRIX: {"Acque": Matrix.SURFACE_WATER, "Biota": Matrix.BIOTA}}

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_54(SLNormaliser):
    unit = "ng/l"
    columns = {
        "Provincia": KEEP,
        "Comune": CITY,
        "Data del prelievo": DATE,
        "Tipologia di acqua campionata": MATRIX,
        "Latitudine": LAT,
        "Longitudine": LON,
        "PFBA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFPeS": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFHpS": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFUnDA": SL_VALUE,
        "PFUnS": SL_VALUE,
        "PFDoDA": SL_VALUE,
        "PFDoS": SL_VALUE,
        "PFTrDA": SL_VALUE,
        "PFTrDS": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFOA linear": SL_VALUE,
        "PFOA branched": SL_VALUE,
        "PFOA.1": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFOS linear": SL_VALUE,
        "PFOS branched": SL_VALUE,
        "PFOS.1": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFNS": SL_VALUE,
        "PFDS": SL_VALUE,
        "PMHpA": DROP,
        "PMHpS": DROP,
        "ADONA": SL_VALUE,
        "HFPO-DA": SL_VALUE,
        "4:2-FTS": SL_VALUE,
        "6:2-FTS": SL_VALUE,
        "8:2-FTS": SL_VALUE,
        "C6O4": SL_VALUE,
        "cC6O4(CAS 1190931-27-1)(ng/L)cC6O4": SL_VALUE,
        "cC6O4(CAS 1190931-41-9)(ng/L)cC6O4": SL_VALUE,
        "Cl-PFPECA (0,1)": DROP,
        "Cl-PFPECA (0,2)": DROP,
        "Cl-PFPECA (0,3)": DROP,
        "Cl-PFPECA (0,4)": DROP,
        "Cl-PFPECA (1,0)": DROP,
        "Cl-PFPECA (1,1)": DROP,
        "Cl-PFPECA (2,0)": DROP,
        "Cl-PFPECA (2,1)": DROP,
        "Cl-PFPECA (3,0)": DROP,
        "Cl-PFPECA (4,0)": DROP,
        "Somma CL-PFPECA(CAS 329238-24-6)(ng/L)": SL_VALUE,
    }
    static_values = {
        SOURCE_URL: "https://www.arpa.veneto.it/dati-ambientali/open-data/idrosfera/concentrazione-di-sostanze-perfluoroalchiliche-pfas-nelle-acque-prelevate-da-arpav",
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known",
        COUNTRY: "Italy",
        TYPE: "Sampling location",
        NAME: "Sampling location",
        SOURCE_TEXT: "ARPA Veneto",
    }

    replace_map = {
        MATRIX: {
            "SUPERFICIALI": "Surface water",
            "SORGENTI O RISORGIVE": "Surface water",
            "SOTTERRANEE": "Groundwater",
        }
    }

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False

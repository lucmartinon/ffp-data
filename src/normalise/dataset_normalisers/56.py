from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_56(SLNormaliser):
    unit = "μg/L"
    columns = {
        "Station Code": KEEP,
        "Date": DATE,
        "Name": NAME,
        "PFPeA μg/L": SL_VALUE,
        "PFHxA μg/L": SL_VALUE,
        "PFHpA μg/L": SL_VALUE,
        "PFOA μg/L": SL_VALUE,
        "PFNA μg/L": SL_VALUE,
        "PFDA μg/L": SL_VALUE,
        "PFUnA μg/L": SL_VALUE,
        "PFDoA μg/L": SL_VALUE,
        "PFBS μg/L": SL_VALUE,
        "PFOS μg/L": SL_VALUE,
        "Coord.": LAT,
        "Coord..1": LON,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        TYPE: "Sampling location",
        CATEGORY: "Known",
        MATRIX: "Surface water",
        COUNTRY: "Italy",
        SOURCE_TEXT: "Istituto di Ricerca sulle Acque – CNR",
        SOURCE_TYPE: "Authorities",
    }

    @staticmethod
    def _parse_value(val: str):
        if val == "<DL":
            return None, None
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False

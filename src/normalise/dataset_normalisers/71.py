from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_71(MLNormaliser):
    columns = {
        "Codice Stazione": KEEP,
        "Nome Parametro": PARAM,
        "Unità di misura": UNIT,
        "Data ": DATE,
        "Concentrazione": ML_VALUE,
        "REGIONE": KEEP,
        "TIPO_STAZIONE": MATRIX,
        "COMUNE": CITY,
        "CODICE_COMUNE": KEEP,
        "STAZ_LATITUDE (ETRS_89_EPSG_4258)": LAT,
        "STAZ_LONGITUDE  (ETRS_89_EPSG_4258)": LON,
        "Codice CAS": DROP,
        "LOCALITA": DROP,
    }
    date_format = "ISO8601"
    static_values = {
        NAME: "Sampling location",
        TYPE: "Sampling location",
        SOURCE_TEXT: "ARPA Sicilia",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Italy",
        SOURCE_URL: "Press request",
        CATEGORY: "Known",
    }

    replace_map = {MATRIX: {"SW": Matrix.SURFACE_WATER, "GW": Matrix.GROUNDWATER}}

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_96(MLNormaliser):
    columns = {
        "Station Number": KEEP,
        "Station Name": NAME,
        "Local Authority": KEEP,
        "Easting": COORD_X,
        "Northing": COORD_Y,
        "NGR": KEEP,
        "Station Type": KEEP,
        "Sampling Medium": MATRIX,
        "Sampling Mechanism": KEEP,
        "Sampling Reason": KEEP,
        "Sampling DateTime": DATE,
        "Method Name": KEEP,
        "Parameter Shortname": DROP,
        "Parameter Name": PARAM,
        "Sign": DROP,
        "Sample Value": ML_VALUE,
        "Unit Name": UNIT,
        "Unit Symbol": DROP,
        "Caveat": DROP,
    }
    date_format = "mixed"  # '%d/%m/%y %H:%M'
    static_values = {
        CATEGORY: "Known",
        COUNTRY: "United Kingdom",
        SOURCE_TEXT: "Natural resources Wales",
        SOURCE_TYPE: "Authorities",
        TYPE: "Sampling location",
    }
    proj_epsg_id = 27700

    replace_map = {
        MATRIX: {
            "RIVER / RUNNING SURFACE WATER": Matrix.SURFACE_WATER,
            "MYTILUS EDULIS - MUSSEL - TISSUE - SEE COMMENTS": Matrix.BIOTA,
            "SALMO TRUTTA - BROWN TROUT - MUSCLE": Matrix.BIOTA,
        }
    }

    def _parse_value_from_row(self, row):
        # TODO some values are marked as non high quality, decide what to do with them
        return float(row[ML_VALUE].replace(",", ".")), row["Sign"] == "<"

    @staticmethod
    def _parse_value(val: str):
        pass

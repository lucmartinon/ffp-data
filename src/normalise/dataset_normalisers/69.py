from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_69(MLNormaliser):
    columns = {
        "Provincia": KEEP,
        "Comune": CITY,
        "Parametro misurato": PARAM,
        "Codice punto di monitoraggio": KEEP,
        "Punto di monitoraggio": NAME,
        "Corpo idrico monitorato": KEEP,
        "Gruppo di parametri": DROP,
        "Categoria corpo idrico": DROP,
        "Data": DATE,
        "Anno": YEAR,
        "Tipo valore": DROP,
        "Valore del parametro": ML_VALUE,
        "Unita` di misura": UNIT,
        "Profondita campionamento (m)": DROP,
        "X": LON,
        "Y": LAT,
    }
    date_format = "%d/%m/%y"
    static_values = {
        SOURCE_TEXT: "ARPA Piemonte",
        COUNTRY: "Italy",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        SOURCE_URL: "Press request",
        TYPE: "Sampling location",
        MATRIX: "Surface water",
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), row["Tipo valore"] == "<"

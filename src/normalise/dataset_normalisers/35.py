from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_35(MLNormaliser):
    columns = {
        "Datum  (Frage 3)": DATE,
        "Medium": MATRIX,
        "PFAS-Belastung  (Frage 4)": ML_VALUE,
        "Welche PFAS  (Frage 4)": PARAM,
        "Ursache  (Frage 19)": KEEP,
        "Messstellennummer (Frage 5)": KEEP,
        "Ortsbezeichnung (Frage 5)/  Bezeichnung Messstelle": NAME,
        "Gemeinde / Kreis /kreisfreie Stadt  (Frage 10)": CITY,
        "Rechtswert": COORD_X,
        "Hochwert": COORD_Y,
    }
    static_values = {
        COUNTRY: "Germany",
        SOURCE_TYPE: "Authorities",
        TYPE: "Sampling location",
        CATEGORY: "Known",
        SOURCE_TEXT: "Rheinland-Pfalz Ministerium für Klimaschutz, Umwelt, Energie und Mobilität (MKUEM)",
    }
    proj_epsg_id = 25832
    unit = "ng/l"

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df["Medium"] = self.df["Medium"].replace({"Grundwasser": "Groundwater"})

    @staticmethod
    def _parse_value(val: str):
        try:
            # example 0,03 µg/L
            return float(val.replace(",", ".")[:-5]), False
        except ValueError:
            return None, None

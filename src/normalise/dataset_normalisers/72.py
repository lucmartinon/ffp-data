from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_72(MLNormaliser):
    columns = {
        "Rt Data Ora Prelievo": DATE,
        "Parametro": PARAM,
        "Unita Misura Descrizione": UNIT,
        "Segno Valore": DROP,
        "Valore An": ML_VALUE,
        "codStaz": DROP,
        "stazione": KEEP,
        "id": KEEP,
        "lat": LAT,
        "lon": LON,
        "Rif Campione": DROP,
        "Revisione": DROP,
        "Durata Campionamento Descrizione": DROP,
        "rete_monit": DROP,
        "rete_descr": DROP,
        "tema": DROP,
        "nome_pnt": NAME,
        "codice_eur": DROP,
        "data_creaz": DROP,
        "data_dismi": DROP,
        "unita_terr": DROP,
        "ind_pnt": DROP,
        "loc_pnt": DROP,
        "comune": CITY,
        "provincia": KEEP,
        "descpadre": DROP,
    }
    date_format = "ISO8601"
    static_values = {
        TYPE: "Sampling location",
        CATEGORY: "Known",
        COUNTRY: "Italy",
        SOURCE_TYPE: "Authorities",
        MATRIX: "Groundwater",
        SOURCE_URL: "Press request",
        SOURCE_TEXT: "ARPA Lazio",
    }

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), row["Segno Valore"] == "<"

    @staticmethod
    def _parse_value(val: str):
        pass

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_118(MLNormaliser):
    columns = {
        "ref_gwb": DROP,
        "date_ech": DATE,
        "no_code_site": DROP,
        "Résultats_conc(ng/l)": ML_VALUE,
        "param": PARAM,
        "no_code-station": KEEP,
        "x_ref_m": COORD_X,
        "y_ref_m": COORD_Y,
    }
    unit = "ng/l"
    date_format = "ISO8601"
    static_values = {
        COUNTRY: "Belgium",
        TYPE: "Sampling location",
        MATRIX: "Groundwater",
        SOURCE_TEXT: "Service public de Wallonie (SPW)",
        NAME: "Sampling location",
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known",
    }
    proj_epsg_id = 31370

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".").strip()
        if val.startswith("< "):
            return float(val[2:]), True
        return float(val), False

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_33(SLNormaliser):
    columns = {
        "order": DROP,
        "category": CATEGORY,
        "lat": LAT,
        "lon": LON,
        "State": KEEP,
        "County": KEEP,
        "Municipality": KEEP,
        "city": CITY,
        "name": NAME,
        "country": COUNTRY,
        "type": TYPE,
        "sector": SECTOR,
        "matrix": MATRIX,
        "year": YEAR,
        "pfos": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfos_pfoa": DROP,
        "pfna": SL_VALUE,
        "pfbs": SL_VALUE,
        "pfhxa": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfas in total": DROP,
        "pfas_sum": PFAS_SUM,
        "source_type": SOURCE_TYPE,
        "response by industry": KEEP,
        "note": KEEP,
        "source_text": SOURCE_TEXT,
        "source_url": SOURCE_URL,
        "source_url_2": KEEP,
        "source_url_3": KEEP,
        "details": DETAILS,
        "Antwort gekürzt": KEEP,
        "Antwort_url": KEEP,
    }
    static_values = {}

    @staticmethod
    def _parse_value(val: str):
        try:
            return float(val), False
        except ValueError:
            return None, None

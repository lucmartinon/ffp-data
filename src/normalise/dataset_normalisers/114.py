from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_114(SLNormaliser):
    unit = "μg/kg DS"
    columns = {
        "voie d'eau": KEEP,
        "Type échantillon*": KEEP,
        "localisation": NAME,
        "coordonnées X": COORD_X,
        "coordonnées Y": COORD_Y,
        "date échantillonnage": DATE,
        "numero du rapport": KEEP,
        "perfluoro-n-butanoic acid (PFBA) μg/kg DS": SL_VALUE,
        "perfluoro-n-pentanoic acid (PFPeA) μg/kg DS": SL_VALUE,
        "perfluoro-n-hexanoic acid (PFHxA) μg/kg DS": SL_VALUE,
        "perfluoro-n-heptanoic acid (PFHpA) μg/kg DS": SL_VALUE,
        "perfluoro-n-octanoic acid linear (PFOA) μg/kg DS": SL_VALUE,
        "perfluorooctanoic acid branched (PFOA) μg/kg DS": SL_VALUE,
        "perfluorooctanoic acid total (PFOAtotal) μg/kg DS": SL_VALUE,
        "perfluoro-n-nonanoic acid (PFNA) μg/kg DS": SL_VALUE,
        "perfluoro-n-decanoic acid (PFDA) μg/kg DS": SL_VALUE,
        "perfluoro-n-undecanoic acid (PFUnDA) μg/kg DS": SL_VALUE,
        "perfluoro-n-dodecanoic acid (PFDoDA) μg/kg DS": SL_VALUE,
        "perfluoro-n-tridecanoic acid (PFTrDA) μg/kg DS": SL_VALUE,
        "perfluoro-n-tetradecanoic acid (PFTeDA) μg/kg DS": SL_VALUE,
        "perfluoro-n-hexadecanoic acid (PHxDA) μg/kg DS": SL_VALUE,
        "perfluoro-n-butanesulfonic acid (PFBS) μg/kg DS": SL_VALUE,
        "perfluoro-n-pentanesulfonic acid (PFPeS) μg/kg DS": SL_VALUE,
        "perfluoro-n-hexane sulfonate linéaire (PFHxS) μg/kg DS": SL_VALUE,
        "perfluorohexane sulfonate ramifié (PFHxS) μg/kg DS": SL_VALUE,
        "perfluorohexanesulfonic acid total (PFHxStotal) μg/kg DS": SL_VALUE,
        "perfluoro-n-heptanesulfonic acid (PFHpS) μg/kg DS": SL_VALUE,
        "perfluoro-n-octanesulfonic acid linear (PFOS) μg/kg DS": SL_VALUE,
        "perfluorooctanesulfonic acid branched (PFOS) μg/kg DS": SL_VALUE,
        "perfluorooctanesulfonic acid total (PFOStotal) μg/kg DS": SL_VALUE,
        "perfluoro-n-nonanesulfonic acid (PFNS) μg/kg DS": SL_VALUE,
        "perfluoro-n-decanesulfonic acid (PFDS) μg/kg DS": SL_VALUE,
        "perfluoro-n-octanesulfonamide linear (PFOSA) μg/kg DS": SL_VALUE,
        "perfluorooctanesulfonamide branched (PFOSA) μg/kg DS": SL_VALUE,
        "perfluorooctanesulfonamide total (PFOSAtotal) μg/kg DS": SL_VALUE,
        "N-methylperfluoro-n-octanesulfonamide linéaire (MePFOSA) μg/kg DS": SL_VALUE,
        "N-methylperfluoroctanesulfonamide ramifié (MePFOSA) μg/kg DS": SL_VALUE,
        "N-methylperfluoroctanesulfonamide total (MePFOSAtotal) μg/kg DS": SL_VALUE,
        "N-ethylperfluoro-n-octanesulfonamide linéaire (EtPFOSA) μg/kg DS": SL_VALUE,
        "N-ethylperfluoroctanesulfonamide ramifié (EtPFOSA) μg/kg DS": SL_VALUE,
        "N-ethylperfluoroctanesulfonamide total (EtPFOSAtotal) μg/kg DS": SL_VALUE,
        "N-methylperfluoro-n-octanesulfonamidoacetic acid (MePFOSAA) μg/kg DS": SL_VALUE,
        "N-ethylperfluoro-n-octanesulfonamido acetic acid (EtPFOSAA) μg/kg DS": SL_VALUE,
        "4:2 fluorotelomer sulfonic acid (4:2 FTS) μg/kg DS": SL_VALUE,
        "6:2 fluorotelomer sulfonic acid (6:2 FTS) μg/kg DS": SL_VALUE,
        "8:2 fluorotelomer sulfonic acid (8:2 FTS) μg/kg DS": SL_VALUE,
        "8:2 Fluorotelomer phosphate diester (8:2diPAP) μg/kg DS": SL_VALUE,
        "perfluoro-2-propoxypropanoic acid (HFPO-DA) μg/kg DS": SL_VALUE,
        "4,8-dioxa-3H-perfluorononanoic acid (DONA) μg/kg DS": SL_VALUE,
        "perfluoro-4-ethylcyclohexane sulfonic acid (PFECHS) μg/kg DS": SL_VALUE,
        "Somme PFAS quantitatif μg/kg DS": PFAS_SUM,
        "Perfluoro-n-butane sulfonamide (PFBSA) μg/kg DS": SL_VALUE,
        "N-methylperfluoro-n-butane sulfonamide (MeFBSA) μg/kg DS": SL_VALUE,
        "Perfluoro-n-hexane sulfonamide (PFHxSA) μg/kg DS": SL_VALUE,
    }
    static_values = {
        "type": "Sampling location",
        "category": "Known",
        "source_type": "Authorities",
        "source_text": "Service public de Wallonie (SPW)",
        "country": "Belgium",
        "matrix": "Sediment",
    }
    proj_epsg_id = 31370

    @staticmethod
    def _parse_value(val: str):
        if val.strip() == "-":
            return None, None
        val = val.replace(" ", "")
        val = val.replace(",", ".")
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False

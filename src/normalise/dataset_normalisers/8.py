from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_8(SLNormaliser):
    static_values = {COUNTRY: "France", CATEGORY: "Presumptive"}
    columns = {
        "category": CATEGORY,
        "lat": LAT,
        "lon": LON,
        "name": NAME,
        "city": CITY,
        "country": COUNTRY,
        "type": TYPE,
        "sector": SECTOR,
        "matrix": MATRIX,
        "year": YEAR,
        "pfos": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfos_pfoa": KEEP,
        "pfna": SL_VALUE,
        "pfbs": SL_VALUE,
        "pfhxa": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfas_sum": PFAS_SUM,
        "source_type": SOURCE_TYPE,
        "source_text": SOURCE_TEXT,
        "source_url": SOURCE_URL,
        "details": DETAILS,
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

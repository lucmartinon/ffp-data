from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_74(MLNormaliser):
    columns = {
        "Anno": YEAR,
        "Data": DATE,
        "ID": DROP,
        "TipoSito": KEEP,
        "CodiceSito": KEEP,
        "NomeSito": NAME,
        "Est": COORD_X,
        "Nord": COORD_Y,
        "Prelievo": DROP,
        "Campione": DROP,
        "Variabile": PARAM,
        "Censura": DROP,
        "Valore": ML_VALUE,
        "Unità": UNIT,
    }
    date_format = "%m/%d/%Y"
    static_values = {
        MATRIX: "Surface water",
        SOURCE_URL: "Press request",
        CATEGORY: "Known",
        COUNTRY: "Italy",
        SOURCE_TEXT: "ARPA Trento",
        TYPE: "Sampling location",
        SOURCE_TYPE: "Authorities",
    }
    proj_epsg_id = 25832

    def _parse_value_from_row(self, row):
        val = row[ML_VALUE].replace(",", ".")
        return float(val), row["Censura"] == "<"

    @staticmethod
    def _parse_value(val: str):
        pass

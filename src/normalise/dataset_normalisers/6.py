from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_6(SLNormaliser):
    static_values = {COUNTRY: "Italy", CATEGORY: "Presumptive", SOURCE_TYPE: "OSINT"}
    columns = {
        "category": CATEGORY,
        "lat": LAT,
        "lon": LON,
        "city": CITY,
        "country": COUNTRY,
        "name": NAME,
        "type": TYPE,
        "source_type": SOURCE_TYPE,
        "source_text": SOURCE_TEXT,
        "source_url": SOURCE_URL,
        "source": KEEP,
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

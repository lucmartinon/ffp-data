from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_116(MLNormaliser):
    columns = {
        "Commune": CITY,
        "site": KEEP,
        "nomoulieu": NAME,
        "date du prélèvement": DATE,
        "expression": UNIT,
        "masse": KEEP,
        "valeurmesurée": ML_VALUE,
        "description": DROP,
        "sigletitul": KEEP,
        "lat": LAT,
        "lng": LON,
        "type": DROP,
        "NomSection": KEEP,
        "Ss-réseau": KEEP,
        "RéfContexte": KEEP,
        "codeESO": DROP,
        "Symbole": PARAM,
        "XLambert": DROP,
        "YLambert": DROP,
    }
    date_format = "%d-%M-%y"
    static_values = {
        CATEGORY: "Known",
        COUNTRY: "Belgium",
        SOURCE_TEXT: "Service public de Wallonie (SPW)",
        SOURCE_TYPE: "Authorities",
        MATRIX: "Groundwater",
        TYPE: "Sampling location",
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("-"):
            return float(val[1:].strip()), True
        return float(val), False

    def _preprocess(self):
        self.df = self.raw_df.copy()

        # boy their dateformat is stupid...
        self.df["date du prélèvement"] = self.df["date du prélèvement"].str.replace(
            ".", ""
        )
        replace_map = {
            "janv": "01",
            "févr": "02",
            "mars": "03",
            "avr": "04",
            "mai": "05",
            "juin": "06",
            "juil": "07",
            "août": "08",
            "sept": "09",
            "oct": "10",
            "nov": "11",
            "déc": "12",
        }
        for k, v in replace_map.items():
            self.df["date du prélèvement"] = self.df["date du prélèvement"].str.replace(
                k, v
            )

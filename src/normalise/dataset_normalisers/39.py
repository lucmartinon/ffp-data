from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_39(MLNormaliser):
    date_format = "%d.%m.%Y"
    columns = {
        "Parameter": PARAM,
        "Datum": DATE,
        "Einheit": UNIT,
        "Gewässer": KEEP,
        "Messstelle": NAME,
        "UTMx": COORD_X,
        "UTMy": COORD_Y,
        "Wert": ML_VALUE,
    }
    static_values = {
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Germany",
        CATEGORY: "Known",
        SOURCE_TEXT: "Bremen Die Senatorin für Klimaschutz, Umwelt, Mobilität, Stadtentwicklung und Wohnungsbau",
        TYPE: "Sampling location",
        MATRIX: "Surface water",
    }
    proj_epsg_id = 25832

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df["UTMx"] = self.df["UTMx"].str.replace(".", "")
        self.df["UTMx"] = self.df["UTMx"].str.replace(",", ".")
        self.df["UTMy"] = self.df["UTMy"].str.replace(".", "")
        self.df["UTMy"] = self.df["UTMy"].str.replace(",", ".")
        # a weird single value
        self.df["Datum"] = self.df["Datum"].str.replace("1/24/2019", "24.01.2019")

    @staticmethod
    def _parse_value(val: str):
        if val == "n. b.":
            return None, None

        val = val.replace(",", ".")
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False

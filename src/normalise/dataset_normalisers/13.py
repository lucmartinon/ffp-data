from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_13(SLNormaliser):
    static_values = {CATEGORY: "Known"}
    columns = {
        "category": CATEGORY,
        "lat": LAT,
        "lon": LON,
        "name": NAME,
        "city": CITY,
        "country": COUNTRY,
        "type": TYPE,
        "sector": SECTOR,
        "comment": KEEP,
        "matrix": MATRIX,
        "year": YEAR,
        "pfos": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfna": SL_VALUE,
        "pfbs": SL_VALUE,
        "pfhxa": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfas_sum": PFAS_SUM,
        "source_type": SOURCE_TYPE,
        "source_text": SOURCE_TEXT,
        "source_url": SOURCE_URL,
        "details": DETAILS,
        "Antwort gekürzt": KEEP,
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

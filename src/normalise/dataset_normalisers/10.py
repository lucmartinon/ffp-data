from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_10(SLNormaliser):
    static_values = {
        CATEGORY: "Known",
        SOURCE_TYPE: "FOI",
        SOURCE_TEXT: "European Commission Joint Research Center",
        SOURCE_URL: "https://drive.google.com/drive/folders/1CKxJ5Q1Q-VqctdDvvnFPM8mHPYOyj_qo",
    }
    columns = {
        "category": CATEGORY,
        "lat": LAT,
        "lon": LON,
        "name": NAME,
        "city": CITY,
        "country": COUNTRY,
        "type": TYPE,
        "sector": SECTOR,
        "matrix": MATRIX,
        "year": YEAR,
        "pfos": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfna": SL_VALUE,
        "pfbs": SL_VALUE,
        "pfhxa": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfas_sum": PFAS_SUM,
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

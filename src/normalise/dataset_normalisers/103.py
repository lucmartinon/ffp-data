from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_103(SLNormaliser):
    # a lot of them are false as they are in rivers / on the coast.
    fill_missing_cities = False
    unit = "ug/l"
    columns = {
        "lat": LAT,
        "lon": LON,
        "pfas_sum": PFAS_SUM,
        "PFPeA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFUnA": SL_VALUE,
        "PFDoDA": SL_VALUE,
        "PFTrDA": SL_VALUE,
        "PFTeDA": SL_VALUE,
        "PFPeS": SL_VALUE,
        "PFHpS": SL_VALUE,
        "PFOS branched": SL_VALUE,
        "PFOS linear": SL_VALUE,
        "PFNS": SL_VALUE,
        "PFBSA": SL_VALUE,
        "PFHxSA": SL_VALUE,
        "PFOSA": SL_VALUE,
        "4:2FTS": SL_VALUE,
        "6:2FTS": SL_VALUE,
        "8:2FTS": SL_VALUE,
        "HPFO-DA": SL_VALUE,
        "ADONA": SL_VALUE,
        "PFECHS": SL_VALUE,
        "9Cl-PF3ONS": SL_VALUE,
        "LIMS number": KEEP,
        "Date sample collected": DATE,
        "Time UTC": KEEP,
        "National Grid Reference": KEEP,
        "Survey Vessel": KEEP,
        "Survey area": KEEP,
        "Unnamed: 8": DROP,
        "Water depth": KEEP,
        "CTD depth (m)": KEEP,
        "Temp °C": KEEP,
        "Salinity": KEEP,
        "DO%": KEEP,
        "Turbidity FTU": KEEP,
        "Sediment description": KEEP,
        "Site name": NAME,
        "TOC content": KEEP,
        "PFBuA": SL_VALUE,
        "PFDcA": SL_VALUE,
        "PFBuS": SL_VALUE,
        "PFHxS branched": SL_VALUE,
        "PFHxS linear": SL_VALUE,
        "PFDcS": SL_VALUE,
        "11Cl-PF3OUdS": SL_VALUE,
        "NMeFOSAA": SL_VALUE,
        "NEtFOSAA": SL_VALUE,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        TYPE: "Sampling location",
        MATRIX: "Sediment",
        COUNTRY: "United Kingdom",
        SOURCE_TYPE: "Authorities",
        SOURCE_URL: "https://randd.defra.gov.uk/ProjectDetails?ProjectId=20506",
        CATEGORY: "Known",
        SOURCE_TEXT: "Centre for Environment Fisheries & Aquaculture Science",
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_31(MLNormaliser):
    date_format = "%d/%m/%Y %H:%M"
    unit = "ng/l"
    columns = {
        "x": COORD_X,
        "y": COORD_Y,
        "codequalitometre": KEEP,
        "codeproducteur": KEEP,
        "identifiant_point_de_mesures": KEEP,
        "date_prelevement": DATE,
        "parametre": PARAM,
        "unite": DROP,
        "valeur": ML_VALUE,
        "lim_quantification": DROP,
        "code_remarque": DROP,
    }
    static_values = {
        MATRIX: "Groundwater",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "France",
        SOURCE_TEXT: "Aprona",
        TYPE: "Sampling location",
        NAME: "Sampling location",
        SOURCE_URL: "https://www.aprona.net/FR/ermes-rhin/presentation-ermes-rhin.html",
    }
    proj_epsg_id = 2154

    replace_map = {
        PARAM: {
            7951: "914637-49-3",
            5978: "307-24-4",
            6548: "754-91-6",
            7945: "757124-72-4",
            7969: "27854-31-5",
            5347: "335-67-1",
            6509: "335-76-2",
            6549: "72629-94-8",
            6510: "2058-94-8",
            7947: "53826-13-4",
            7968: "70887-88-6",
            6025: "59933-66-3",
            7949: "70887-94-4",
            6550: "335-77-3",
            6830: "355-46-4",
            7946: "39108-34-4",
            6507: "307-55-1",
            7893: "27619-97-2",
            7970: "70887-84-2",
            6508: "375-95-1",
            6561: "45298-90-6",
            6542: "375-92-8",
            5977: "375-85-9",
            6049: "30334-69-1",
            5980: "375-22-4",
            5979: "2706-90-3",
            6547: "376-06-7",
            7967: "53826-12-3",
        }
    }

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df["valeur"] = self.df["valeur"].astype(str)
        self.df["valeur"] = self.df.apply(parse_row_value, axis=1)

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False


def parse_row_value(row):
    # see https://www.sandre.eaufrance.fr/jeu-de-donnees/code-remarque
    if row["code_remarque"] == 1:
        return row["valeur"]
    if row["code_remarque"] in [7, 10]:
        return "<" + row["valeur"]

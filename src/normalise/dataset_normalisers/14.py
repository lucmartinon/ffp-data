from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_14(SLNormaliser):
    columns = {
        "Einheit": UNIT,
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFUnDA": SL_VALUE,
        "PFDoA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFPeS": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFHpS": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFNS": SL_VALUE,
        "PFDS": SL_VALUE,
        "4:2 FTS": SL_VALUE,
        "6:2 FTS": SL_VALUE,
        "8:2 FTS": SL_VALUE,
        "name": NAME,
        "city": CITY,
        "matrix": MATRIX,
        "ADONA ": SL_VALUE,
        "F-53B": SL_VALUE,
        "GenX ": SL_VALUE,
        "Summe Gesamt-PFAS": DROP,
        "Summe der PFAS gem. EU-RL": DROP,
        "Summe 4 PFAS (PFOA, PFNA, PFHxS und PFOS)": DROP,
    }
    static_values = {
        YEAR: "2020",
        CATEGORY: "Known",
        SOURCE_URL: "https://www.ages.at/",
        TYPE: "Sampling location",
        SOURCE_TEXT: "AGES",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Austria",
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.strip()
        val = val.replace(",", ".")
        if val == "n.n.":
            return None, None

        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

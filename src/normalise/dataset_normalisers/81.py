import numpy as np

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_81(MLNormaliser):
    columns = {
        "Longitude": LON,
        "SAMPLE_ID": DROP,
        "LOCATION_CODE": KEEP,
        "DESCRIPTION": NAME,
        "Latitute": LAT,
        "SITE_CODE": DROP,
        "ANALYSIS_CODE": DROP,
        "ANALYTE_NAME": PARAM,
        "COMBINATION_RESULT": ML_VALUE,
        "COLLECTION_DATE": DATE,
        "SUBMIT_DATE": DROP,
        "SAMPLE_TYPE": MATRIX,
        "ANALYSIS_UNIT": DROP,
        "ANALYTE_MDL": DROP,
    }
    date_format = "%d/%m/%Y"
    # there is a unit column, but not filled for each row and it seems to be always micrograms
    unit = "µg/l"
    static_values = {
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Luxembourg",
        TYPE: "Sampling location",
        SOURCE_TEXT: "Luxembourg Ministry of the Environment, Climate and Sustainable Development",
        CATEGORY: "Known",
        SOURCE_URL: "https://mecdd.gouvernement.lu/en.html",
    }

    replace_map = {
        MATRIX: {
            "eau de surface": Matrix.SURFACE_WATER,
            # assuming this because of the name of the file
            np.nan: Matrix.GROUNDWATER,
        }
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

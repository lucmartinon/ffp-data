from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_1(SLNormaliser):
    
    columns = {
        "FacilityInspireID": KEEP,
        "city": CITY,
        "country": COUNTRY,
        "Longitude": LON,
        "Latitude": LAT,
        "sector": SECTOR,
        "facilityName": NAME,
        "eprtr_sector": KEEP,
        "eprtr_activity_code": KEEP,
        "eprtr_activity": KEEP
    }
    static_values = {
        TYPE: "Sampling location",
        CATEGORY: "Presumptive",
        SOURCE_URL: "https://industry.eea.europa.eu/#/home",
        SOURCE_TEXT: "EPRTR",
        SOURCE_TYPE: "Authorities"
    }

    @staticmethod
    def _parse_value(val: str):
        # val = val.replace(',', '.')
        if val.startswith('<'):
            return float(val[1:].strip()), True
        return float(val), False

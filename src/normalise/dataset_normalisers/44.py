from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_44(MLNormaliser):
    columns = {
        "LAT": LAT,
        "LON": LON,
        "DATAVALUE": ML_VALUE,
        "PARAMETER": PARAM,
        "UNITNAME": UNIT,
        "CAMPAIGN": KEEP,
        "DATA_DATETIME": DATE,
        "DOI": KEEP,
        "ORIGINAL_DATA": DROP,
        "MEASUREMENT_DEPTH": DROP,
        "PARAMETERSITE_NAME": MATRIX,
        "STATIONID": KEEP,
    }
    date_format = "ISO8601"
    static_values = {
        SOURCE_TEXT: "Helmholtz-Zentrum Hereon",
        CATEGORY: "Known",
        TYPE: "Sampling location",
        COUNTRY: "Germany",
        SOURCE_TYPE: "Scientific article",
        SOURCE_URL: "https://hcdc.hereon.de/campaign_db/#/download",
    }

    replace_map = {
        MATRIX: {
            "Water": "Surface water",
            "Atmosphere": "Atmosphere",
            "Sediment": "Sediment",
        }
    }

    @staticmethod
    def _parse_value(val: str):
        try:
            return float(val), False
        except ValueError:
            return None, None

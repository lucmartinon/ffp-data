import logging
import re

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_34(MLNormaliser):
    columns = {
        "Grundwassermessstellennummer": KEEP,
        "Messstellenname": NAME,
        "Parameter": PARAM,
        "Datum": DATE,
        "Wert": ML_VALUE,
        "Einheit": UNIT,
        "Hochwert UTM": COORD_Y,
        "Rechtswert UTM": COORD_X,
    }
    static_values = {
        TYPE: "Sampling location",
        MATRIX: "Groundwater",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Schleswig-Holstein Landesamt für Umwelt (LfU)",
        COUNTRY: "Germany",
    }
    proj_epsg_id = 25832

    def _preprocess(self):
        self.df = self.raw_df.copy()

        def clear_name(name):
            p = re.compile(r"(.*) F\d+$")
            result = p.search(name)
            if result:
                return result.groups(1)[0]
            return name

        self.df["Messstellenname"] = self.df["Messstellenname"].apply(clear_name)
        self.df["Rechtswert UTM"] = self.df["Rechtswert UTM"].apply(
            lambda x: float(str(x)[2:])
        )
        logging.info("Corrected Rechtswert UTM")

    @staticmethod
    def _parse_value(val: str):
        try:
            return float(val), False
        except ValueError:
            return None, None

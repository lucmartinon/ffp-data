import re

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_86(SLNormaliser):
    columns = {
        "Region": KEEP,
        "River": KEEP,
        "Date of sampling": DATE,
        "Latitude": LAT,
        "Longitude": LON,
        "Unit": UNIT,
        "PFOS": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "River Basin": KEEP,
        "Town": CITY,
        "Matrix (ground water, soil, sediment etc.) ": MATRIX,
        "Species (pooled fish)": KEEP,
        "Year of sampling": YEAR,
        "PFOS + PFOA in one sample": DROP,
        "PFAS total": DROP,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        TYPE: "Sampling location",
        COUNTRY: "Spain",
        SOURCE_URL: "https://doi.org/10.1016/j.chemosphere.2021.131940",
        NAME: "Sampling location",
        SOURCE_TEXT: " Roscales 2020 ",
        SOURCE_TYPE: "Scientific article",
        CATEGORY: "Known",
    }

    replace_map = {MATRIX: {"Fish": Matrix.BIOTA, "River water": Matrix.SURFACE_WATER}}

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        return float(val), False

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df["Latitude"] = self.df["Latitude"].apply(to_dec_coordinates)
        self.df["Longitude"] = self.df["Longitude"].apply(to_dec_coordinates)
        self.df["Date of sampling"] = self.df["Date of sampling"].replace(
            {"Unknown": None}
        )


def to_dec_coordinates(coord_str):
    elements = re.split("[º°' ]", str(coord_str))
    elements = [e for e in elements if e != ""]
    deg, min, sec, dir = 0, 0, 0, "N"
    deg = elements[0]
    min = elements[1]

    if len(elements) == 3:
        dir = elements[2]
    elif len(elements) == 4:
        sec = elements[2]
        dir = elements[3]

    res = (float(deg) + float(min) / 60 + float(sec) / (60 * 60)) * (
        -1 if dir in ["W", "S"] else 1
    )
    # print(f"{coord_str} > {res}")
    return res

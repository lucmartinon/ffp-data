from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_28(MLNormaliser):
    columns = {
        "bss_id": NAME,
        "code_bss": KEEP,
        "precision_coordonnees": KEEP,
        "longitude": LON,
        "latitude": LAT,
        "altitude": KEEP,
        "nom_commune_actuel": CITY,
        "date_debut_prelevement": DATE,
        "code_param": DROP,
        "resultat": ML_VALUE,
        "code_remarque_analyse": DROP,
        "nom_remarque_analyse": DROP,
        "code_unite": DROP,
        "nom_unite": UNIT,
        "cas": PARAM
    }
    date_format = 'ISO8601'
    static_values = {
        SOURCE_TYPE: "Authorities",
        COUNTRY: "France",
        MATRIX: "Groundwater",
        SOURCE_TEXT: "ADES",
        SOURCE_URL: "https://ades.eaufrance.fr/ ",
        TYPE: "Sampling location",
        CATEGORY: "Known"
    }

    def _parse_value_from_row(self, row):
        # see https://www.sandre.eaufrance.fr/jeu-de-donnees/code-remarque
        if row['code_remarque_analyse'] in [2, 7, 10]:
            return float(row[ML_VALUE]), True
        return float(row[ML_VALUE]), False

    @staticmethod
    def _parse_value(val: str):
        pass

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_67(MLNormaliser):
    columns = {
        "PROVINCIA": KEEP,
        "CODICE STAZIONE": KEEP,
        "CORPO IDRICO": KEEP,
        "DATA PRELIEVO": DATE,
        "PARAMETRO": PARAM,
        "CONCENTRAZIONE ": ML_VALUE,
        "X": COORD_X,
        "Y": COORD_Y,
        "matrix": MATRIX,
        "UNITA' DI MISURA": UNIT,
        "METODO": KEEP,
    }
    date_format = "%Y-%m-%d"
    static_values = {
        SOURCE_TYPE: "Authorities",
        SOURCE_URL: "Press request",
        NAME: "Sampling location",
        CATEGORY: "Known",
        COUNTRY: "Italy",
        TYPE: "Sampling location",
        SOURCE_TEXT: "ARPA Campania",
    }
    proj_epsg_id = 23033

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

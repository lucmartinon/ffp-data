from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_102(MLNormaliser):
    columns = {
        "SAMP_SMPT_USER_REFERENCE": KEEP,
        "SMPT_TYPE": KEEP,
        "SPT_DESC": KEEP,
        "SMPT_GRID_REF_KM": UK_GRID_REFERENCE,
        "SAMP_ID": DROP,
        "SAMP_SAMPLE_DATE": DATE,
        "SAMP_MATERIAL": DROP,
        "SAMP_CONFIDENTIAL": DROP,
        "SMC_DESC": MATRIX,
        "MEAS_SIGN": DROP,
        "DWI Tiers 1-4": DROP,
        "MEAS_RESULT": ML_VALUE,
        "UNIT_SHORT_DESC": UNIT,
        "SAMP_PURPOSE_CODE": DROP,
        "PURP_DESC": KEEP,
        "MEAS_ANAL_METH_CODE": DROP,
        "MEAS_DETERMINAND_CODE": DROP,
        "DETE_DESC": PARAM,
        "ARE_DESC": NAME,
    }
    date_format = "%Y-%m-%d %H:%M:%S"
    static_values = {
        SOURCE_TEXT: "Environment Agency",
        TYPE: "Sampling location",
        SOURCE_TYPE: "FOI",
        CATEGORY: "Known",
        COUNTRY: "United Kingdom",
    }

    replace_map = {
        MATRIX: {
            "GROUNDWATER": Matrix.GROUNDWATER,
            "CANAL WATER": Matrix.SURFACE_WATER,
            "POND / LAKE / RESERVOIR WATER": Matrix.SURFACE_WATER,
            "GROUNDWATER - PURGED/PUMPED/REFILLED": Matrix.GROUNDWATER,
            "ANY WATER": Matrix.SURFACE_WATER,
            "ANY TRADE EFFLUENT": Matrix.WASTEWATER,
            "RIVER / RUNNING SURFACE WATER": Matrix.SURFACE_WATER,
            "SURFACE DRAINAGE": Matrix.SURFACE_WATER,
        }
    }

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), row["MEAS_SIGN"] == "<"

    @staticmethod
    def _parse_value(val: str):
        pass

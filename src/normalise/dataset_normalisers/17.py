from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_17(SLNormaliser):
    columns = {
        "Sample code": KEEP,
        "Sampling location": CITY,
        "PFOA ": SL_VALUE,
        "Br-PFOS ": SL_VALUE,
        "L-PFOS ": SL_VALUE,
        "PFHxA ": SL_VALUE,
        "PFHxS ": SL_VALUE,
        "PFBS ": SL_VALUE,
        "PFNA ": SL_VALUE,
        "PFHpA ": SL_VALUE,
        "PFDA ": SL_VALUE,
        "PFUdA ": SL_VALUE,
        "PFDoA ": SL_VALUE,
        "PFTrDA ": SL_VALUE,
        "PFTeDA ": SL_VALUE,
        "PFPrS ": SL_VALUE,
        "PFPeS ": SL_VALUE,
        "PFHpS ": SL_VALUE,
        "PFNS ": SL_VALUE,
        "PFDS ": SL_VALUE,
        "PFDoS ": SL_VALUE,
        "HFPO-DA ": SL_VALUE,
        "NaDONA ": SL_VALUE,
        "9Cl-PF3ONS ": SL_VALUE,
        "11Cl-PF3OUdS": SL_VALUE,
    }
    static_values = {
        SOURCE_URL: "https://doi.org/10.1007/s11356-022-20156-7",
        NAME: "Sampling location",
        SOURCE_TEXT: "Jurikova 2022",
        SOURCE_TYPE: "Scientific article",
        MATRIX: "Drinking water",
        COUNTRY: "Czechia",
        CATEGORY: "Known",
        TYPE: "Sampling location",
        YEAR: 2018,
    }

    def _preprocess(self):
        self.df = self.raw_df.copy()

        for c in [c for c in self.df.columns if self.columns[c] == SL_VALUE]:
            mloq = self.df.loc[0][c]
            self.df[c] = self.df[c].replace("<MLOQ", f"<{mloq}")
        self.df = self.df[1:]
        self.df = self.df[self.df["Sampling location"] != "x"]
        self.df[LAT] = None
        self.df[LON] = None
        self.columns[LAT] = LAT
        self.columns[LON] = LON

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        else:
            return float(val), False

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_23(MLNormaliser):
    columns = {
        "city": CITY,
        "pointno": KEEP,
        "x1": COORD_X,
        "y1": COORD_Y,
        "SampleDate": DATE,
        "year": YEAR,
        "matrix": MATRIX,
        "attribute": KEEP,
        "value": ML_VALUE,
        "Unit": UNIT,
        "sampleid": DROP,
        "projectno": KEEP,
        "coordinateQualityT": DROP,
        "coordinateMethodT": DROP,
        "sampleno": DROP,
        "Depth1": KEEP,
        "depth2": KEEP,
        "intakeDepth1": DROP,
        "intakeDepth2": DROP,
        "parameterid": DROP,
        "parameter": DROP,
        "CommonName": PARAM,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        COUNTRY: "Denmark",
        SOURCE_TEXT: "Danske Region Nordjylland",
        CATEGORY: "Known",
        TYPE: "Sampling location",
        SOURCE_TYPE: "Authorities",
    }
    proj_epsg_id = 25832

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), str(row["attribute"]).strip() == "<"

    def _parse_value(value_str: str) -> (float, bool):
        logging.error("we should never pass here")
        return None, None

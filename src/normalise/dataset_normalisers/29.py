from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_29(MLNormaliser):
    columns = {
        "code_station": KEEP,
        "libelle_station": NAME,
        "uri_station": KEEP,
        "code_support": KEEP,
        "libelle_support": MATRIX,
        "libelle_fraction": DROP,
        "date_prelevement": DATE,
        "code_parametre": DROP,
        "libelle_parametre": DROP,
        "resultat": ML_VALUE,
        "code_unite": DROP,
        "symbole_unite": UNIT,
        "code_remarque": DROP,
        "mnemo_remarque": DROP,
        "longitude": LON,
        "latitude": LAT,
        "cas": PARAM,
        "code_fraction": DROP
    }
    date_format = 'ISO8601'
    static_values = {
        SOURCE_URL: "https://naiades.eaufrance.fr/",
        COUNTRY: "France",
        SOURCE_TEXT: "Naiades",
        TYPE: "Sampling location",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities"
    }

    replace_map = {
        MATRIX: {
            "Sédiments": Matrix.SEDIMENT,
            "Eau": Matrix.SURFACE_WATER,
            "Gammares": Matrix.BIOTA,
            "Poissons": Matrix.BIOTA,
            "Matières en suspension (M.E.S.)": Matrix.UNKNOWN
        }
    }

    def _parse_value_from_row(self, row):
        # see https://www.sandre.eaufrance.fr/definition/ALQ/2.2/RqAna
        if row['code_remarque'] in [2, 7, 10]:
            return float(row[ML_VALUE]), True

        return float(row[ML_VALUE]), False

    @staticmethod
    def _parse_value(val: str):
        pass

import logging

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_18(MLNormaliser):
    columns = {
        "Stedtype": KEEP,
        "StedID": KEEP,
        "Stedtekst": KEEP,
        "Referencer": KEEP,
        "GeoZone": DROP,
        "x-koordinat": COORD_X,
        "y-koordinat": COORD_Y,
        "Kommune": CITY,
        "Region": KEEP,
        "Medie": KEEP,
        "Vandområde": KEEP,
        "Dato": DATE,
        "Måling nr": DROP,
        "Undersøgelsestype": MATRIX,
        "Teknisk anvisning": DROP,
        "Dataejer": DROP,
        "Link": DROP,
        "Målested navn": NAME,
        "Målested GeoZone": DROP,
        "Målested, x-koordinat": DROP,
        "Målested, y-koordinat": DROP,
        "Prøvetagning": DROP,
        "Prøve": DROP,
        "Artkode": KEEP,
        "Art latin": KEEP,
        "Art": KEEP,
        "Analysefraktion": KEEP,
        "ScKode": DROP,
        "Stofparameter": DROP,
        "Resultat-attribut": DROP,
        "Resultat": ML_VALUE,
        "Enhed": UNIT,
        "Kvalitetsmærke": DROP,
        "Prøvetype": DROP,
        "Delprøve": DROP,
        "Sedimentlag-fra (cm)": DROP,
        "Sedimentlag-til (cm)": DROP,
        "Dybde (m)": KEEP,
        "Faktiske dybder (m)": DROP,
        "Start dybde (m)": DROP,
        "Slut dybde (m)": DROP,
        "CASnr": PARAM,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        SOURCE_URL: "https://eng.mst.dk/ ",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Danish Environmental Protection Agency",
        CATEGORY: "Known",
        TYPE: "Sampling location",
        COUNTRY: "Denmark",
    }
    proj_epsg_id = 25832

    replace_map = {
        MATRIX: {
            "MFS biota, fisk, Marin": "Biota",
            "MFS biota, fisk, Sø": "Biota",
            "MFS biota, fisk, Vandløb": "Biota",
            "MFS sediment, Sø": "Sediment",
            "Vandkemi, Marin": "Sea water",
            "MFS vand, Vandløb": "Surface water",
        }
    }

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), row["Resultat-attribut"].strip() == "<"

    def _parse_value(value_str: str) -> (float, bool):
        logging.error("we should never pass here")
        return None, None

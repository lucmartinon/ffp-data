from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_2(SLNormaliser):
    fill_missing_cities = False
    columns = {
        "country": COUNTRY,
        "uwwCode": KEEP,
        "uwwName": NAME,
        "uwwLatitude": LAT,
        "uwwLongitude": LON,
        "uwwCapacity": KEEP,
        "repCode": KEEP,
        "uwwNUTS": KEEP,
        "uwwBeginLife": KEEP,
        "uwwEndLife": KEEP,
        "uwwInspireIDFacility": KEEP,
        "rptMStateKey": KEEP,
    }
    static_values = {
        SOURCE_TEXT: "European Environment Agency",
        SOURCE_URL: "https://sdi.eea.europa.eu/catalogue/srv/eng/catalog.search#/metadata/7c26ff2c-d1b1-4533-95e6-29b34d29fe0e ",
        TYPE: "Sampling location",
        CATEGORY: "Presumptive",
        SOURCE_TYPE: "Authorities",
    }

    @staticmethod
    def _parse_value(val: str):
        pass

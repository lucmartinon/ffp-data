import logging

import pyproj

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_101(MLNormaliser):
    proj_epsg_id = 27700
    columns = {
        "name": NAME,
        "lat": LAT,
        "lon": LON,
        "TreatmentPlant": KEEP,
        "SampleDateTime": DATE,
        "SampleLocationName": DROP,
        "SampleValue": ML_VALUE,
        "NameDeterminandName": PARAM,
        "UnitsName": UNIT,
        "BelowMinReading": DROP,
    }
    date_format = "%d/%m/%Y %H:%M:%S"
    static_values = {
        SOURCE_TYPE: "Authorities",
        COUNTRY: "United Kingdom",
        MATRIX: "Wastewater",
        SOURCE_TEXT: "Luxembourg Ministry of the Environment, Climate and Sustainable Development",
        CATEGORY: "Known",
        TYPE: "Sampling location",
        SOURCE_URL: "https://mecdd.gouvernement.lu/en.html",
    }

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), row["BelowMinReading"] == "Yes"

    # we implement the coord adapt because the column is a mix of lat / lon and coord in 3346

    def _adapt_coordinates(self):
        # adapt coordinates
        if self.proj_epsg_id:
            self.df["lonlat"] = self.df.apply(
                lambda row: self._transform_coord(row), axis=1
            )
            self.df[LON] = self.df["lonlat"].str[0]
            self.df[LAT] = self.df["lonlat"].str[1]
            self.df = self.df.drop(columns=["lonlat"])

            logging.info(
                f"Transposed coordinates to GPS, using EPSG ID [{self.proj_epsg_id}]"
            )

    def _transform_coord(self, row) -> (float, float):
        if self.proj_transformer is None:
            self.proj_transformer = pyproj.Transformer.from_crs(
                self.proj_epsg_id, 4326, always_xy=True
            )

        if row[LAT] > 90 and row[LON] > 90:
            lonlat = self.proj_transformer.transform(row[LON], row[LAT])
            return round(lonlat[1], 5), round(lonlat[0], 5)
        return row[LON], row[LAT]

    @staticmethod
    def _parse_value(val: str):
        pass

    def _preprocess(self):
        self.df = self.raw_df.copy()
        # Olivia.Mair@defra.gov.uk sent me (luc) a correction request by email on 2023-02-23.
        # We manually correct the possition of 1 water treatment plant.
        r_map = {
            "Fazakerley STW - River Upstream": (53.459967, -2.9113095),
            "Fazakerley STW - River Downstream": (53.464893, -2.9136745),
            "Fazakerley STW - Treatment Effluent": (53.46326, -2.9155976),
        }
        [33]
        self.df["lat"] = self.df.apply(
            lambda r: r_map[r["name"]][0] if r["name"] in r_map else r["lat"], axis=1
        )
        self.df["lon"] = self.df.apply(
            lambda r: r_map[r["name"]][1] if r["name"] in r_map else r["lon"], axis=1
        )

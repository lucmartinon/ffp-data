from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_40(MLNormaliser):
    columns = {
        "Name": KEEP,
        "Matrix": KEEP,
        "Parameter": PARAM,
        "Wert": ML_VALUE,
        "Einheit": UNIT,
        "Probenahmeort ": NAME,
        "Probenahmedatum*": DATE,
        "Bemerkung": KEEP,
    }
    date_format = "mixed"
    static_values = {
        MATRIX: "Biota",
        TYPE: "Sampling location",
        COUNTRY: "Germany",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Bremen Die Senatorin für Klimaschutz, Umwelt, Mobilität, Stadtentwicklung und Wohnungsbau",
    }

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df["Probenahmedatum*"] = self.df["Probenahmedatum*"].fillna(method="ffill")

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

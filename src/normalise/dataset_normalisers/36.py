from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_36(SLNormaliser):
    unit = "µg/kg"
    columns = {
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFUnA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFHpS": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFDS": SL_VALUE,
        "ERFPROJEKT": KEEP,
        "NRBF": KEEP,
        "PROFBEZ": KEEP,
        "BODSYST": KEEP,
        "NUTZ": KEEP,
        "GEMEINDE": CITY,
        "DATUM": DATE,
        "PNUM": KEEP,
        "PBEZ": KEEP,
        "HORBEZ": KEEP,
        "OTIEFEREP": KEEP,
        "UTIEFEREP": KEEP,
        "PFAS_SUMME": PFAS_SUM,
        "PFDoA": SL_VALUE,
        "PFTrA": SL_VALUE,
        "PFTeA": SL_VALUE,
        "PFNS": SL_VALUE,
        "PFOSA": SL_VALUE,
        "LABMETHODE": KEEP,
        "EXTRAKT": KEEP,
        "LABOR": KEEP,
        "BEZUG": KEEP,
    }
    static_values = {
        NAME: "Sampling location",
        MATRIX: "Groundwater",
        TYPE: "Sampling location",
        SOURCE_TEXT: "Hessisches Landesamt für Naturschutz, Umwelt und Geologie",
        COUNTRY: "Germany",
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known",
    }

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df[LAT] = None
        self.df[LON] = None

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("< "):
            return float(val[2:]), True
        return float(val), False

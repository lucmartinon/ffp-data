from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_123(SLNormaliser):
    columns = {
        "Adresse": KEEP,
        "Lat": LAT,
        "Lon": LON,
        "PFBA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFDoA": SL_VALUE,
        "PFDoS": SL_VALUE,
        "PFDS": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFHpS": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFNS": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFPeS": SL_VALUE,
        "PFTrA": SL_VALUE,
        "PFUnA": SL_VALUE,
        "Ville": CITY,
        "Date prélèv.": DATE,
        "PFAS-20": DROP,
        "PFTrS": SL_VALUE,
        "PFUnS": SL_VALUE,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        CATEGORY: "Known",
        COUNTRY: "Belgium",
        SOURCE_URL: "https://www.vivaqua.be/en/",
        SOURCE_TEXT: "Vivaqua",
        TYPE: "Sampling location",
        MATRIX: "Drinking water",
        NAME: "Sampling location",
        SOURCE_TYPE: "Authorities",
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("< "):
            return float(val[2:]), True
        return float(val), False

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_55(MLNormaliser):
    columns = {
        "DATA CAMPIONAMENTO": DATE,
        "CODICE SIRAV PUNTO PRELIEVO": KEEP,
        "PUNTO PRELIEVO": DROP,
        "DESCRIZIONE": KEEP,
        "TIPO DI ANALISI": DROP,
        "PARAMETRO CHIMICO": PARAM,
        "VALORE DETERMINATO": ML_VALUE,
        "UNITA' DI MISURA": UNIT,
        "lat": LAT,
        "lon": LON,
        "name": NAME,
        "GIUDIZIO": DROP,
        "all": DROP,
        "code": DROP,
        "MW": DROP,
    }
    date_format = "%d/%m/%Y %H:%M:%S"
    static_values = {
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known",
        SOURCE_TEXT: "ARPA Veneto",
        TYPE: "Sampling location",
        MATRIX: "Groundwater",
        COUNTRY: "Italy",
    }

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

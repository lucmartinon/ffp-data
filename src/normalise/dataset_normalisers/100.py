from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_100(SLNormaliser):
    static_values = {COUNTRY: "United Kingdom", CATEGORY: "Known"}
    columns = {
        "year": YEAR,
        "type": TYPE,
        "matrix": MATRIX,
        "lat": LAT,
        "lon": LON,
        "source_text": SOURCE_TEXT,
        "source_url": SOURCE_URL,
        "source_type": SOURCE_TYPE,
        "name": NAME,
        "pfas_sum": PFAS_SUM,
        "pfbs": SL_VALUE,
        "pfhps": KEEP,
        "pfhxs": SL_VALUE,
        "pfos": SL_VALUE,
        "6:2 FtS 1-hydroxyethane-2perfluorohexane": KEEP,
        "Fluoroalkylthioamido sulphonate (FATAS-586)": KEEP,
        "Perluorohexanoic acid (PFC-6)": KEEP,
        "Perluorooctanoic acid": KEEP,
        "Polyfluoropentyl sulphonate (PFPS)": KEEP,
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_77(MLNormaliser):
    date_format = "%d/%m/%Y"
    columns = {
        "Date": DATE,
        "Parameter": PARAM,
        "Latitude": LAT,
        "Longitude": LON,
        "matrix": MATRIX,
        "Result": ML_VALUE,
        "Unit of measure": UNIT,
        "Flag": DROP,
        "Monitoring station": NAME,
        "Method": DROP,
    }
    static_values = {
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Lithuania",
        SOURCE_TEXT: "Latvian Environment, Geology and Meteorology Centre (LEGMC)",
        SOURCE_URL: "https://videscentrs.lvgmc.lv/",
        TYPE: "Sampling location",
    }

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False

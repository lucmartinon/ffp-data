from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_122(MLNormaliser):
    columns = {
        "parameter": PARAM,
        "value": ML_VALUE,
        "date": DATE,
        "unit": UNIT,
        "name": NAME,
        "matrix": MATRIX,
        "Dossier": KEEP,
        "sampling_point_id": KEEP,
        "deapth (min/max)": KEEP,
        "affectation_zone": KEEP,
        "L72_x": COORD_X,
        "L72_y": COORD_Y,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        TYPE: "Sampling location",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Bruxelles Environment",
        COUNTRY: "Belgium",
        SOURCE_URL: "https://geodata.environnement.brussels/client/view/13e9e42d-6172-4255-a925-a61cbb14a695",
    }
    proj_epsg_id = 31370

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

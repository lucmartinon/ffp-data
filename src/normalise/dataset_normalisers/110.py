from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_110(SLNormaliser):
    columns = {
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFOS": SL_VALUE,
        "sample N°": KEEP,
        "species": KEEP,
        "povodí": KEEP,
        "Locality": CITY,
    }
    static_values = {
        TYPE: "Sampling location",
        YEAR: "2019",
        SOURCE_TEXT: "Ústav pro životní prostředí Univerzity Karlovy",
        MATRIX: "Biota",
        NAME: "Sampling location",
        COUNTRY: "Czechia",
        SOURCE_URL: "https://www.natur.cuni.cz/fakulta/zivotni-prostredi",
        CATEGORY: "Known",
        SOURCE_TYPE: "Press inquiry",
    }

    @staticmethod
    def _parse_value(val: str):
        # val = val.replace(',', '.')
        if val.startswith("<"):
            return float(val[1:].strip()), True
        return float(val), False

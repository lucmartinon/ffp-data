from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_53(SLNormaliser):
    columns = {
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFDeA": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFUnA": SL_VALUE,
        "PFDoA": SL_VALUE,
        "Region": KEEP,
        "SW / GW": MATRIX,
        "Station ID Code": NAME,
        "Long WGS84 GD": LON,
        "Lat WGS84 GD": LAT,
    }
    static_values = {
        CATEGORY: "Known",
        SOURCE_TEXT: "ISPRA",
        COUNTRY: "Italy",
        TYPE: "Sampling location",
        YEAR: 2018,
        SOURCE_TYPE: "Authorities",
        SOURCE_URL: "https://www.isprambiente.gov.it/it/pubblicazioni/rapporti/indirizzi-perla-progettazione-delle-reti-di-monitoraggio-delle-sostanze-perfluoroalchiliche-pfas-nei-corpi-idrici-superficiali-e-sotterranei",
    }

    replace_map = {
        MATRIX: {"GW": "Groundwater", "SW": "Surface water", "nan": "Unknown"}
    }

    @staticmethod
    def _parse_value(val: str):
        val = str(val).replace(',', '.')
        # they have 2 values that are marked as suspicious, we remove them
        if val.endswith(' [???]'):
            return None, None
        if str(val).startswith('<'):
            return float(val[1:]), True

        return float(val), False

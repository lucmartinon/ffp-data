from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_85(SLNormaliser):
    unit = "ug/l"
    columns = {
        "station": NAME,
        "pfos": SL_VALUE,
        "date": DATE,
        "LATITUDE": LAT,
        "LONGITUDE": LON,
        "RH": KEEP,
        "ARH": KEEP,
        "COD_MA": KEEP,
        "NOME_MA": KEEP,
        "CONCELHO": KEEP,
        "FREGUESIA": KEEP,
        "BACIA": KEEP,
        "SUBBACIA": KEEP,
        "ESTAÇÃO BIOTA-PEIXES": DROP,
    }
    date_format = "mixed"
    static_values = {
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Portugal",
        MATRIX: "Biota",
        SOURCE_TEXT: "Sistema Nacional de Informação de Recursos Hídricos (SNIRH) Departamento de Recursos Hídricos da Agência Portuguesa do Ambiente",
        CATEGORY: "Known",
        TYPE: "Sampling location",
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

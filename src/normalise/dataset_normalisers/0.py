from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_0(SLNormaliser):
    columns = {
        "category": CATEGORY,
        "lat": LAT,
        "lon": LON,
        "name": NAME,
        "city": CITY,
        "country": COUNTRY,
        "type": TYPE,
        "sector": SECTOR,
        "source_type": SOURCE_TYPE,
        "source_text": SOURCE_TEXT,
        "source_url": SOURCE_URL,
        "maps_link": KEEP,
    }
    static_values = {}

    def __init__(self):
        super().__init__()

    @staticmethod
    def _parse_value(val: str):
        try:
            return float(val)
        except ValueError:
            return None

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_12(MLNormaliser):
    unit = "ng/l"
    columns = {
        "lat": LAT,
        "lon": LON,
        "country": COUNTRY,
        "compound": PARAM,
        "y": YEAR,
        "ref_authors": KEEP,
        "ref_title": KEEP,
        "ref_id": KEEP,
        "ref_key": KEEP,
        "ref_year": KEEP,
        "ref_type": KEEP,
        "doi": KEEP,
        "ref_journal": KEEP,
        "_uid_": DROP,
        "id": DROP,
        "site_type": KEEP,
        "urban": KEEP,
        "mid": DROP,
        "parameter": ML_VALUE,
        "m": KEEP,
    }
    static_values = {
        "source_type": "Scientific article",
        "category": "Known",
        "type": "Sampling location",
        "source_text": "Muir 2021",
        "name": "Sampling location",
        "source_url": "https://doi.org/10.1021/acs.est.0c08035",
    }

    @staticmethod
    def _parse_value(val: str):
        if str(val).lower() == "nan":
            return None, None
        try:
            return float(val), False
        except ValueError:
            return None, None

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df[MATRIX] = self.df.apply(
            lambda row: get_matrix(row["site_type"]), axis=1
        )
        self.columns[MATRIX] = MATRIX


# l Lac                                       Surface water
# r Rivière                                   Surface water
# s Mer                                       Sea water
# e Estuaire                                  Sea water
# g Golf                                      Sea water
# c Côte                                      Sea water
# o Océan                                     Sea water
# u Eau sous-terraine                         Ground water
# w Eau sous-terraine dans un forage          Ground water
# m Eau sous-terraine dans un puits           Ground water
# d Eau du robinet provenant d’une source     Drinking water
# t Eau du robinet traitée                    Drinking water
# b Eau de source en bouteille                Drinking water
# c Eau traitée en bouteille                  Drinking water
# n Neige                                     Surface water
# i Glace                                     Surface water
# z Eau de pluie                              Surface water


def get_matrix(site_type):
    if site_type in ["s", "e", "g", "c", "o"]:
        return "Sea water"
    if site_type in ["l", "r", "n", "i", "z"]:
        return "Surface water"
    if site_type in ["u", "w", "m"]:
        return "Ground water"
    if site_type in ["d", "t", "b", "c"]:
        return "Drinking water"

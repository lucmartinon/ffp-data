from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_32(SLNormaliser):
    columns = {
        "order": DROP,
        "category": CATEGORY,
        "lat": LAT,
        "lon": LON,
        "State": KEEP,
        "County": KEEP,
        "Municipality": KEEP,
        "city": CITY,
        "name": NAME,
        "country": COUNTRY,
        "type": TYPE,
        "sector": SECTOR,
        "matrix": MATRIX,
        "actual matrix": DROP,
        "year": YEAR,
        "pfos": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfas_sum": DROP,
        "source_type": SOURCE_TYPE,
        "source_text": SOURCE_TEXT,
        "source_url": SOURCE_URL,
        "details": DETAILS,
        "pfos_pfoa": DROP,
        "pfna": SL_VALUE,
        "pfbs": SL_VALUE,
        "pfhxa": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfas in total": DROP,
        "response by industry": KEEP,
        "note": KEEP,
        "source_url_2": KEEP,
        "source_url_3": KEEP,
    }
    static_values = {}

    @staticmethod
    def _parse_value(val: str):
        try:
            return float(val)
        except ValueError:
            return None, None

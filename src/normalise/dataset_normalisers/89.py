from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_89(SLNormaliser):
    static_values = {
        COUNTRY: "Sweden",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Sveriges geologiska undersökning",
        SOURCE_URL: "https://www.diva-portal.org/smash/record.jsf?pid=diva2%3A1604725&dswid=8636",
    }
    columns = {
        "categoy": KEEP,
        "lat": LAT,
        "lon": LON,
        "source": KEEP,
        "name": NAME,
        "city": CITY,
        "country": COUNTRY,
        "type": TYPE,
        "sector": SECTOR,
        "matrix": MATRIX,
        "year": YEAR,
        "pfos": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfos_pfoa": KEEP,
        "pfna": SL_VALUE,
        "pfbs": SL_VALUE,
        "pfhxa": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfas_sum": PFAS_SUM,
        "source_type": SOURCE_TYPE,
        "source_text": SOURCE_TEXT,
        "source_url": SOURCE_URL,
        "Contamination source": KEEP,
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

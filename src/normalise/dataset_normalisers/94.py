from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_94(MLNormaliser):
    columns = {
        "MEAS_DETERMINAND_CODE": DROP,
        "SAMP_ID": DROP,
        "SMC_DESC": MATRIX,
        "SAMP_PURPOSE_CODE": DROP,
        "PURP_DESC": KEEP,
        "SMPT_TYPE": KEEP,
        "SPT_DESC": KEEP,
        "ARE_DESC": KEEP,
        "Concentration": ML_VALUE,
        "year": YEAR,
        "COUNTRY": DROP,
        "Longitude": LON,
        "Latitude": LAT,
        "Sample_Site_ID": KEEP,
        "SAMP_MATERIAL": DROP,
        "Sample_datetime": DATE,
        "ARE_CODE": KEEP,
        "Screening_Method_Details": DROP,
        "unit": UNIT,
        "less_than": DROP,
        "CAS_Number": PARAM,
        "Compound_Name": DROP,
        "LOD": DROP,
        "method": DROP,
        "Spectral_Fit": DROP,
        "SMPT_LONG_NAME": NAME,
        "OPCAT_NAME": CITY,
        "SMPT_EASTING": DROP,
        "SMPT_NORTHING": DROP,
    }
    date_format = "ISO8601"
    static_values = {
        TYPE: "Sampling location",
        COUNTRY: "United Kingdom",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        SOURCE_URL: "https://www.data.gov.uk/dataset/0c63b33e-0e34-45bb-a779-16a8c3a4b3f7/water-quality-monitoring-data-gc-ms-and-lc-ms-semi-quantitative-screen ",
        SOURCE_TEXT: "Environment Agency",
    }

    replace_map = {
        MATRIX: {
            "RIVER / RUNNING SURFACE WATER": Matrix.SURFACE_WATER,
            "ESTUARINE WATER": Matrix.SURFACE_WATER,
            "GROUNDWATER": Matrix.GROUNDWATER,
            "GROUNDWATER - STATIC/UNPURGED": Matrix.GROUNDWATER,
            "POND / LAKE / RESERVOIR WATER": Matrix.SURFACE_WATER,
            "GROUNDWATER - PURGED/PUMPED/REFILLED": Matrix.GROUNDWATER,
            "SEA WATER": Matrix.SEA_WATER,
            "FINAL SEWAGE EFFLUENT": Matrix.WASTEWATER,
            "SURFACE DRAINAGE": Matrix.SURFACE_WATER,
            "ANY TRADE EFFLUENT": Matrix.WASTEWATER,
            "ANY WATER": Matrix.UNKNOWN,
        }
    }

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), row["less_than"] == "true"

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

import re

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_87(SLNormaliser):
    columns = {
        "Town": DROP,
        "Date of sampling": DATE,
        "Latitude": LAT,
        "Longitude": LON,
        "PFOA": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFBA": SL_VALUE,
        "PFHpS": SL_VALUE,
        "FOUEA": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFDS": SL_VALUE,
        "PFDoDA": SL_VALUE,
        "PFTrDA": SL_VALUE,
        "PFODA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFUnDA": SL_VALUE,
        "PFTeDA": SL_VALUE,
        "i,p-PFNA": SL_VALUE,
        "PFUdA": SL_VALUE,
        "Region (optional)": KEEP,
        "Matrix (drinking water, surface water, ground water, soil, sediment etc.)": MATRIX,
        "Sum of XX PFAS (ng/g) (and provide list) (optional)": DROP,
        "Sum PFOS + PFOA (ng/g) (optional)": DROP,
        "ipPFNA": SL_VALUE,
        "L-PFBS": SL_VALUE,
        "unit": UNIT,
    }
    date_format = "ISO8601"
    static_values = {
        TYPE: "Sampling location",
        NAME: "Sampling location",
        SOURCE_URL: "dx.doi.org/10.1016/j.envres.2016.03.010;dx.doi.org/10.1016/j.envres.2016.03.010;https://doi.org/10.1016/j.scitotenv.2017.06.005;doi.org/10.1016/j.scitotenv.2018.07.304",
        SOURCE_TEXT: "Campo 2016, Campo 2017, Lorenzo 2019",
        CATEGORY: "Known",
        COUNTRY: "Spain",
        SOURCE_TYPE: "Scientific article",
    }

    replace_map = {
        MATRIX: {
            "Soil": Matrix.SOIL,
            "Sediment": Matrix.SEDIMENT,
            "Lake water": Matrix.SURFACE_WATER,
            "Lake sediment (ng/g)": Matrix.SEDIMENT,
            "River water": Matrix.SURFACE_WATER,
            "River sediment (ng/g)": Matrix.SEDIMENT,
        }
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        return float(val), False

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df["Latitude"] = self.df["Latitude"].apply(to_dec_coordinates)
        self.df["Longitude"] = self.df["Longitude"].apply(to_dec_coordinates)

        # arbitrary choice
        self.df["Date of sampling"] = self.df["Date of sampling"].replace(
            {"Winter 2016-2017": "2017-01-01"}
        )

        # the units are ng/g for soil and sediment, ng/l for surface water
        unit_map = {
            "Soil": "ng/g",
            "Sediment": "ng/g",
            "Lake water": "ng/l",
            "Lake sediment (ng/g)": "ng/g",
            "River water": "ng/l",
            "River sediment (ng/g)": "ng/g",
        }
        self.df[UNIT] = self.df[
            "Matrix (drinking water, surface water, ground water, soil, sediment etc.)"
        ].map(unit_map)


def to_dec_coordinates(coord_str):
    elements = re.split("[º°′″'\"]", str(coord_str))
    print(coord_str)
    print(elements)
    elements = [e.replace(",", ".") for e in elements if e != ""]
    deg, min, sec, dir = 0, 0, 0, "N"
    deg = elements[0]
    min = elements[1]

    if len(elements) == 3:
        dir = elements[2]
    elif len(elements) == 4:
        sec = elements[2]
        dir = elements[3]

    res = (float(deg) + float(min) / 60 + float(sec) / (60 * 60)) * (
        -1 if dir in ["W", "S"] else 1
    )
    # print(f"{coord_str} > {res}")
    return res

import logging

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_22(MLNormaliser):
    columns = {
        "city": CITY,
        "x1": COORD_X,
        "y1": COORD_Y,
        "SampleDate": DATE,
        "year": YEAR,
        "matrix": MATRIX,
        "value": ML_VALUE,
        "Unit": UNIT,
        "sampleno": KEEP,
        "Depth1": KEEP,
        "depth2": KEEP,
        "intakeDepth1": DROP,
        "intakeDepth2": DROP,
        "parameterid": DROP,
        "parameter": DROP,
        "CommonName": PARAM,
        "attribute": KEEP,
        "sampleid": DROP,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        SOURCE_TYPE: "Authorities",
        TYPE: "Sampling location",
        COUNTRY: "Denmark",
        CATEGORY: "Known",
        SOURCE_TEXT: "Danske Region Syddanmark ",
    }
    proj_epsg_id = 25832

    replace_map = {
        MATRIX: {
            "Drikkevand": "Drinking water",
            "Groundwater": "Groundwater",
            "Soil": "Soil",
        }
    }

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), str(row["attribute"]).strip() == "<"

    def _parse_value(value_str: str) -> (float, bool):
        logging.error("we should never pass here")
        return None, None

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_104(SLNormaliser):
    unit = 'µg/kg'

    columns = {
        "lat": LAT,
        "lon": LON,
        "PFOA": SL_VALUE,
        "PFOS": SL_VALUE,
        "Depth (m)": KEEP
    }
    static_values = {
        CATEGORY: "Known",
        TYPE: "Sampling location",
        COUNTRY: "Malta",
        YEAR: "2019",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Malta Environment and Resources Authority",
        NAME: "Sampling location",
        MATRIX: "Soil"
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(',', '.')
        if val.startswith('<'):
            return float(val[1:].strip()), True
        return float(val), False

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_79(SLNormaliser):
    unit = "µg/l"
    columns = {
        "Unnamed: 0": KEEP,
        "LOCATION": NAME,
        "EAST MV rytų koordinatė": LAT,
        "NORTH MV šiaurės koordinatė": LON,
        "DATE Tyrimų data": DATE,
        "Perfluoroktano sulfoninė rūgštis (PFOS)": SL_VALUE,
        "Perfluoroktaninė rūgštis (PFOA)": SL_VALUE,
        "Unnamed: 7": DROP,
        "Perfluorobutano sulfonrūgštis (PFBS)": SL_VALUE,
        "Perfluorheksano rūgštis (PFHxA)": SL_VALUE,
        "Perfluorheksano sulfonrūgštis (PFHxS)": SL_VALUE,
        "Perfluorobutano rūgštis (PFBA)": SL_VALUE,
        "Perfluorpentano rūgštis (PFPeA)": SL_VALUE,
        "Perfluoroheptano rūgštis (PFHpA)": SL_VALUE,
        "Perfluoronano rūgštis (PFNR)": SL_VALUE,
        "Perfluorodekano rūgštis (PFDA)": SL_VALUE,
        "Perfluoroundekano rūgštis (PFUnDA)": SL_VALUE,
        "Perfluorodekekano rūgštis (PFDoDA)": SL_VALUE,
        "Perfluoroheptano sulfonrūgštis (PFHpS)": SL_VALUE,
        "Perfluorodekano sulfonrūgštis (PFDS)": SL_VALUE,
        "Perfluoroktano sulfonamidas (FOSA)": SL_VALUE,
        "6: 2 Fluorotelomero sulfonrūgštis (6: 2 FTS)": SL_VALUE,
        "8: 2 Fluorotelomero sulfonrūgštis (8: 2 FTS)": SL_VALUE,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        TYPE: "Sampling location",
        SOURCE_URL: "https://aad.lrv.lt/",
        SOURCE_TEXT: "Lithuanian Environmental Protection Agency",
        SOURCE_TYPE: "Authorities",
        MATRIX: "Surface water",
        CATEGORY: "Known",
        COUNTRY: "Lithuania",
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

    def _preprocess(self):
        self.df = self.raw_df.copy()
        # removig first line with the units
        self.df = self.df.drop([0])

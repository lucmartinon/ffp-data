from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_19(MLNormaliser):
    columns = {
        "Lon": LON,
        "Lat": LAT,
        "Parameter": PARAM,
        "Attribute": KEEP,
        "Value": ML_VALUE,
        "Unit": UNIT,
        "Method": KEEP,
        "WwtpName": DROP,
        "TreatmentType": KEEP,
        "MeasurementPointType": KEEP,
        "MeasurementPoint": NAME,
        "StartDate": DATE,
        "SamplingStarted": DROP,
        "SamplingEnded": DROP,
        "SampleId": DROP,
        "Purpose": DROP,
        "SampleType": KEEP,
        "ParameterCode": DROP,
        "Fraction": DROP,
        "DetectionLimit": DROP,
        "RelativeUncertainty": DROP,
        "AbsoluteUncertainty": DROP,
        "Accredited": DROP,
        "Status": DROP,
        "Reference": DROP,
        "Laboratory": DROP,
        "SampleRemarks1": DROP,
        "SampleRemarks2": DROP,
    }
    date_format = "mixed"
    static_values = {
        MATRIX: "Wastewater",
        TYPE: "Sampling location",
        COUNTRY: "Denmark",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Danish Environmental Protection Agency",
        CATEGORY: "Known",
        SOURCE_URL: "https://eng.mst.dk/",
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

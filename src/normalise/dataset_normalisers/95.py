from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_95(MLNormaliser):
    columns = {
        "National Grid Reference": UK_GRID_REFERENCE,
        "S_SAMPLING_PT_DESC": NAME,
        "ORIGINAL_SAMPLE": DROP,
        "SAMPLE_NUMBER": DROP,
        "S_MEDIA": MATRIX,
        "SAMPLED_DATE": DATE,
        "REPORTED_NAME": PARAM,
        "FORMATTED_ENTRY": ML_VALUE,
        "UNITS": UNIT,
    }
    date_format = "%m/%d/%y %H:%M"
    static_values = {
        CATEGORY: "Known",
        TYPE: "Sampling location",
        SOURCE_TYPE: "FOI",
        SOURCE_TEXT: "Scotland Scottish Environment Protection Agency (SEPA)",
        COUNTRY: "United Kingdom",
    }

    replace_map = {MATRIX: {"RIV_WATER": Matrix.SURFACE_WATER}}

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

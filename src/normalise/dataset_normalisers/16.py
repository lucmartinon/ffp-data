import re

import pandas as pd

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_16(MLNormaliser):
    columns = {
        "id": DROP,
        "opdracht": KEEP,
        "pfasdossiernr": KEEP,
        "profielnaam": NAME,
        "top_in_m": KEEP,
        "basis_in_m": KEEP,
        "jaar": YEAR,
        "datum": DATE,
        "parameter": PARAM,
        "meetwaarde": ML_VALUE,
        "meeteenheid": UNIT,
        "medium": MATRIX,
        "profieltype": KEEP,
        "plaatsing_profiel": KEEP,
        "commentaar": KEEP,
        "x_ml72": COORD_X,
        "y_ml72": COORD_Y,
        "straat": KEEP,
        "gemeente": CITY,
        "detectieconditie": DROP,
        "data_type": KEEP,
        "projectdeel": KEEP,
        "boring": KEEP,
        "diepte_van_m": KEEP,
        "diepte_tot_m": KEEP,
        "nummer": KEEP,
        "analysemonster": KEEP,
        "gegevens": KEEP,
        "peilbuis": KEEP,
        "filter_van_m": KEEP,
        "filter_tot_m": KEEP,
        "code_meetplaats": KEEP,
        "naam_meetplaats": KEEP,
        "startdatum": KEEP,
        "stopdatum": KEEP,
        "bron": KEEP,
        "vha_code": KEEP,
        "locatiebeschrijving": KEEP,
        "motivatie_staalname": KEEP,
        "overschrijdingsfactor_tw": KEEP,
        "beoordeling_tw": KEEP,
        "potentieel_gebruik_bodem": KEEP,
        "overschrijdingsfactor_vlarebo_bijlage_vi": KEEP,
        "beoordeling_bouwkundig_gebruik": KEEP,
        "ogc_fid": KEEP,
        "meetplaats": KEEP,
        "omschrijving": KEEP,
        "vha_segment_code": KEEP,
    }
    static_values = {
        CATEGORY: "Known",
        SOURCE_TEXT: "Databank Ondergrond Vlaanderen",
        COUNTRY: "Belgium",
        SOURCE_URL: "https://www.dov.vlaanderen.be/portaal/?module=pfasverkenner",
        TYPE: "Sampling location",
        SOURCE_TYPE: "Authorities",
    }
    proj_epsg_id = 31370

    replace_map = {
        MATRIX: {
            "Grondwater": Matrix.GROUNDWATER,
            "Vaste deel van de aarde": Matrix.UNKNOWN,
            "Effluent": Matrix.WASTEWATER,
            "Regenwater": Matrix.RAINWATER,
            "Oppervlaktewater": Matrix.SURFACE_WATER,
            "Waterbodem - sediment": Matrix.SEDIMENT,
            "Migratie": Matrix.UNKNOWN,
            "Puur product": Matrix.UNKNOWN,
            "Waterbodem - vaste deel van waterbodem": Matrix.SEDIMENT,
            Matrix.ATMOSPHERE: Matrix.ATMOSPHERE,
            Matrix.SURFACE_WATER: Matrix.SURFACE_WATER,
            Matrix.SOIL: Matrix.SOIL,
            Matrix.GROUNDWATER: Matrix.GROUNDWATER,
            Matrix.BIOTA: Matrix.BIOTA,
        }
    }

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df.loc[self.df["commentaar"] == "Eenheid:ng/l", "meeteenheid"] = "ng/l"
        self.df = self.df[self.df["meeteenheid"] != "onbenoemd"]
        self.df = self.df[~pd.isna(self.df["meeteenheid"])]
        self.df = self.df[~pd.isna(self.df["datum"])]

        matrix_map = {
            "pfas:lantis_bodem_metingen": Matrix.SOIL,
            "pfas:pfas_oppwater": Matrix.SURFACE_WATER,
            "pfas:lucht_zwevendstof_metingen": Matrix.ATMOSPHERE,
            "pfas:lantis_gw_metingen_publiek": Matrix.GROUNDWATER,
            "pfas:lucht_gas_metingen": Matrix.ATMOSPHERE,
            "pfas:pfas_biota": Matrix.BIOTA,
        }

        def matrix(row):
            if not pd.isna(row["medium"]):
                return row["medium"]
            return matrix_map[row["data_type"]]

        self.df["medium"] = self.df.apply(matrix, axis=1)

        self.df["meetwaarde"] = self.df["meetwaarde"].astype(str)

    def _parse_date(self):
        def to_date(val):
            if pd.isna(val):
                return None
            if val == "31/02/2022":
                val = "28/02/2022"
            if len(val) == 10:
                try:
                    if re.match(r"\d\d/\d\d/\d\d\d\d", val):
                        return pd.to_datetime(val, format="%d/%m/%Y")
                    if re.match(r"\d\d\d\d/\d\d/\d\d", val):
                        return pd.to_datetime(val, format="%Y/%m/%d")
                    elif "-" in val:
                        return pd.to_datetime(val, format="%Y-%m-%d")
                except ValueError:
                    pass

            print(f"WTF is this date: {val}")

        self.df[DATE] = self.df[DATE].apply(to_date)

        self.df[YEAR] = pd.DatetimeIndex(self.df[DATE]).year

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

    def _parse_value_from_row(self, row):
        val = row[ML_VALUE]

        # they have value like "5.392,5"
        if "," in val and "." in val:
            val = val.replace(".", "")
        val = val.replace(",", ".")

        return float(val), row["detectieconditie"] == "<"

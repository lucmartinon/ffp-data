from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_57(SLNormaliser):
    unit = "ng/L"
    columns = {
        "Provincia": KEEP,
        "Comune": CITY,
        "Data del prelievo": DATE,
        "Tipologia di acqua campionata": MATRIX,
        "Latitudine": LAT,
        "Longitudine": LON,
        "Acido Perfluoro Butanoico (ng/L) PFBA": SL_VALUE,
        "Perfluoro Heptane Sulfonate (ng/L) PFHpS": SL_VALUE,
        "Perfluoro 2-Propoxy -Propanoic Acid (ng/L) HFPO-DA": SL_VALUE,
        "4:2-Fluoro Telomer Sulfonate (ng/L) 4:2-FTS": SL_VALUE,
        "6:2-Fluoro Telomer Sulfonate (ng/L) 6:2-FTS": SL_VALUE,
        "8:2-Fluoro Telomer Sulfonate (ng/L) 8:2-FTS": SL_VALUE,
        "cC6O4 (CAS 1190931-27-1) (ng/L) [nanog./L] cC6O4": SL_VALUE,
        "cC6O4 (CAS 1190931-41-9) (ng/L) cC6O4": SL_VALUE,
        "Acido Perfluoro Pentanoico (ng/L) PFPeA": SL_VALUE,
        "Perfluoro Butan Sulfonato (ng/L) PFBS": SL_VALUE,
        "Acido Perfluoro Esanoico (ng/L) PFHxA": SL_VALUE,
        "Acido Perfluoro Eptanoico (ng/L) PFHpA": SL_VALUE,
        "Perfluoro Esan Sulfonato (ng/L) PFHxS": SL_VALUE,
        "Acido Perfluoro Ottanoico (ng/L) PFOA": SL_VALUE,
        "Acido Perfluoro Ottanoico (ng/L) PFOA isomero Lineare": SL_VALUE,
        "Acido Perfluoro Ottanoico Isomeri ramificati espressi come PFOA (ng/L) PFOA Isomeri Ramificati": SL_VALUE,
        "Acido Perfluoro Ottanoico (ng/L) PFOA totali somma isomeri": SL_VALUE,
        "Acido Perfluoro Nonanoico (ng/L) PFNA": SL_VALUE,
        "Acido Perfluoro Decanoico (ng/L) PFDeA": SL_VALUE,
        "Perfluoro Ottan Solfonato (ng/L) PFOS": SL_VALUE,
        "Perfluoro Ottan Solfonato (ng/L) PFOS isomero Lineare": SL_VALUE,
        "Perfluoro Ottan Solfonato Isomeri ramificati espressi come PFOS (ng/L) PFOS isomeri Ramificati": SL_VALUE,
        " Perfluoro Ottan Solfonato (ng/L) PFOS totali somma isomeri": SL_VALUE,
        "Acido Perfluoro Undecanoico (ng/L) PFUnA": SL_VALUE,
        "Acido Perfluoro Dodecanoico (ng/L) PFDoA": SL_VALUE,
        "Acido Perfluoro Metil Eptanoico somma di 5 isomeri (ng/L) PMHpA": SL_VALUE,
        "Perfluoro Metil Eptan Sulfonato somma di 6 isomeri (ng/L) PMHpS": SL_VALUE,
        "Cl-PFPECA (0,1) (Presente / Assente) Cl-PFPECA (0,1)": DROP,
        "Cl-PFPECA (0,2) (Presente / Assente) Cl-PFPECA (0,2)": DROP,
        "Cl-PFPECA (1,0) (Presente / Assente) Cl-PFPECA (1,0)": DROP,
        "Cl-PFPECA (1,1) (Presente / Assente) Cl-PFPECA (1,1)": DROP,
        "Cl-PFPECA (2,0) (Presente / Assente) Cl-PFPECA (2,0)": DROP,
    }
    date_format = "%m/%d/%Y"
    static_values = {
        COUNTRY: "Italy",
        SOURCE_TEXT: "ARPA Veneto",
        TYPE: "Sampling location",
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known",
        NAME: "Sampling location",
    }

    replace_map = {
        MATRIX: {
            "SUPERFICIALI": Matrix.SURFACE_WATER,
            "SORGENTI O RISORGIVE": Matrix.SURFACE_WATER,
            "SOTTERRANEE": Matrix.GROUNDWATER,
        }
    }

    # TODO note: i am very unsure of these two:
    # Acido Perfluoro Metil Eptanoico somma di 5 isomeri (ng/L) PMHpA,375-85-9,
    # Perfluoro Metil Eptan Sulfonato somma di 6 isomeri (ng/L) PMHpS,60270-55-5,

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

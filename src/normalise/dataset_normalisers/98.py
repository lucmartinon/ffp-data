from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_98(SLNormaliser):
    columns = {
        "Latitude": LAT,
        "Longtitude": LON,
        "Date": DATE,
        "Branched PFOS": SL_VALUE,
        "City": CITY,
        "Component": NAME,
        "PFBA Perfluoro-n-butanoic acid": SL_VALUE,
        "PFPA Perfluoro-n-pentanoic acid": SL_VALUE,
        "PFHxA Perfluoro-n-hexanoic acid": SL_VALUE,
        "PFBS Perfluoro-1-butanesulfonate": SL_VALUE,
        "PFHpA Perfluoro-n-heptanoic acid": SL_VALUE,
        "6:2 PTS Perfluoro-octane sulfonate 6:2": SL_VALUE,
        "PFOA Perfluoro-n-octanoic acid": SL_VALUE,
        "PFHxS Perfluoro-1-hexanesulfonate": SL_VALUE,
        "PFNA Perfluoro-n-nonanoic acid": SL_VALUE,
        "PFHpS Perfluoro-1-heptanesulfonate": SL_VALUE,
        "PFDA Perfluoro-n-decanoic acid": SL_VALUE,
        "Linear PFOS Perfluoro-1-octanesulfonate": SL_VALUE,
        "PFUnA Perfluoro-n-undecanoic acid": SL_VALUE,
        "PFDoA Perfluoro-n-dodecanoic acid": SL_VALUE,
        "PFOSA Perfluoro-octanesulfonamide": SL_VALUE,
        "PFDS Perfluoro-1-decanesulfonate": SL_VALUE,
        "PFPeS Perfluoro-1-pentanesulfonate": SL_VALUE,
    }
    date_format = "mixed"
    static_values = {
        SOURCE_TYPE: "Own sampling campaign",
        SOURCE_TEXT: "Watershed sampling campaign",
        TYPE: "Sampling location",
        CATEGORY: "Known",
        MATRIX: "Surface water",
        COUNTRY: "United Kingdom",
    }

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

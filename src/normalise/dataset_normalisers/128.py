from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_128(MLNormaliser):
    columns = {
        "lat": LAT,
        "lon": LON,
        "probenahmestelle": NAME,
        "probenahmedatum": DROP,
        "entnahmezeit": KEEP,
        "gruppe": DROP,
        "wert": ML_VALUE,
        "einheit": UNIT,
        "cas_bezeichnung": PARAM,
        "bafu_bezeichnung": DROP,
        "probenahmedatum_date": DATE,
        "probenahmejahr": YEAR
    }
    date_format = 'ISO8601'
    static_values = {
        CATEGORY: "Known",
        MATRIX: "Surface water",
        COUNTRY: "Switzerland",
        TYPE: "Sampling location",
        SOURCE_URL: "https://data.bs.ch/explore/dataset/100068",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Amt für Umwelt und Energie"
    }

    @staticmethod
    def _parse_value(val: str):
        if val == 'n. bestimmbar':
            return None, None
        # val = val.replace(',', '.')
        if val.startswith('<'):
            return float(val[1:].strip()), True
        return float(val), False

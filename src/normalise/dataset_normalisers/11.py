from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_11(SLNormaliser):
    date_format = "%d.%m.%Y"
    columns = {
        "site_type": KEEP,
        "name": NAME,
        "country": COUNTRY,
        "lat": LAT,
        "lon": LON,
        "PFHxS Perfluorohexanesulfonic acid|Potassium perfluorohexanesulfonate|Lithium perfluorohexanesulfonate|Ammonium perfluorohexanesulfonate|Bis(2-hydroxyethyl)ammonium perfluorohexanesulfonate|Sodium perfluorohexanesulfonate": SL_VALUE,
        "PFHxA Perfluorohexanoic acid|Sodium perfluorohexanoate|Ammonium perfluorohexanoate|Potassium undecafluorohexanoate|Undecafluorohexanoic acid--piperazine (1/1)": SL_VALUE,
        "1763-23-1": SL_VALUE,
        "PFBS Perfluorobutanesulfonic acid|Potassium perfluorobutanesulfonate|Sodium nonafluorobutane-1-sulfonate|N,N,N-Tributylbutan-1-aminium nonafluorobutane-1-sulfonate|Lithium nonafluorobutane-1-sulfonate|Sulfonium, triphenyl-, salt with 1,1,2,2,3,3,4,4,4-nonafluoro-1-butanesulfonic acid (1:1)|Diphenyliodanium nonafluorobutane-1-sulfonate|Bis(4-tert-butylphenyl)iodanium nonafluorobutane-1-sulfonate|(4-Cyclohexylphenyl)(diphenyl)sulfanium nonafluorobutane-1-sulfonate|1-(1-Methyl-1H-indol-3-yl)thiolan-1-ium nonafluorobutane-1-sulfonate|1-Ethyl-3-methylpyridin-1-ium nonafluorobutane-1-sulfonate|Ammonium perfluorobutanesulfonate|1-Methyl-3-octyl-1H-imidazolium perfluorobutanesulfonate|3-Hexyl-1-methyl-1H-Imidazolium perfluorobutanesulfonate|N,N-Dimethyl-N-(propan-2-yl)propan-2-aminium nonafluorobutane-1-sulfonate|[4-(2-tert-Butoxy-2-oxoethoxy)phenyl](diphenyl)sulfanium nonafluorobutane-1-sulfonate|N,N-Dibutyl-N-methylbutan-1-aminium nonafluorobutane-1-sulfonate|Magnesium nonafluorobutanesulfonate|N,N,N-Tripropylpentan-1-aminium nonafluorobutane-1-sulfonate|Bis(2-hydroxyethyl)ammonium perfluorobutanesulfonate|Tetrabutylphosphonium perfluorobutanesulfonate": SL_VALUE,
        "PFNA Perfluorononanoic acid|Ammonium perfluorononanoate|Sodium heptadecafluorononanoate|Potassium heptadecafluorononanoate|Heptadecafluorononanoic acid--N-ethylethanamine (1/1)|Heptadecafluorononanoic acid--N-methylmethanamine (1/1)|Methanaminium perfluorononanoate|N,N-Diethylethanaminium heptadecafluorononanoate|Piperidinium perfluorononanoate|Heptadecafluorononanoic acid--aniline (1/1)|Cyclohexanaminium perfluorononanoate": SL_VALUE,
        "335-67-1": SL_VALUE,
        "68140-21-6": SL_VALUE,
        "865-86-1": SL_VALUE,
        "6:2 Fluorotelomer sulfonic acid|Sodium 3,3,4,4,5,5,6,6,7,7,8,8,8-tridecafluorooctane-1-sulfonate|Potassium 3,3,4,4,5,5,6,6,7,7,8,8,8-tridecafluorooctanesulphonate|6:2 Fluorotelomer sulphonate ammonium|1-Octanesulfonic acid, 3,3,4,4,5,5,6,6,7,7,8,8,8-tridecafluoro-, barium salt (2:1)": SL_VALUE,
        "3-(Perfluorodecyl)-2-hydroxypropyl dihydrogen phosphate|Diammonium 3-(perfluorodecyl)-2-hydroxypropyl dihydrogen phosphate": SL_VALUE,
        "2144-53-8": SL_VALUE,
        "34395-24-9": SL_VALUE,
        "864551-38-2": SL_VALUE,
        "70887-94-4": SL_VALUE,
        "4180-26-1": SL_VALUE,
        "335-60-4": SL_VALUE,
        "63967-42-0": SL_VALUE,
        "54009-78-8": SL_VALUE,
        "PFOPA|(Heptadecafluorooctyl)phosphonic acid--4-methylaniline (1/1)": SL_VALUE,
        "Perfluoro-2-methyl-3-oxahexanoic acid|Ammonium perfluoro-2-methyl-3-oxahexanoate|Sodium 2,3,3,3-tetrafluoro-2-(heptafluoropropoxy)propanoate|Potassium 2,3,3,3-tetrafluoro-2-(heptafluoropropoxy)propanoate|2,3,3,3-Tetrafluoro-2-(heptafluoropropoxy)propanoic acid--N-propylpropan-1-amine (1/1)|Triethylaminium perfluoro-2-propoxypropanoate": SL_VALUE,
        "16083-64-0": SL_VALUE,
        "25065-50-3": SL_VALUE,
        "25600-66-2": SL_VALUE,
        "34143-74-3": SL_VALUE,
        "38565-54-7": SL_VALUE,
        "61119-62-8": SL_VALUE,
        "89373-67-1": SL_VALUE,
        "94158-66-4": SL_VALUE,
        "94333-56-9": SL_VALUE,
        "2,2,3,3-Tetrafluoro-3-(heptafluoropropoxy)propionic acid|Sodium 2,2,3,3-tetrafluoro-3-(heptafluoropropoxy)propanoate": SL_VALUE,
        "67118-55-2": SL_VALUE,
        "59587-38-1": SL_VALUE,
        "84100-11-8": SL_VALUE,
        "94200-46-1": SL_VALUE,
        "335-83-1": SL_VALUE,
        "96791-81-0": SL_VALUE,
        "129838-40-0": SL_VALUE,
        "107650-06-6": SL_VALUE,
        "59587-39-2": SL_VALUE,
        "755-02-2": SL_VALUE,
        "129838-39-7": SL_VALUE,
        "216389-85-4": SL_VALUE,
        "38506-72-8": SL_VALUE,
        "1355554-99-2": SL_VALUE,
        "158607-41-1": SL_VALUE,
        "16083-62-8": SL_VALUE,
        "317817-24-6": SL_VALUE,
        "1621953-16-9": SL_VALUE,
        "1263361-03-0": SL_VALUE,
        "307-96-0": SL_VALUE,
        "610778-18-2": SL_VALUE,
        "494798-73-1": SL_VALUE,
        "96383-57-2": SL_VALUE,
        "146983-96-2": SL_VALUE,
        "132665-00-0": SL_VALUE,
        "1807944-82-6": SL_VALUE,
        "423-88-1": SL_VALUE,
        "67963-75-1": SL_VALUE,
        "20015-46-7": SL_VALUE,
        "129838-38-6": SL_VALUE,
        "165281-74-3": SL_VALUE,
        "27619-94-9": SL_VALUE,
        "56554-52-0": SL_VALUE,
        "100646-00-2": SL_VALUE,
        "156323-66-9": SL_VALUE,
        "918-32-1": SL_VALUE,
        "54336-60-6": SL_VALUE,
        "23069-32-1": SL_VALUE,
        "103628-86-0": SL_VALUE,
        "762241-70-3": SL_VALUE,
        "26649-26-3": SL_VALUE,
        "38506-76-2": SL_VALUE,
        "79630-80-1": SL_VALUE,
        "63225-58-1": SL_VALUE,
        "1262446-14-9": SL_VALUE,
        "1287702-48-0": SL_VALUE,
        "1361253-41-9": SL_VALUE,
        "Potassium 3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,12-henicosafluorododecyl sulfate|1H,1H,2H,2H-Perfluorododecanol sulfate ammonium salt": SL_VALUE,
        "Perfluoroheptanoic acid|Ammonium perfluoroheptanoate|Sodium perfluoroheptanoate|Potassium tridecafluoroheptanoate|Caesium perfluoroheptanoate": SL_VALUE,
        "Perfluorodecanoic acid|Ammonium perfluorodecanoate|Sodium perfluorodecanoate": SL_VALUE,
        "Perfluoroundecanoic acid|Sodium henicosafluoroundecanoate|Ammonium perfluoroundecanoate|Potassium henicosafluoroundecanoate|Perfluoroundecanoic acid calcium salt (2:1)": SL_VALUE,
        "375-92-8": SL_VALUE,
        "pfas_sum": PFAS_SUM,
        "date": DATE,
    }
    static_values = {
        SOURCE_TYPE: "Scientific article",
        SOURCE_URL: "https://doi.org/10.1016/j.jhazmat.2022.129276",
        SOURCE_TEXT: "Kelsey Ng 2022",
        CATEGORY: "Known",
        TYPE: "Sampling location",
    }

    @staticmethod
    def _parse_value(val: str):
        try:
            return float(val.replace(",", ".")), False
        except ValueError:
            return None, None

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df[UNIT] = self.df["site_type"].map(unit_map)
        self.columns[UNIT] = UNIT
        self.df[MATRIX] = self.df["site_type"].map(matrix_map)
        self.columns[MATRIX] = MATRIX


matrix_map = {
    "Biota (ng/g w.w.)": "Biota",
    "Effluent waste water (µg/L)": "Wastewater",
    "Ground water (µg/L)": "Groundwater",
    "Influent waste water (µg/L)": "Wastewater",
    "River water (µg/L)": "Surface water",
    "Sediment (ng/g d.w.)": "Sediment",
}

unit_map = {
    "Biota (ng/g w.w.)": "ng/g",
    "Effluent waste water (µg/L)": "ng/l",
    "Ground water (µg/L)": "ng/l",
    "Influent waste water (µg/L)": "ng/l",
    "River water (µg/L)": "ng/l",
    "Sediment (ng/g d.w.)": "ng/g",
}

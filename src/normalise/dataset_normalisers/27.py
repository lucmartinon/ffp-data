from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_27(SLNormaliser):
    columns = {
        "Sampling point": KEEP,
        "lat": LAT,
        "lon": LON,
        "matrix": MATRIX,
        "name": NAME,
        "Dry matter content (%)": KEEP,
        "Sample": KEEP,
        "Unit": UNIT,
        "pfos": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfna": SL_VALUE,
        "pfbs": SL_VALUE,
        "pfhxa": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfas_sum": PFAS_SUM,
        "4:2 FTS": SL_VALUE,
        "6:2 FTS": SL_VALUE,
        "8:2 FTS": SL_VALUE,
        "FOSA": SL_VALUE,
        "N-EtFOSA": SL_VALUE,
        "PFBA": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFDoA": SL_VALUE,
        "PFDS": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFHpS": SL_VALUE,
        "PFHxDA": SL_VALUE,
        "PFODA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFTEDA": SL_VALUE,
        "PFTrDA": SL_VALUE,
        "PFUdA": SL_VALUE,
        "Site": DROP,
        "latlon": DROP,
    }
    static_values = {
        CATEGORY: "Known",
        SOURCE_URL: "https://doi.org/10.1016/j.scitotenv.2022.154237",
        SOURCE_TYPE: "Scientific article",
        SOURCE_TEXT: "Reinikainen 2022 ",
        TYPE: "Sampling location",
        COUNTRY: "Finland",
        YEAR: 2017,
    }

    replace_map = {
        MATRIX: {
            "Sediment": Matrix.SEDIMENT,
            "Surface water": Matrix.SURFACE_WATER,
            "Earthworm": Matrix.BIOTA,
            "Perch": Matrix.BIOTA,
            "Soil": Matrix.SOIL,
            "Ground water": Matrix.GROUNDWATER,
            "Vimba": Matrix.BIOTA,
            "Wastewater": Matrix.WASTEWATER,
        }
    }

    @staticmethod
    def _parse_value(val: str):
        if val == "<LOQ":
            return None, None
        val = val.replace(",", "")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

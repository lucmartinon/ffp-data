from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_93(SLNormaliser):
    columns = {
        "PFOA": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOSA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFUnA": SL_VALUE,
        "Longitude [ETRS89]": LON,
        "Latitude [ETRS89]": LAT
    }
    unit = 'µg/l'
    static_values = {
        NAME: "Sampling location",
        TYPE: "Sampling location",
        MATRIX: "Groundwater",
        SOURCE_TEXT: "Swiss Federal Office for the Environment",
        CATEGORY: "Known",
        COUNTRY: "Switzerland",
        SOURCE_TYPE: "Authorities",
        YEAR: "2008",
        SOURCE_URL: "https://www.bafu.admin.ch/bafu/fr/home/themes/eaux/info-specialistes/etat-des-eaux/etat-des-eaux-souterraines/observation-nationale-des-eaux-souterraines-naqua.html"
    }

    @staticmethod
    def _parse_value(val: str):
        # val = val.replace(',', '.')
        if val.startswith('<'):
            return float(val[1:].strip()), True
        return float(val), False

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_84(SLNormaliser):
    unit = "µg/Kg peso húmido"
    columns = {
        "pfos": SL_VALUE,
        "date": DATE,
        "station": NAME,
        "ESTACÃO ÁGUA SUPERFICIAL": DROP,
        "LATITUDE": LAT,
        "LONGITUDE": LON,
        "RH": KEEP,
        "ARH": KEEP,
        "COD_MA": KEEP,
        "NOME_MA": KEEP,
        "CONCELHO": KEEP,
        "FREGUESIA": KEEP,
        "BACIA": KEEP,
        "SUBBACIA": KEEP,
    }
    date_format = "mixed"
    static_values = {
        SOURCE_TEXT: "Sistema Nacional de Informação de Recursos Hídricos (SNIRH) Departamento de Recursos Hídricos da Agência Portuguesa do Ambiente",
        CATEGORY: "Known",
        TYPE: "Sampling location",
        COUNTRY: "Portugal",
        SOURCE_TYPE: "Authorities",
        MATRIX: "Surface water",
    }

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("(<)"):
            val = val.replace("(<)", "")
            return float(val), True
        else:
            return float(val), False

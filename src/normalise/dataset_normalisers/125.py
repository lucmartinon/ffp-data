from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_125(SLNormaliser):
    columns = {
        "Betrieb": NAME,
        "Betriebs- standort": KEEP,
        "Anhang  der AbwV": KEEP,
        "Proben- nahmedatum": DATE,
        "Labor-Nr.:": DROP,
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFOPA": SL_VALUE,
        "N-MeFOSA-A": SL_VALUE,
        "N-Et-FOSA-A": SL_VALUE,
        "FOSA": SL_VALUE,
        "N-MeFOSA": SL_VALUE,
        "N-Et-FOSA": SL_VALUE,
        "N,N-diMe-FOSA": SL_VALUE,
        "Summe:": PFAS_SUM,
        "Ergebnis  erhalten am:": KEEP,
        "Bemerkungen": KEEP,
        "SGD": KEEP,
    }
    static_values = {
        CATEGORY: "Known",
        MATRIX: "Wastewater",
        SOURCE_TYPE: "Authorities",
        TYPE: "Sampling location",
        SOURCE_TEXT: "Rheinland-Pfalz Ministerium für Klimaschutz, Umwelt, Energie und Mobilität (MKUEM)",
        COUNTRY: "Germany",
    }

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df[LAT] = None
        self.df[LON] = None

    @staticmethod
    def _parse_value(val: str):
        if val in ["x", "n.n.", "n.n", "n.b."]:
            return None, None
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

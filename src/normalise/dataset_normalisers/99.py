from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_99(SLNormaliser):
    columns = {
        "Sample": KEEP,
        "Sample Point": KEEP,
        "name": NAME,
        "EASTING": COORD_X,
        "NORTHING": COORD_Y,
        "NGR": KEEP,
        "SMPT_GRID_REF_KM": UK_GRID_REFERENCE,
        "TYPE": KEEP,
        "type": TYPE,
        "matrix": MATRIX,
        "REGION": KEEP,
        "Unit": UNIT,
        "pfhxs": SL_VALUE,
        "pfos": SL_VALUE,
        "pfas_sum": PFAS_SUM,
        "pfbs": SL_VALUE,
        "pfhxa": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfna": SL_VALUE,
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "4:2 FTS": SL_VALUE,
        "PFPeS": SL_VALUE,
        "FBSA": SL_VALUE,
        "HFPO-DA (GenX)": SL_VALUE,
        "PFHpA": SL_VALUE,
        "ADONA": SL_VALUE,
        "5:3 FTCA": SL_VALUE,
        "6:2 FTS": SL_VALUE,
        "PFHpS": SL_VALUE,
        "8:2 FTS": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFNS": SL_VALUE,
        "FOSA": SL_VALUE,
        "PFDS": SL_VALUE,
        "PFUnA": SL_VALUE,
        "PFUnDS": SL_VALUE,
        "PFDoA": SL_VALUE,
        "PFDoS": SL_VALUE,
        "PFTrDA": SL_VALUE,
        "MeFOSA": SL_VALUE,
        "PFTeA": SL_VALUE,
        "EtFOSA": SL_VALUE,
        "PFHxDA": SL_VALUE,
        "PFODA": SL_VALUE,
        "PFHxS-B": SL_VALUE,
        "PFHxS-L": SL_VALUE,
        "PFecHS": SL_VALUE,
        "FHxSA": SL_VALUE,
        "PFOS-B": SL_VALUE,
        "PFOS-L": SL_VALUE,
        "HFPO-TA": SL_VALUE,
        "9Cl-PF3ONS (6:2 Cl-PFESA)": SL_VALUE,
        "MeFOSAA-B": SL_VALUE,
        "MeFOSAA-L": SL_VALUE,
        "EtFOSAA-B": SL_VALUE,
        "EtFOSAA-L": SL_VALUE,
        "11Cl-PF3OUdS (8:2 Cl-PFESA)": SL_VALUE,
    }
    static_values = {
        SOURCE_TYPE: "FOI",
        YEAR: "0",
        CATEGORY: "Known",
        SOURCE_TEXT: "Environment Agency",
        COUNTRY: "United Kingdom",
    }
    proj_epsg_id = 27700

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("< "):
            return float(val[2:]), True
        return float(val), False

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_45(SLNormaliser):
    unit = "µg/kg"
    columns = {
        "IME_SAMPLEID": KEEP,
        "Gewaesser": NAME,
        "TaAn_PFBA": SL_VALUE,
        "TaAn_PFPeA": SL_VALUE,
        "TaAn_PFHxA": SL_VALUE,
        "TaAn_PFHpA": SL_VALUE,
        "TaAn_PFOA": SL_VALUE,
        "TaAn_PFNA": SL_VALUE,
        "TaAn_PFDA": SL_VALUE,
        "TaAn_PFUnA": SL_VALUE,
        "TaAn_PFDoA": SL_VALUE,
        "TaAn_PFBS": SL_VALUE,
        "TaAn_PFOS_lin": SL_VALUE,
        "TaAn_PFOS_bra": SL_VALUE,
        "lat": LAT,
        "lon": LON,
        "ID": DROP,
        "Standort": KEEP,
    }
    static_values = {
        CATEGORY: "Known",
        SOURCE_URL: "https://sumpfas.ime.fraunhofer.de/",
        TYPE: "Sampling location",
        MATRIX: "Sediment",
        SOURCE_TEXT: "Fraunhofer",
        COUNTRY: "Germany",
        SOURCE_TYPE: "Authorities",
        YEAR: 2021,
    }

    @staticmethod
    def _parse_value(val: str):
        try:
            return float(val), False
        except ValueError:
            return None, None

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_92(MLNormaliser):
    columns = {
        "probenahmestelle": NAME,
        "probenahmedatum": DATE,
        "entnahmezeit": KEEP,
        "gruppe": DROP,
        "parameter": DROP,
        "wert": ML_VALUE,
        "einheit": UNIT,
        "cas_bezeichnung": PARAM,
        "bafu_bezeichnung": DROP,
        "probenahmedatum_date": DROP,
        "probenahmejahr": YEAR,
        "lat": LAT,
        "lon": LON,
        "probentyp": DROP,
        "geo_point_2d": DROP,
        "x_coord": DROP,
        "y_coord": DROP,
        "probenahmedauer": DROP,
        "reihenfolge": DROP,
        "bg": DROP,
        "wert_num": DROP,
        "auftragnr": DROP,
        "probennr": DROP,
        "resultatnummer": DROP,
        "automatische_auswertung": DROP,
        "allgemeine_parametergruppe": KEEP
    }
    date_format = '%d.%m.%Y'
    static_values = {
        SOURCE_TEXT: "Swiss Federal Office for the Environment",
        TYPE: "Sampling location",
        MATRIX: "Surface water",
        COUNTRY: "Switzerland",
        SOURCE_URL: "https://www.bafu.admin.ch/bafu/en/home.html",
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known"
    }

    @staticmethod
    def _parse_value(val: str):
        if val == 'n. bestimmbar':
            return None, None
        # val = val.replace(',', '.')
        if val.startswith('<'):
            return float(val[1:].strip()), True
        return float(val), False

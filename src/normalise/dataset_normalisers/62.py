from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_62(SLNormaliser):
    columns = {
        "PFOA": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFDeA": SL_VALUE,
        "PFUnA": SL_VALUE,
        "PFDoA": SL_VALUE,
        "GenX": SL_VALUE,
        "C6O4": SL_VALUE,
        "Codice Lab": KEEP,
        "Data prelievo": DATE,
        "Ora prelievo": KEEP,
        "Tipo campione": KEEP,
        "Ore": KEEP,
        "Punto Prelievo": NAME,
        "Luogo prelievo/Corpo idrico": KEEP,
        "Codice stazione": KEEP,
        "Coord X": COORD_X,
        "Coord Y": COORD_Y,
        "N° Verbale/Prot.": KEEP,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        COUNTRY: "Italy",
        MATRIX: "Surface water",
        CATEGORY: "Known",
        SOURCE_URL: "Press request",
        SOURCE_TEXT: "ARPA Bolzano",
        TYPE: "Sampling location",
        SOURCE_TYPE: "Authorities",
    }
    proj_epsg_id = 25832

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(" ", "")
        if val.startswith("<"):
            return float(val[1:]), True
        else:
            return float(val), False

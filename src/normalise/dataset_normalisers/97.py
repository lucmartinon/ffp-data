from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_97(SLNormaliser):
    columns = {
        "name": NAME,
        "matrix": MATRIX,
        "type": TYPE,
        "year": YEAR,
        "lat": LAT,
        "lon": LON,
        "Matrix": DROP,
        "pfas_sum": PFAS_SUM,
        "pfos": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfna": SL_VALUE,
        "pfhxa": SL_VALUE,
        "pfbs": SL_VALUE,
        "Bottle ref": KEEP,
        "Water Co": KEEP,
        "Date of sampling": DATE,
        "Units": UNIT,
        "PFBA (357-22-4) Perfluoro-n-butanoic acid": SL_VALUE,
        "PFPA (2706-90-3) Perfluoro-n-pentanoic acid": SL_VALUE,
        "PFHpA (375-85-9) Perfluoro-n-heptanoic acid": SL_VALUE,
        "6:2PTS (27619-97-2) Perfluoro-octane sulfonate 6:2": SL_VALUE,
        "PFHpS (375-92-8) Perfluoro-1-heptanesulfonate": SL_VALUE,
        "PFDA (335-76-2) Perfluoro-n-decanoic acid": SL_VALUE,
        "Linear PFOS(1763-23-1) Perfluoro-1-octanesulfonate": SL_VALUE,
        "Branched PFOS": SL_VALUE,
        "PFUnA (2058-94-8) Perfluoro-n-undecanoic acid": SL_VALUE,
        "PFDoA (307-55-1) Perfluoro-n-dodecanoic acid": SL_VALUE,
        "PFOSA (754-91-6) Perfluoro-octanesulfonamide": SL_VALUE,
        "PFDS (335-73-3) Perfluoro-1-decanesulfonate": SL_VALUE,
        "PFPeS (2706-91-4) Perfluoro-1-pentanesulfonate": SL_VALUE,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        CATEGORY: "Known",
        COUNTRY: "United Kingdom",
        SOURCE_TYPE: "Own sampling campaign",
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

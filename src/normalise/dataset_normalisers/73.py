import numpy as np

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_73(MLNormaliser):
    unit = "ng/L"
    columns = {
        "PROVINCIA": KEEP,
        "METODO": KEEP,
        "STAZIONE_TIPO": KEEP,
        "STAZIONE_MON": DROP,
        "STAZIONE_ID": KEEP,
        "MATRICE": MATRIX,
        "SUBSITO": KEEP,
        "STA_ATTIVA": DROP,
        "STAZIONE_NOME": NAME,
        "STA_WISE_ID": KEEP,
        "STA_GB_E": COORD_X,
        "STA_GB_N": COORD_Y,
        "PROF_M": KEEP,
        "LOCALITA": DROP,
        "STAZIONE_USO": KEEP,
        "AUTORITA_BACINO": KEEP,
        "CORSO_ID": KEEP,
        "CORSO_NOME": KEEP,
        "CORPO_IDRICO_TIPO": KEEP,
        "CORPO_IDRICO_ID": KEEP,
        "CORPO_IDRICO_NOME": KEEP,
        "COMUNE_NOME": CITY,
        "PARAMETRO_TIPO": DROP,
        "PARAMETRO_GRUPPO": DROP,
        "PARAMETRO_SOSTANZA_PRIORITARIA": DROP,
        "PARAMETRO_SOE": DROP,
        "PARAMETRO_CAS": PARAM,
        "PARAMETRO_ID": DROP,
        "PARAMETRO_NOME": DROP,
        "PRELIEVO": DROP,
        "FONTE": DROP,
        "NOTE": DROP,
        "DATO_ORIGINE": DROP,
        "ANNO": YEAR,
        "SEMESTRE": DROP,
        "TRIMESTRE": DROP,
        "MESE": DROP,
        "DATA_NUM": DROP,
        "DATA": DATE,
        "VALORE_ALFANUM": ML_VALUE,
        "VALORE_NUM": DROP,
        "VALORE_NUM_CONV": DROP,
        "VALORE_STATO": DROP,
        "VALORE_SOGLIA_SQA_MA": DROP,
        "VALORE_SOGLIA_SQA_CMA": DROP,
        "VALORE_NOTE": DROP,
        "VAL_ID": DROP,
        "STA_MONIT": DROP,
        "STA_POZ_PROF_M": KEEP,
        "STA_POZ_TIPO_FALDA": DROP,
        "CORPO_IDRICO_RISCHIO": KEEP,
        "COMUNE_ID": DROP,
        "PARAMETRO_NORMA": DROP,
        "VALORE_SOGLIA": DROP,
        "VALORE_SOGLIA_VFN": DROP,
        "SUBSITO_GB_E": DROP,
        "SUBSITO_GB_N": DROP,
    }
    date_format = "ISO8601"
    static_values = {
        SOURCE_URL: "Press request",
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known",
        SOURCE_TEXT: "ARPA Toscana",
        COUNTRY: "Italy",
        TYPE: "Sampling location",
    }
    proj_epsg_id = 3003

    replace_map = {
        MATRIX: {
            "ACQ": Matrix.SURFACE_WATER,
            "BNT": Matrix.SURFACE_WATER,
            "BIO": Matrix.BIOTA,
            np.nan: Matrix.SURFACE_WATER,
        }
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(" ", "")
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

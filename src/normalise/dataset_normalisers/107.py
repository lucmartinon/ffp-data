from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_107(SLNormaliser):
    columns = {
        "name": NAME,
        "PFBuS": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFDeA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "lat": LAT,
        "lon": LON
    }
    static_values = {
        MATRIX: "Leachate",
        YEAR: "2019",
        COUNTRY: "Greece",
        SOURCE_TEXT: "Nika 2020",
        TYPE: "Sampling location",
        SOURCE_URL: "https://doi.org/10.1016/j.jhazmat.2020.122493",
        SOURCE_TYPE: "Scientific article",
        CATEGORY: "Known"
    }
    unit = 'μg/L'

    @staticmethod
    def _parse_value(val: str):
        # val = val.replace(',', '.')
        if val.startswith('<'):
            return float(val[1:].strip()), True
        return float(val), False

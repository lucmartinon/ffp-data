from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_126(SLNormaliser):
    columns = {
        "Lebensmittel": KEEP,
        "Betriebsort": KEEP,
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFUnDA": SL_VALUE,
        "PFDoDA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFPeS": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFHpS": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFNS": SL_VALUE,
        "PFDS": SL_VALUE,
        "4:2-Fluortelomersulfonsäure": SL_VALUE,
        "6:2-Fluortelomersulfonsäure": SL_VALUE,
        "8:2- Fluortelomersulfonsäure": SL_VALUE,
        "ADONA": SL_VALUE,
        "F-35B": SL_VALUE,
        "Summe alle PFTs": DROP,
        "Summe PFOA, PFNA, PFHxS und PFOS": DROP,
        "city": CITY,
        "matrix": MATRIX,
        "year": YEAR,
        "name": NAME,
        "GenX ": SL_VALUE,
    }
    static_values = {
        SOURCE_URL: "https://www.ages.at/forschung/wissen-aktuell/detail/popmon",
        SOURCE_TEXT: "AGES",
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known",
        COUNTRY: "Austria",
        TYPE: "Sampling location",
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.strip()
        val = val.replace(",", ".")
        if val == "n.n.":
            return None, None

        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_9(SLNormaliser):
    columns = {
        "pfasdossiernr": KEEP,
        "coord_x": COORD_X,
        "coord_y": COORD_Y,
        "FID": KEEP,
        "id": KEEP,
        "gemeente": CITY,
        "straat": KEEP,
        "status": KEEP,
        "creatiedatum": KEEP,
        "updatedatum": KEEP,
        "versiedatum": KEEP,
        "locatietype": KEEP,
        "geometriebron": DROP,
        "geom": DROP,
    }
    static_values = {
        TYPE: "Firefighting incident / training",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Databank Ondergrond Vlaanderen",
        SOURCE_URL: "https://www.dov.vlaanderen.be/geonetwork/srv/eng/catalog.search#/metadata/97e28171-1171-41dc-ae71-6ae1e5d28568",
        COUNTRY: "Belgium",
        CATEGORY: "Presumptive",
        NAME: "Fire incident",
    }
    proj_epsg_id = 31370

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

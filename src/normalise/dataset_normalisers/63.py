from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_63(SLNormaliser):
    columns = {
        "Punto di monitoraggio": NAME,
        "Codice Arpal": KEEP,
        "Y_GB": COORD_Y,
        "X_GB": COORD_X,
        "Data misura": DATE,
        "Quota sensore (m)": KEEP,
        "Ac. Perfluorobutanoico (PFBA)": SL_VALUE,
        "Ac. Perfluorobutansolfonico (PFBS)": SL_VALUE,
        "Ac. Perfluoroesanoico (PFHxA)": SL_VALUE,
        "Ac. Perfluoroottanoico (PFOA)": SL_VALUE,
        "Ac. Perfluoroottansolfonico e suoi sali (PFOS)": SL_VALUE,
        "Ac. Perfluoropentanoico (PFPeA)": SL_VALUE,
    }
    date_format = "%d/%m/%Y %H:%M"
    static_values = {
        MATRIX: "Surface water",
        SOURCE_URL: "Press request",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Italy",
        TYPE: "Sampling location",
        CATEGORY: "Known",
        SOURCE_TEXT: "ARPA Liguria",
    }
    proj_epsg_id = 3003
    unit = "microg/l"

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(" ", "")
        if val.startswith("<"):
            return float(val[1:]), True
        else:
            return float(val), False

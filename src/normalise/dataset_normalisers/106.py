from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_106(SLNormaliser):
    columns = {
        "Bodentiefe BIS:": KEEP,
        "Jahr der Probenahme": YEAR,
        "Koord_X_LV95_a": COORD_Y,  # yes the names seem to be inverted in the source file.
        "Koord_Y_LV95_a": COORD_X,
        "Standort-Gemeinde": CITY,
        "Höhe M.ü.M": KEEP,
        "Nutzung": KEEP,
        "PFBS": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFNS": SL_VALUE,
        "PFDS": SL_VALUE,
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFUnDA": SL_VALUE,
        "PFDoDA": SL_VALUE,
        "PFTeDA": SL_VALUE,
        "MeFOSAA": SL_VALUE,
        "EtFOSAA": SL_VALUE,
        "FOSA": SL_VALUE,
        "EtFOSA": SL_VALUE,
        "MeFOSA": SL_VALUE,
        "DONA": SL_VALUE,
        "Agroscope ID": KEEP,
        "Standort_Kennung_a": NAME,
        "Kt.": KEEP,
        "Kollektiv": KEEP,
        "Reihenfolge wieder-herstellen": KEEP,
        "PFOSlin": SL_VALUE,
        "8:2-FTUCA": SL_VALUE,
        "9Cl-PF3ONS / F-53B": SL_VALUE
    }
    static_values = {
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        TYPE: "Sampling location",
        MATRIX: "Soil",
        SOURCE_TEXT: "Swiss Federal Office for the Environment",
        COUNTRY: "Switzerland"
    }
    proj_epsg_id = 2056
    unit = 'µg/kg'

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

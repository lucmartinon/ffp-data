from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_76(MLNormaliser):
    columns = {
        "Monitoring station": NAME,
        # yes, this is not a mistake, x and y are inverted in the source document.
        "X": COORD_Y,
        "Y": COORD_X,
        "Date": DATE,
        "Parameter": PARAM,
        "Result": ML_VALUE,
        "Flag": DROP,
        "Unit of measurement": UNIT,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        CATEGORY: "Known",
        COUNTRY: "Latvia",
        SOURCE_URL: "https://videscentrs.lvgmc.lv/",
        MATRIX: "Groundwater",
        TYPE: "Sampling location",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Latvian Environment, Geology and Meteorology Centre (LEGMC)",
    }
    proj_epsg_id = 3059

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), row["Flag"] == 782

    @staticmethod
    def _parse_value(val: str):
        pass

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_30(SLNormaliser):
    columns = {
        "Date prélèvement": DATE,
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFDA": SL_VALUE,
        "Région": KEEP,
        "Dpt": KEEP,
        "Commune": CITY,
        "Bassin": KEEP,
        "Code SISE EAUX (EB)": KEEP,
        "PSV - Code national": KEEP,
        "Nom ressource": KEEP,
        "CAP - Type - Nom": KEEP,
        "PSV - Nom": NAME,
        "PSV - Commune - Nom": DROP,
        "PSV - Lieu": KEEP,
        "Code SISE EAUX TTP/UDI  (ET)": KEEP,
        "Sélection": KEEP,
        "Type d'eau": KEEP,
        "ESO/ESU": MATRIX,
        "Date réception": KEEP,
        "délai plvt réception (j)": KEEP,
    }
    date_format = "ISO8601"
    unit = "ug/L"
    static_values = {
        SOURCE_URL: "https://www.anses.fr/fr/system/files/LABO-Ra-Perfluorates.pdf",
        TYPE: "Sampling location",
        SOURCE_TEXT: "French Agency for Food, Environmental and Occupational Health & Safety (ANSES)",
        SOURCE_TYPE: "FOI",
        COUNTRY: "France",
        CATEGORY: "Known",
    }

    replace_map = {MATRIX: {"ESO": Matrix.GROUNDWATER, "ESU": Matrix.SURFACE_WATER}}

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

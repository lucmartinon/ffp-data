import logging

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_20(MLNormaliser):
    columns = {
        "city": CITY,
        "year": YEAR,
        "matrix": MATRIX,
        "projectno": KEEP,
        "pointno": KEEP,
        "x1": COORD_X,
        "y1": COORD_Y,
        "coordinateQualityT": DROP,
        "coordinateMethodT": DROP,
        "SampleDate": DATE,
        "sampleno": KEEP,
        "parameterid": DROP,
        "parameter": DROP,
        "CommonName": PARAM,
        "attribute": KEEP,
        "value": ML_VALUE,
        "Unit": UNIT,
        "sampleid": DROP,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Danske Region Sjælland ",
        COUNTRY: "Denmark",
        CATEGORY: "Known",
        TYPE: "Sampling location",
    }
    proj_epsg_id = 25832

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), str(row["attribute"]).strip() == "<"

    def _parse_value(value_str: str) -> (float, bool):
        logging.error("we should never pass here")
        return None, None

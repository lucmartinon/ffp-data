from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_127(MLNormaliser):
    columns = {
        "data": DATE,
        "cod_stazione": KEEP,
        "Corpo Idrico": KEEP,
        "Nome_punto": NAME,
        "E_UTM50": COORD_X,
        "N_UTM50": COORD_Y,
        "parametro": PARAM,
        "valore": ML_VALUE,
        "UdM": UNIT,
        "LOQ": DROP,
    }
    date_format = "%Y-%m-%d"
    static_values = {
        COUNTRY: "Italy",
        MATRIX: "Groundwater",
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known",
        TYPE: "Sampling location",
    }
    proj_epsg_id = 23032

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

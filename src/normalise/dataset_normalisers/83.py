from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_83(SLNormaliser):
    columns = {
        "Sampling area": KEEP,
        "Sample name": NAME,
        "E": COORD_X,
        "N": COORD_Y,
        "pfhxa": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfna": SL_VALUE,
        "pfbs": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfos": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFUnDA": SL_VALUE,
        "PFDoDA": SL_VALUE,
        "PFTrDA": SL_VALUE,
        "PFTeDA": SL_VALUE,
        "PFPeDA": SL_VALUE,
        "PFHxDA": SL_VALUE,
        "PFHpS": SL_VALUE,
        "Br-PFOS": SL_VALUE,
        "PFDS": SL_VALUE,
        "PFDoDS": SL_VALUE,
        "FOSA": SL_VALUE,
        "MeFOSA": SL_VALUE,
        "EtFOSA": SL_VALUE,
        "EtFOSE": SL_VALUE,
        "FOSAA": SL_VALUE,
        "MeFOSAA": SL_VALUE,
        "EtFOSAA": SL_VALUE,
        "6:2 FTS": SL_VALUE,
        "8:2 FTS": SL_VALUE,
        "10:2 FTS": SL_VALUE,
        "12:2 FTS": SL_VALUE,
        " PFHpA": SL_VALUE,
        "14:2 FTS": SL_VALUE,
    }
    static_values = {
        CATEGORY: "Known",
        MATRIX: "Surface water",
        TYPE: "Sampling location",
        SOURCE_TYPE: "Scientific article",
        SOURCE_TEXT: "Langberg 2021",
        YEAR: "2021",
        COUNTRY: "Norway",
        SOURCE_URL: "https://doi.org/10.1016/j.envpol.2020.116259",
    }
    proj_epsg_id = 25832

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_109(SLNormaliser):
    
    static_values = {
        COUNTRY: "Greece",
        CATEGORY: "Known",
        SOURCE_TYPE: "Scientific article",
        SOURCE_TEXT: "Thomaidis 2020"
    }
    columns = {
        "country": COUNTRY,
        "year": YEAR,
        "type": TYPE,
        "name": NAME,
        "matrix": MATRIX,
        "lat": LAT,
        "lon": LON,
        "pfas_sum": PFAS_SUM,
        "pfhxa": SL_VALUE,
        "pfoa": SL_VALUE,
        "pfhxs": SL_VALUE,
        "pfos": SL_VALUE,
        "pfbs": SL_VALUE
    }

    @staticmethod
    def _parse_value(val: str):
        # val = val.replace(',', '.')
        if val.startswith('<'):
            return float(val[1:].strip()), True
        return float(val), False

import logging

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_21(MLNormaliser):
    columns = {
        "SampleDate": DATE,
        "year": YEAR,
        "matrix": MATRIX,
        "name": NAME,
        "Parameter": DROP,
        "Unit": UNIT,
        "ProjectNo": KEEP,
        "Title": KEEP,
        "PointNo": KEEP,
        "PublicNo": KEEP,
        "X1": COORD_X,
        "Y1": COORD_Y,
        "IntakeNo": DROP,
        "PrøveDybde": KEEP,
        "IndtagTopDybde": DROP,
        "IndtagBundDybde": DROP,
        "ReportNo": DROP,
        "SampleNo": DROP,
        "GroupName": DROP,
        "ParameterId": DROP,
        "CommonName": PARAM,
        "Attribute": KEEP,
        "Value": ML_VALUE,
        "BrancheAktivitet1": KEEP,
        "BrancheAktivitet2": KEEP,
        "BrancheAktivitet3": KEEP,
        "BrancheAktivitet4": KEEP,
        "BrancheAktivitet5": KEEP,
    }
    date_format = "ISO8601"
    static_values = {
        COUNTRY: "Denmark",
        SOURCE_TEXT: "Danske Region Midtjyllands ",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        TYPE: "Sampling location",
    }
    proj_epsg_id = 25832

    def _parse_value_from_row(self, row):
        return float(row[ML_VALUE]), str(row["Attribute"]).strip() == "<"

    def _parse_value(value_str: str) -> (float, bool):
        logging.error("we should never pass here")
        return None, None

from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_124(MLNormaliser):
    """Data from the drinking water public register"""
    columns = {
        "inae": MATRIX,
        "referenceprel": KEEP,
        "dateprel": DATE,
        "heureprel": DROP,
        "coord_x": COORD_X,
        "coord_y": COORD_Y,
        "nomcommune": CITY,
        "distrlib": KEEP,
        "casparam": PARAM,
        "cdparametre": DROP,
        "libminparametre": DROP,
        "rqana": ML_VALUE,
        "cdunitereferencesiseeaux": UNIT,
    }
    static_values = {
        SOURCE_TEXT: "French ministry of health",
        SOURCE_URL: "https://www.data.gouv.fr/fr/datasets/resultats-du-controle-sanitaire-de-leau-du-robinet/",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "France",
        CATEGORY: "Known",
        NAME: "Sampling location",
        TYPE: "Sampling location",
    }
    proj_epsg_id = 9793

    replace_map = {
        MATRIX: {
            "ESU": "Groundwater",
            "ESO": "Surface water",
            "EMI": "Surface water",
            "MER": "Sea water",
        },
    }

    @staticmethod
    def _parse_value(val: str):
        if val.strip() == "?":
            return None, None
        val = val.replace(" ", "")
        val = val.replace(",", ".")
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False

    def _preprocess(self):
        self.df = self.raw_df.copy()

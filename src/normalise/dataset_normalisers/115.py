from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_115(MLNormaliser):
    columns = {
        "Commune": CITY,
        "Bassin": KEEP,
        "X": DROP,
        "Y": DROP,
        "lat": LAT,
        "lon": LON,
        "Paramètre": PARAM,
        "Code station": KEEP,
        "Localité": NAME,
        "Cours d'eau": KEEP,
        "Masse d'eau surveillée principale": KEEP,
        "Masses d'eau surveillées secondaires": KEEP,
        "Masse d'eau du site de contrôle": KEEP,
        "Code paramètre": DROP,
        "Code forme": DROP,
        "Forme": DROP,
        "Date de prélèvement": DATE,
        "Heure de prélèvement": DROP,
        "Donnée brute": ML_VALUE,
        "Donnée traitée": DROP,
        "Unités": UNIT,
        "Statut": DROP,
        "Substance détectée": DROP,
        "Commentaire": DROP,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        SOURCE_TEXT: "Service public de Wallonie (SPW)",
        SOURCE_TYPE: "Authorities",
        MATRIX: "Surface water",
        COUNTRY: "Belgium",
        TYPE: "Sampling location",
        CATEGORY: "Known",
    }

    @staticmethod
    def _parse_value(val: str):
        if str(val).startswith("<"):
            return float(val[1:].strip()), True
        return float(val), False

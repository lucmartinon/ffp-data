from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_60(MLNormaliser):
    columns = {
        "Province": KEEP,
        "Municipality": KEEP,
        "Date": DATE,
        "CAS": PARAM,
        "UM": UNIT,
        "Sampling Spot Code": KEEP,
        "Ground Water Body (GWB)": NAME,
        "Substance": DROP,
        "COORD (EST)": COORD_X,
        "COORD (NORD)": COORD_Y,
        "Concentrazion": ML_VALUE,
    }
    date_format = "mixed"
    static_values = {
        SOURCE_TYPE: "Authorities",
        TYPE: "Sampling location",
        CATEGORY: "Known",
        SOURCE_URL: "Press request",
        MATRIX: "Groundwater",
        SOURCE_TEXT: "ARPA Lombardia",
        COUNTRY: "Italy",
    }
    proj_epsg_id = 25832

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False

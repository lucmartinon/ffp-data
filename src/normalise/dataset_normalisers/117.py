from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_117(MLNormaliser):
    columns = {
        "Commune": CITY,
        "NomSection": KEEP,
        "site": KEEP,
        "nomoulieu": NAME,
        "date du prélèvement": DATE,
        "expression": UNIT,
        "masse": KEEP,
        "Ss-réseau": KEEP,
        "RéfContexte": KEEP,
        "codeESO": DROP,
        "valeurmesurée": ML_VALUE,
        "Symbole": PARAM,
        "description": DROP,
        "sigletitul": KEEP,
        "XLambert": COORD_X,
        "YLambert": COORD_Y,
        "lat": LAT,
        "lng": LON,
        "type": DROP,
    }
    static_values = {
        "category": "Known",
        "source_type": "Authorities",
        "source_text": "Service public de Wallonie (SPW)",
        "country": "Belgium",
        "type": "Sampling location",
        "matrix": "Surface water",
    }
    proj_epsg_id = 31370

    @staticmethod
    def _parse_value(val_str: str):
        val_str = str(val_str).replace(",", ".")
        float_val = float(val_str)

        if float_val == 0:
            return None, None

        if float_val < 0:
            return 0 - float_val, True
        return float_val, False

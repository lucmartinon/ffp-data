from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_25(MLNormaliser):
    columns = {
        "name": NAME,
        "lon": LON,
        "lat": LAT,
        "year": YEAR,
        "Parameter": PARAM,
        "Flag": DROP,
        "Result": ML_VALUE,
        "Unit": UNIT,
        "ETRS-TM35FIN x": KEEP,
        "ETRS-TM35FIN y": KEEP,
        "Site depth": KEEP,
        "Site type": KEEP,
        "Sampling time": DROP,
        "Sampling depth": KEEP,
        "Method": KEEP,
        "Lab": KEEP,
    }
    static_values = {
        TYPE: "Sampling location",
        SOURCE_TYPE: "Authorities",
        SOURCE_TEXT: "Finnish Environment Institute",
        CATEGORY: "Known",
        MATRIX: "Surface water",
        COUNTRY: "Finland",
        SOURCE_URL: "https://www.syke.fi",
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        return float(val), False

    def _parse_value_from_row(self, row):
        val = row[ML_VALUE]
        val = val.replace(",", ".")
        return float(val), row["Flag"] in ["L", "LT"]

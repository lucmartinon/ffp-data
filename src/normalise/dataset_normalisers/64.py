import pandas as pd

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_64(SLNormaliser):
    date_format = "%d/%m/%Y"
    unit = "ug/L"
    columns = {
        "Type": TYPE,
        "Municipality": CITY,
        "Date": DATE,
        "Sampling point code": KEEP,
        "Hydric Body": NAME,
        "Basin": KEEP,
        "River Auction": KEEP,
        "EU Sampling Point Code": KEEP,
        "Longitude (X)": COORD_X,
        "Latitude (Y)": COORD_Y,
        "Actual Coordinates X_WGS84": DROP,
        "Actual Coordinates Y_WGS84": DROP,
        "Altitude": KEEP,
        "Lab Code": KEEP,
        "Acido Perfluoroottanoico (PFOA)": SL_VALUE,
        "Acido perfluorottansolfoni (PFOS)": SL_VALUE,
        "Acido Perfluorobutansulfonico (PFBS)": SL_VALUE,
        "Acido Perfluorononanoico (PFNA)": SL_VALUE,
        "Acido Perfluoroesansolfonico (PFHxS)": SL_VALUE,
        "Acido Perfluoroesanoico (PFHxA)": SL_VALUE,
        "Acifo Perfluorobutanoico (PFBA)": SL_VALUE,
        "Acido Perfluoropentanoico (PFPeA)": SL_VALUE,
        "Acido Perfluoroeptanoico (PFHpA)": SL_VALUE,
        "Acido Perfluorodecanoico (PFDA)": SL_VALUE,
        "Acido Perfluoroundecanoico (PFUnDA)": SL_VALUE,
        "Acido Perfluorodecansolfonico (PFDS)": SL_VALUE,
        "Acido Perfluoroeptansolfonico (PFHpS)": SL_VALUE,
        "Acido Perfluorononansolfonico (PFNS)": SL_VALUE,
        "Acido Perfluoropentansolfonico (PFPeS)": SL_VALUE,
    }
    static_values = {
        SOURCE_TEXT: "ARPA Basilicata",
        TYPE: "Sampling location",
        MATRIX: "Surface water",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Italy",
    }
    proj_epsg_id = 23033

    def _preprocess(self):
        self.df = self.raw_df.copy()
        self.df["Date"] = self.df["Date"].apply(clean_date)
        self.df = self.df[~pd.isna(self.df["Date"])]

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False


def clean_date(date_str):
    if pd.isna(date_str):
        return date_str
    elements = str(date_str).split("/")
    if len(elements) == 3:
        d, m, y = elements[0], elements[1], elements[2]
        if len(d) == 1:
            d = f"0{d}"
        if len(m) == 1:
            m = f"0{m}"
        if len(y) == 2:
            y = f"20{y}"
        if not (len(d) == 2 and len(m) == 2 and len(y) == 4):
            print(f"What the fuck: {date_str}")
        else:
            return f"{d}/{m}/{y}"
    else:
        print(f"What the fuck: {date_str}")

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_70(SLNormaliser):
    columns = {
        "Location Code": KEEP,
        "CI_Code": KEEP,
        "Municipality": CITY,
        "Coord_X": COORD_X,
        "Coord_Y": COORD_Y,
        "Date": DATE,
        "Unit": UNIT,
        "Location Name": NAME,
        "Address": KEEP,
        "Monitoring Year": YEAR,
        "Perfluorobutanesulphonic acid (PFBS)": SL_VALUE,
        "Perfluorohexanoic acid (PFHxA)": SL_VALUE,
        "Perfluorooctanoic acid (PFOA)": SL_VALUE,
        "Perfluorooctanesulfonic acid (PFOS)": SL_VALUE,
        "Perfluoropentanoic acid (PFPeA)": SL_VALUE,
    }
    date_format = "%Y-%m-%d"
    static_values = {
        TYPE: "Sampling location",
        MATRIX: "Groundwater",
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        SOURCE_URL: "Press request",
        COUNTRY: "Italy",
        SOURCE_TEXT: "ARPA Piemonte",
    }
    proj_epsg_id = 25832

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

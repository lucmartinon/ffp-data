from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_65(MLNormaliser):
    columns = {
        "Point Code": KEEP,
        "substance": PARAM,
        "Date": DATE,
        "Value": ML_VALUE,
        "Unit": UNIT,
        "Station' Name": NAME,
        "Location": KEEP,
        "Hydric Body Code": KEEP,
        "Hydric Body Name": KEEP,
        "River": KEEP,
        "Hydrographic Basin": KEEP,
        "Municipality": KEEP,
        "Coordinate X WGS84": COORD_X,
        "Coordinate Y WGS84": COORD_Y,
    }
    date_format = "%Y-%m-%d"
    static_values = {
        CATEGORY: "Known",
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Italy",
        SOURCE_TEXT: "ARPA Umbria",
        SOURCE_URL: "Press request",
        MATRIX: "Surface water",
        TYPE: "Sampling location",
    }
    proj_epsg_id = 3065

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

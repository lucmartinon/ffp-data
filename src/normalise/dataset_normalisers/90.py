from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_90(SLNormaliser):
    proj_epsg_id = 3006
    columns = {
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFUnDA": SL_VALUE,
        "PFDoDA": SL_VALUE,
        "PFTriDA": SL_VALUE,
        "PFTeDA": SL_VALUE,
        "PFHxDA": SL_VALUE,
        "PFOcDA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFDS": SL_VALUE,
        "6:2 FTSA": SL_VALUE,
        "FOSA": SL_VALUE,
        "N-MeFOSA": SL_VALUE,
        "N-EtFOSA": SL_VALUE,
        "N-MeFOSE": SL_VALUE,
        "N-EtFOSE": SL_VALUE,
        "FOSAA": SL_VALUE,
        "County": CITY,
        "Latitude": COORD_Y,
        "Longitude": COORD_X,
        "ID": NAME,
        "ID.1": DROP,
        "N-MeFOSAA": SL_VALUE,
        "N-EtFOSAA": SL_VALUE,
        "∑PFCAs": DROP,
        "∑PFSAs": DROP,
        "∑PFSAs.1": DROP,
        "∑11PFAS": DROP,
        "Σ:PFASs": DROP,
        "Type of water": MATRIX,
        "Drinking water source": KEEP,
        "Sampling depth (m)": KEEP,
    }
    static_values = {
        SOURCE_TYPE: "Scientific article",
        COUNTRY: "Sweden",
        TYPE: "Sampling location",
        SOURCE_TEXT: "Gobelius 2018",
        CATEGORY: "Known",
        SOURCE_URL: "https://doi.org/10.1021/acs.est.7b05718",
        YEAR: 2015,
    }

    replace_map = {
        MATRIX: {
            "groundwater": Matrix.GROUNDWATER,
            "surface water": Matrix.SURFACE_WATER,
            "leachate": Matrix.LEACHATE,
            "ditch, outlet STP": Matrix.WASTEWATER,
            "STP effluent": Matrix.WASTEWATER,
            "ditch (surface water)": Matrix.SURFACE_WATER,
            "other, leachate": Matrix.LEACHATE,
            "recipient water (surface water)": Matrix.SURFACE_WATER,
            "storm water, surface water": Matrix.SURFACE_WATER,
            "surface water, coast": Matrix.SURFACE_WATER,
            "Fire dam, surface water": Matrix.SURFACE_WATER,
        }
    }

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

    def _preprocess(self):
        # for ID 20 and 21, coord x and coord y seems to have been inversed
        self.df = self.raw_df.copy()
        for i in self.df[self.df["ID"].isin(["20", "21"])].index:
            print(i)
            self.df.loc[i, ["Latitude", "Longitude"]] = self.df.loc[
                i, ["Longitude", "Latitude"]
            ].values

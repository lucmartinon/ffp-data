from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_68(SLNormaliser):
    columns = {
        "Data": DATE,
        "Codice Stazione": KEEP,
        "Nome Stazione": NAME,
        "Codice Corpo idrico": KEEP,
        "Nome Corpo idrico": KEEP,
        "X (ED50)": COORD_X,
        "Y (ED50)": COORD_Y,
        "757124-72-4": SL_VALUE,
        "29420-49-3": SL_VALUE,
        "39108-34-4": SL_VALUE,
        "1190931-27-1": SL_VALUE,
        "13252-13-6": SL_VALUE,
        "375-22-4": SL_VALUE,
        "375-73-5": SL_VALUE,
        "335-76-2": SL_VALUE,
        "307-55-1": SL_VALUE,
        "375-85-9": SL_VALUE,
        "375-92-8": SL_VALUE,
        "307-24-4": SL_VALUE,
        "355-46-4": SL_VALUE,
        "375-95-1": SL_VALUE,
        "335-67-1 linear": SL_VALUE,
        "335-67-1 branched": SL_VALUE,
        "1763-23-1 branched": SL_VALUE,
        "1763-23-1 linear": SL_VALUE,
        "2706-90-3": SL_VALUE,
        "2058-94-8": SL_VALUE,
    }
    date_format = "%Y-%m-%d"
    static_values = {
        TYPE: "Sampling location",
        SOURCE_URL: "Press request",
        SOURCE_TEXT: "ARPA Valle d'Aosta",
        COUNTRY: "Italy",
        MATRIX: "Surface water",
        SOURCE_TYPE: "Authorities",
        CATEGORY: "Known",
    }
    proj_epsg_id = 23032

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_78(SLNormaliser):
    columns = {
        "PFPeA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFDA": SL_VALUE,
        "PFUdA": SL_VALUE,
        "PFDoA": SL_VALUE,
        "PFTrDA": SL_VALUE,
        "PFTeDA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFPeS": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFHpS": SL_VALUE,
        "PFOS": SL_VALUE,
        "PFNS": SL_VALUE,
        "PFDS": SL_VALUE,
        "lat": LAT,
        "lon": LON,
        " Sampling location": NAME,
    }
    static_values = {
        COUNTRY: "Latvia",
        SOURCE_TYPE: "Scientific article",
        CATEGORY: "Known",
        YEAR: "2021",
        SOURCE_TEXT: "Zacs 2020",
        TYPE: "Sampling location",
        MATRIX: "Wastewater",
        SOURCE_URL: "https://doi.org/10.1016/j.dib.2022.108228",
    }

    @staticmethod
    def _parse_value(val: str):
        if val == "<LOQ":
            return None, None
        if val.startswith("<"):
            return float(val[1:]), True
        return float(val), False

from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_111(SLNormaliser):
    columns = {
        "PFBA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFNA": SL_VALUE,
        "PFBS": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFOS": SL_VALUE,
        "Sample Name": CITY,
        "Gen-X": SL_VALUE,
        "62FTS": SL_VALUE,
        "∑PFAS": DROP,
    }
    static_values = {
        SOURCE_TYPE: "Press inquiry",
        YEAR: "2021",
        MATRIX: "Drinking water",
        TYPE: "Sampling location",
        COUNTRY: "Czechia",
        NAME: "Sampling location",
        SOURCE_TEXT: "Ústav pro životní prostředí Univerzity Karlovy",
        SOURCE_URL: "https://www.natur.cuni.cz/fakulta/zivotni-prostredi",
        CATEGORY: "Known",
    }

    @staticmethod
    def _parse_value(val: str):
        # val = val.replace(',', '.')
        if val.startswith("<"):
            return float(val[1:].strip()), True
        return float(val), False

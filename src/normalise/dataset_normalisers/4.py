from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_4(SLNormaliser):
    static_values = {CATEGORY: "Presumptive", SOURCE_TYPE: "OSINT"}
    columns = {
        "category": CATEGORY,
        "lat": LAT,
        "lon": LON,
        "type": TYPE,
        "name": NAME,
        "city": CITY,
        "country": COUNTRY,
        "sector": SECTOR,
        "class": KEEP,
        "url": KEEP,
        "comment": KEEP,
    }

    @staticmethod
    def _parse_value(val: str):
        return float(val), False

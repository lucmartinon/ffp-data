from src.constants.columns import *
from src.constants.value_constants import Matrix
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_108(SLNormaliser):
    columns = {
        "name": NAME,
        "PFBuS": SL_VALUE,
        "PFHxA": SL_VALUE,
        "PFOA": SL_VALUE,
        "PFPeA": SL_VALUE,
        "PFHpA": SL_VALUE,
        "PFHxS": SL_VALUE,
        "PFOS": SL_VALUE,
        "lat": LAT,
        "lon": LON,
        "matrix": MATRIX
    }
    static_values = {
        SOURCE_TEXT: "Thomaidis 2020",
        COUNTRY: "Greece",
        CATEGORY: "Known",
        TYPE: "Waste management site",
        YEAR: "2020",
        SOURCE_TYPE: "Scientific article"
    }

    replace_map = {
        MATRIX: {
            "Waste water": Matrix.WASTEWATER,
            "Surface water": Matrix.SURFACE_WATER
        }
    }

    @staticmethod
    def _parse_value(val: str):
        # val = val.replace(',', '.')
        if val.startswith('<'):
            return float(val[1:].strip()), True
        return float(val), False

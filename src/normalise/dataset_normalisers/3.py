from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_3(SLNormaliser):
    columns = {
        "name": NAME,
        "lat": LAT,
        "lon": LON,
        "city": CITY,
        "country": COUNTRY,
        "type": TYPE,
        "id": KEEP,
        "ident": DROP,
        "airport_type": DROP,
        "elevation_ft": DROP,
        "continent": DROP,
        "iso_country": DROP,
        "iso_region": DROP,
        "scheduled_service": DROP,
        "gps_code": DROP,
        "iata_code": DROP,
        "local_code": DROP,
        "home_link": DROP,
        "wikipedia_link": DROP,
        "keywords": KEEP,
    }
    static_values = {
        "category": "Presumptive",
        "source_url": "https://ourairports.com/",
        "source_text": "OurAirports",
        "source_type": "OSINT",
    }

    @staticmethod
    def _parse_value(val: str):
        try:
            return float(val)
        except ValueError:
            return None

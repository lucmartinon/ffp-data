from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_61(MLNormaliser):
    columns = {
        "provincia": KEEP,
        "comune": CITY,
        "anno": YEAR,
        "cas": PARAM,
        "um": UNIT,
        "nomecorpoidrico": NAME,
        "codicecorpoidrico": KEEP,
        "codicepunto": KEEP,
        "profondita": KEEP,
        "dataen": DATE,
        "parametro": DROP,
        "risultato": ML_VALUE,
        "lat_wgs84": LAT,
        "lon_wgs84": LON,
        "matrice": KEEP,
    }
    date_format = "ISO8601"
    static_values = {
        COUNTRY: "Italy",
        SOURCE_TYPE: "Authorities",
        MATRIX: "Surface water",
        SOURCE_TEXT: "ARPA Friuli Venezia Giulia",
        TYPE: "Sampling location",
        CATEGORY: "Known",
        SOURCE_URL: "https://www.dati.friuliveneziagiulia.it/Ambiente/Acqua-Acque-di-classificazione-Superficiali-intern/q8d6-8b5n ",
    }

    @staticmethod
    def _parse_value(val: str):
        if val.startswith("<"):
            return float(val[1:]), True
        else:
            return float(val), False

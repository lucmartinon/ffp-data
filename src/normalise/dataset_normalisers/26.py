from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_26(MLNormaliser):
    columns = {
        "name": NAME,
        "year": YEAR,
        "Parameter": PARAM,
        "Method": KEEP,
        "Result": ML_VALUE,
        "Site type": KEEP,
        "ETRS-TM35FIN y": KEEP,
        "ETRS-TM35FIN x": KEEP,
        "lon": LON,
        "lat": LAT,
        "Lab": KEEP,
        "Sampling date": DATE,
        "Sample ID": DROP,
        "Species": KEEP,
        "Precise matrix": KEEP,
        "µnit": UNIT,
    }
    date_format = "%m/%d/%Y"
    static_values = {
        SOURCE_URL: "https://www.syke.fi",
        SOURCE_TEXT: "Finnish Environment Institute",
        TYPE: "Sampling location",
        COUNTRY: "Finland",
        CATEGORY: "Known",
        MATRIX: "Biota",
        SOURCE_TYPE: "Authorities",
    }

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("L"):
            return float(val[1:]), True
        return float(val), False

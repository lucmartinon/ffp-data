from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_59(MLNormaliser):
    columns = {
        "Drainage basin": KEEP,
        "Province": KEEP,
        "Municipality": KEEP,
        "Date": DATE,
        "Lake": NAME,
        "Water Body": KEEP,
        "COORD X": COORD_X,
        "COORD Y": COORD_Y,
        "Sampling spot cose": KEEP,
        "Notes": DROP,
        "Depth": DROP,
        "Substance": DROP,
        "CAS": PARAM,
        "UM": UNIT,
        "Concentration": ML_VALUE,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        CATEGORY: "Known",
        SOURCE_TEXT: "ARPA Lombardia",
        SOURCE_TYPE: "Authorities",
        TYPE: "Sampling location",
        COUNTRY: "Italy",
        MATRIX: "Surface water",
    }

    # UTM 32N
    proj_epsg_id = 25832

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False

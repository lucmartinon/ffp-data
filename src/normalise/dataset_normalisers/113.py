from src.constants.columns import *
from src.normalise.sl_normaliser import SLNormaliser


class Normaliser_113(SLNormaliser):
    columns = {
        "PFOS": SL_VALUE,
        "New_Ech_anal": KEEP,
        "X_Lambert": COORD_X,
        "Y_Lambert": COORD_Y,
        "Origine_echantillon": KEEP,
        "Date_de_prelevement": DATE,
        "Masse_d_eau": KEEP,
        "Cours_d_eau": NAME,
        "Code_ORI": KEEP,
        "SPW": DROP,
        "Fraction": DROP,
    }
    date_format = "%d/%m/%Y"
    static_values = {
        SOURCE_TYPE: "Authorities",
        COUNTRY: "Belgium",
        TYPE: "Sampling location",
        SOURCE_TEXT: "Service public de Wallonie (SPW)",
        CATEGORY: "Known",
        MATRIX: "Sediment",
    }
    proj_epsg_id = 31370

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            return float(val[1:].strip()), True
        return float(val), False

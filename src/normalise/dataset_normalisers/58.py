from src.constants.columns import *
from src.normalise.ml_normaliser import MLNormaliser


class Normaliser_58(MLNormaliser):
    columns = {
        "Drainage basin": KEEP,
        "River": NAME,
        "Province": KEEP,
        "Municipality": KEEP,
        "COORD X": COORD_X,
        "COORD Y": COORD_Y,
        "Station Code": KEEP,
        "Date": DATE,
        "CAS": PARAM,
        "UM": UNIT,
        "Concentration": ML_VALUE,
        "Water body": KEEP,
        "Substance": DROP,
    }
    date_format = "mixed"
    static_values = {
        SOURCE_URL: "https://www.arpalombardia.it/Pages/Acque-Superficiali/Rapporti-Annuali.aspx ",
        SOURCE_TYPE: "Authorities",
        MATRIX: "Surface water",
        SOURCE_TEXT: "ARPA Lombardia",
        CATEGORY: "Known",
        TYPE: "Sampling location",
        COUNTRY: "Italy",
    }
    proj_epsg_id = 25832

    @staticmethod
    def _parse_value(val: str):
        val = val.replace(",", ".")
        if val.startswith("<"):
            val = val.replace("<", "")
            return float(val), True
        else:
            return float(val), False

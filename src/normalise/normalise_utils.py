import pandas as pd

from src.constants.csv import SUBSTANCE_SYNONYMS_FP, SUBSTANCES_FP
from src.constants.exceptions import FppDataException

substance_synonyms = pd.read_csv(SUBSTANCE_SYNONYMS_FP)
substance_synonyms = substance_synonyms.set_index("synonym", verify_integrity=True)

substances = pd.read_csv(SUBSTANCES_FP)

NGL = "ng/l"
NGKG = "ng/kg"
PGM3 = "pg/m3"

units = {
    # PER MASS OF DRY MATTER
    "mg/kg ts": {"multiplier": 1000000, "final_unit": NGKG},
    "mg/kg": {"multiplier": 1000000, "final_unit": NGKG},
    "mg/l": {"multiplier": 1000000, "final_unit": NGL},
    "mg/kg ds": {"multiplier": 1000000, "final_unit": NGKG},
    "µg/kg ts": {"multiplier": 1000, "final_unit": NGKG},
    "µg/kg": {"multiplier": 1000, "final_unit": NGKG},
    "μg/kg ds": {"multiplier": 1000, "final_unit": NGKG},
    "µg/(kg ms)": {"multiplier": 1000, "final_unit": NGKG},
    "nanogram per gram dryweight": {"multiplier": 1000, "final_unit": NGKG},
    "µg/kg fs": {"multiplier": 1000, "final_unit": NGKG},
    "µg/kg ds": {"multiplier": 1000, "final_unit": NGKG},
    "ng/kg ds": {"multiplier": 1, "final_unit": NGKG},
    "µg sn/kg ds": {"multiplier": 1000, "final_unit": NGKG},
    "µg/kg dw": {"multiplier": 1000, "final_unit": NGKG},
    # TODO what to do with this  poids frais:(
    "µg/kg peso húmido": {"multiplier": 1000, "final_unit": NGKG},
    "µg/(kg poids frais)": {"multiplier": 1000, "final_unit": NGKG},
    "µg/kg vv": {"multiplier": 1000, "final_unit": NGKG},
    "ng/g ww": {"multiplier": 1000, "final_unit": NGKG},
    "ng/g": {"multiplier": 1000, "final_unit": NGKG},
    # freshweight
    "µg/kg fw": {"multiplier": 1000, "final_unit": NGKG},
    # for this one not sure what ng means, I think it means fresh weight since they (DOV, netherland) use ds elsewhere
    "µg/kg ng": {"multiplier": 1000, "final_unit": NGKG},
    # PER VOLUME OF LIQUID
    "μg/l": {"multiplier": 1000, "final_unit": NGL},
    "µg/l": {"multiplier": 1000, "final_unit": NGL},
    "ug/l": {"multiplier": 1000, "final_unit": NGL},
    "micrograms per litre": {"multiplier": 1000, "final_unit": NGL},
    "microgram per kilogram": {"multiplier": 1000, "final_unit": NGL},
    "microg/l": {"multiplier": 1000, "final_unit": NGL},
    "microgramme par litre": {"multiplier": 1000, "final_unit": NGL},
    "microg_l": {"multiplier": 1000, "final_unit": NGL},
    "ng/l": {"multiplier": 1, "final_unit": NGL},
    "nanogram per liter": {"multiplier": 1, "final_unit": NGL},
    "nanogram per litre": {"multiplier": 1, "final_unit": NGL},
    "nanogramme par litre": {"multiplier": 1, "final_unit": NGL},
    "ngl": {"multiplier": 1, "final_unit": NGL},
    "ng_l": {"multiplier": 1, "final_unit": NGL},
    "picogram per liter": {"multiplier": 0.001, "final_unit": NGL},
    # PER VOLUME OF GAS
    # TODO non mais lol
    "picogram per cubic meter": {"multiplier": 1, "final_unit": PGM3},
    "ng/m³": {"multiplier": 1000, "final_unit": PGM3},
}


def get_multiplier_from_unit(unit: str):
    unit = unit.lower()
    if unit in units:
        return units[unit]["multiplier"], units[unit]["final_unit"]
    else:
        raise FppDataException(f"Unknown unit: {unit}")

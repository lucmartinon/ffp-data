import datetime
import logging
import os

import markdown
import pandas as pd
from git import Repo
from itables import to_html_datatable

from src.constants.csv import PUBLIC, STATS_FP, NORMALISERS, NORMALISED
from src.extract.extract import get_extractor
from src.merge.export_utils import get_con_with_final_data
from src.normalise.get_normaliser import get_normaliser
from src.normalise.old_normaliser import OldNormaliser

CSS_LINK = '<link rel="stylesheet" href="global.css">'


def single_number(query):
    con = get_con_with_final_data()
    return con.sql(query).fetchone()[0]


def html_table(query, total=False, index=None):
    con = get_con_with_final_data()
    df: pd.DataFrame = con.sql(query).to_df()
    df = df.fillna("")

    if index:
        df.set_index(index, inplace=True)
    if total:
        df.loc["Total"] = df.sum(numeric_only=True)
        df.reset_index(inplace=True)

    return df.to_html()


def generate_table(query, table_name):
    con = get_con_with_final_data()
    df = con.sql(query).to_df()
    df = df.fillna("")

    with open(os.path.join(PUBLIC, f"{table_name}.html"), "w") as f:
        f.write(
            CSS_LINK + to_html_datatable(df, style="table-layout:auto;width:950px;")
        )
    df.to_csv(os.path.join(PUBLIC, f"{table_name}.csv"), index=False)


def generate_stats():
    generate_datasets_infos()
    reprocessed = single_number(
        "select count(distinct dataset_id) from final where not dataset_name like '[OLD]%'"
    )

    md_content = f"""
Number of reprocessed datasets: {reprocessed} / 128 ({(100 * reprocessed / 128):.2f} %):

The full dataset contains {single_number("select count(*) from final")} lines:


{html_table("select Category as Category, count(*) as Rows from final group by 1 order by 2 desc")}

### Known
We have {single_number("select count(*) from final where category = 'Known'")} rows with concentrations.
Rows without values are sampling where the values are below the detection or quantification limit.
Rows without geo are cases where we don't have the exact lat / lon, only the city.
PFAS indicates the number of distinct substances that have been tested in each dataset.

<iframe src="datasets.html" height=500px width=100% frameBorder="0"></iframe>
    """

    stats_html_content = f"{CSS_LINK}{markdown.markdown(md_content)}"
    with open(STATS_FP, "w") as f:
        f.write(stats_html_content)

    logging.info(f"Generated {STATS_FP}")


def generate_datasets_infos():
    # first, we get the extra info from the file system
    fns = [fn for fn in os.listdir(NORMALISED) if fn.endswith(".csv")]
    datasets = [get_dataset_infos(dataset_id) for dataset_id in sorted([int(_.split("__")[0]) for _ in fns])]
    datasets_df = pd.DataFrame(datasets)

    # then we add some stats about datasets
    query = f"""
        select
            dataset_id as ID,
            dataset_name as Dataset,
            first_added, 
            last_processed, 
            datasets_df.type,
            info,
            mode(category) category,
            mode(source_url) source_url, 
            mode(source_text) source_text, 
            mode(source_type) source_type, 
            count(*) as Rows,
            count(distinct lat || lon || coalesce(city, '')) as Locations,
            round(100 * sum(case when pfas_sum > 0 then 1 else 0 end) / Rows, 2) || '%' as "with values",
            round(100 * sum(case when lat is NULL then 1 else 0 end) / Rows, 2)  || '%' as "without loc",
            sum(case when pfas_sum > 10 then 1 else 0 end)  "above 10 ng/L",
            sum(case when pfas_sum > 100 then 1 else 0 end)  "above 100 ng/L",
            max(json_array_length(pfas_values)) PFAS
        from final 
            join datasets_df using (dataset_id)
        group by 1,2,3,4,5,6 order by 1
    """
    generate_table(query, 'datasets')


def get_dataset_infos(dataset_id):
    res = {'dataset_id': dataset_id}
    normaliser = get_normaliser(dataset_id)
    extractor = get_extractor(dataset_id)

    normaliser_fp = os.path.join(NORMALISERS, f'{dataset_id}.py')
    if os.path.exists(normaliser_fp):
        repo = Repo('.')
        updates = repo.git.log('--follow', '--format=%ad', '--date=short', normaliser_fp).split('\n')
        res['first_added'] = updates[-1]
    else:
        res['first_added'] = '2022-02-07'  # date of publication of the FPP

    if isinstance(normaliser, OldNormaliser) or extractor.has_raw_source_file:
        res['type'] = 'static'
    else:
        res['type'] = 'dynamic'

    res['last_processed'] = datetime.datetime.fromtimestamp(os.path.getmtime(normaliser.normalised_fp)).date()
    if normaliser.__doc__ and normaliser.__doc__ != "":
        res['info'] = normaliser.__doc__

    return res

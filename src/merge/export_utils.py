import duckdb

from src.constants.csv import FULL_PARQUET_FP

con = None


def get_con_with_final_data() -> duckdb.DuckDBPyConnection:
    global con
    """
    Opens an in memory DuckDB connection, loads the final data in it, returns the con.
    :return:
    """
    if con is None:
        con = duckdb.connect()
        con.sql(
            f"""
create or replace table final as (
    select
      COLUMNS(* REPLACE (pfas_values::json AS pfas_values, details::json AS details, year::int AS year, date::date AS date))
    from '{FULL_PARQUET_FP}'
);"""
        )

    return con

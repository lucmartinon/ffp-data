import logging

from src.constants.columns import *
from src.constants.csv import DICTIONARY_FP, MAP_CSV_FP
from src.merge.export_utils import get_con_with_final_data


def export_map_data():
    con = get_con_with_final_data()

    con.sql("Create table dictionary(field VARCHAR, id INT, name VARCHAR)")

    con.sql(
        """
    insert into dictionary
        select distinct 'dataset_name' as field, dataset_id as id, dataset_name asname
        from final
    """
    )

    con.sql(
        """
create or replace table map_data as (
    select distinct on (lat, lon)
        columns(* exclude (details, pfas_values)),
        (pfas_values->>'$[*].cas_id')::json cas_ids,
        (pfas_values->>'$[*].substance')::json substances,
        (pfas_values->>'$[*].value')::json as values,
        (pfas_values->>'$[*].less_than')::json less_thans,
        (pfas_values->>'$[*].unit')::json units
    from final
    where lat is not null and lon is not null
    and (category != 'Known' or pfas_sum is not null)
    -- we select the highest value in the last 3 calendar years, or the most recent value before this.
    -- to do this we order by the least of [date, 2020-01-01], then pfas_sum
    order by least(date, (DATE_PART('year', now()) - 3 || '-01-01' ) :: datetime) desc, pfas_sum desc
)
    """
    )

    for c in STATIC_COLUMNS + [CITY, DATASET_NAME]:
        if c != NAME:
            con.sql(
                f"""
            insert into dictionary
                select '{c}' as field,  row_number() OVER () as id, name
                from (select distinct {c} as name from final where {c} is not null order by 1)
            """
            )

            con.sql(
                f"""
            update map_data set {c} = d.id
            from dictionary d
            where d.field = '{c}' and d.name = {c}
            """
            )
            logging.info(f"map_data: replaced [{c}]")

    con.sql(f"copy (select * from dictionary) to '{DICTIONARY_FP}'")
    # pfas_values as 4 arrays

    con.sql(f"copy (select * from map_data) to '{MAP_CSV_FP}'")
    logging.info(f"Exported map data to {MAP_CSV_FP}")

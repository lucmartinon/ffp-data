import logging
import os

import duckdb
import pandas as pd

from src.constants.csv import FULL_CSV_FP, FULL_PARQUET_FP, NORMALISED
from src.merge.check_dataset import check_df
from src.merge.export_map import export_map_data
from src.merge.stats import generate_stats
from src.normalise.get_normaliser import get_normaliser


def concat_datasets() -> pd.DataFrame:
    fns = [fn for fn in os.listdir(NORMALISED) if fn.endswith(".csv")]
    ids = sorted([int(_.split("__")[0]) for _ in fns])
    dfs = []
    for id in ids:
        normaliser = get_normaliser(id)
        df = pd.read_csv(normaliser.normalised_fp, low_memory=False)
        if check_df(df, normaliser.dataset_id, normaliser.dataset_name):
            dfs.append(df)

    df = pd.concat(dfs)
    return df


def merge():
    df = concat_datasets()
    logging.info("Concatenated in one dataset")
    query = f"""
copy(
    select COLUMNS(*REPLACE(pfas_values::json AS pfas_values, details::json AS details)) 
    from df
) to '{FULL_CSV_FP}'
    """
    duckdb.sql(query)
    logging.info("Exported csv")
    query = f"""
copy(
    select COLUMNS(*REPLACE(pfas_values::json AS pfas_values, details::json AS details)) 
    from df
) to '{FULL_PARQUET_FP}'
    """
    duckdb.sql(query)
    logging.info("Exported parquet")
    generate_stats()
    export_map_data()

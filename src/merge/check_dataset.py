import json
import logging

import pandas as pd
from pandas.api.types import is_numeric_dtype

from src.constants.columns import *
from src.constants.csv import NORMALISED
from src.normalise.get_normaliser import get_normaliser
from src.utils import get_df


def str_is_float(float_str):
    try:
        float(float_str)
        return True
    except ValueError:
        return False


def check_dataset(dataset_id: int):
    logging.info(f"Checking dataset {dataset_id}")
    normaliser = get_normaliser(dataset_id)
    return check_df(
        df=get_df(NORMALISED, dataset_id),
        dataset_id=dataset_id,
        dataset_name=normaliser.dataset_name)


def check_df(df, dataset_id, dataset_name):
    errors = []
    errors.extend(run_tests_df(df, dataset_id))
    if len(errors) > 0:
        logging.info(
            f"Checked [{dataset_id: >3}] [{dataset_name}]:\n\t\t"
            + "\n\t\t".join(errors)
        )
        return False
    else:
        logging.info(
            f"Checked [{dataset_id: >3}] [{dataset_name}]: no problem"
        )
    return True


def run_tests_df(df, dataset_id):
    """
    Run tests on the df and yields errors
    :param dataset_id:
    :param df:
    :return:
    """

    # check that all columns are authorised
    for c in df.columns:
        if c not in FULL_FORMAT_COLUMNS:
            yield f"Unexpected col {c}"

    if DATE in df.columns:
        try:
            pd.to_datetime(df[DATE], format="ISO8601", errors="raise")
        except ValueError as e:
            yield str(e)

    # check mandatory columns:
    for c in MANDATORY_COLUMNS:
        if c not in df.columns:
            logging.debug(f"Missing col {c} in final dataset")
            yield f"Missing col {c}"

    # some columns should be filled for all the rows
    for c in NON_NULL_COLUMNS:
        if c in df:
            c_df = df[(pd.isna(df[c])) | (df[c] == "")]
            error_count = c_df.shape[0]
            if error_count > 0:
                yield f"{error_count} rows have no value for column {c}. Example: row {c_df.index[0] + 2}"

    # some columns should be numeric only
    for c in NUMERICAL_COLUMNS:
        if c in df:
            if not is_numeric_dtype(df[c]):
                non_null = df[~pd.isnull(df[c])]
                non_empty = non_null[non_null[c] != ""]
                non_numeric = non_empty[~non_empty[c].map(str_is_float)]
                if non_numeric.shape[0] > 0:
                    yield f"{non_numeric.shape[0]} rows have no non numerical value for column {c}. " f"Example: row {non_numeric.index[0] + 2}:{non_numeric.iloc[0][c]}"
                else:
                    df[c] = pd.to_numeric(df[c], errors="coerce")

    # we check if that pfas_sum is > 0
    if PFAS_SUM in df.columns:
        neg = df[df[PFAS_SUM] < 0]
        if neg.shape[0] > 0:
            yield f"{neg.shape[0]} rows have negative value for column PFAS_SUM. " f"Example: row {neg.index[0] + 2}:{neg.iloc[0][PFAS_SUM]}"

    # Check that all values in list columns are in the list. c is the column, e is the corresponding enum
    for c, enum in LIST_COLUMNS.items():
        if c in df:
            non_null = df[~pd.isnull(df[c])]
            non_empty = non_null[non_null[c] != ""]
            unknown_rows = non_empty[~non_empty[c].isin([item.value for item in enum])]
            if unknown_rows.shape[0] > 0:
                yield f"{unknown_rows.shape[0]} rows have an unknown value for column {c}. " "Please add the value to the list or correct the value. " f"Example: row {unknown_rows.index[0] + 2}: {unknown_rows.iloc[0][c]}"

    # check that the column pfas_values is either empty or filled with coherent values
    pfas_values_errors = set(df[PFAS_VALUES].apply(check_pfas_values))
    for e in pfas_values_errors:
        if e is not None:
            yield e

    if "category" in df.columns:
        known_df = df[df["category"].isin([0, "0", "Known", "known"])]
        if known_df.shape[0] > 0:
            # checking that matrix, year and sum_pfas are filled for all lines
            for c in MANDATORY_COLUMNS_KNOWN:
                if c not in df:
                    yield f'Missing column {c} although the dataset is "Known"'
                else:
                    c_df = known_df[(pd.isna(known_df[c])) | (known_df[c] == "")]
                    if c_df.shape[0] > 0:
                        # todo replace this
                        if str(dataset_id) not in ["33_0", "13"] or c != "pfas_sum":
                            yield f'{c_df.shape[0]} "Known" rows have no value for column {c}. ' f"Example: row {c_df.index[0] + 1}: {c_df.iloc[0][c]}"
                        else:
                            logging.info(
                                "German dataset has known rows without values, it's ok"
                            )

        # For lines that are not "known", PFAS_SUM should be absent or empty, as well as the matrix
        not_known_df = df[~df["category"].isin([0, "0", "Known"])]
        for c in [MATRIX, PFAS_SUM]:
            if c in not_known_df.columns:
                err = not_known_df[~pd.isnull(not_known_df[c])]
                err = err[err[c] != ""]
                if err.shape[0] > 0:
                    yield f"{err.shape[0]} rows are not of category 'Known', but have a value in column {c}" f". Example: row {err.index[0] + 2}: {err.iloc[0][c]}"

    duplicated_check_cols = [LAT, LON]
    for c in [MATRIX, DATE]:
        if c in df.columns:
            duplicated_check_cols.append(c)

    # Europe is defined here as in the range 34° to 72° northern latitude and -25° to 45° eastern longitude.
    if (
            LAT in df.columns
            and LON in df.columns
            and is_numeric_dtype(df[LAT])
            and is_numeric_dtype(df[LON])
    ):
        wrong_lat_lon = df.query("lat > 90 or lat < -90 or lon > 180 or lon < -180")
        if wrong_lat_lon.shape[0] > 0:
            yield f"{wrong_lat_lon.shape[0]} rows have incorrect lat / lon. " f"Example: row {wrong_lat_lon.index[0] + 2}: {wrong_lat_lon.iloc[0][LAT]} {wrong_lat_lon.iloc[0][LON]}"


def check_pfas_values(pfas_values):
    if pfas_values:
        pvs = json.loads(pfas_values)
        for pv in pvs:
            if pv[CAS_ID] is None:
                return "Some PFAS values have no CAS ID"

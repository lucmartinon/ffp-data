import datetime
import glob
import logging
import os.path

import click

import src.add_dataset.create_extractor
from src.add_dataset import add_dataset
from src.add_dataset.guess_epsg_id import guess_epsg_id
from src.add_dataset.index import download_all
from src.constants.csv import NORMALISED, NORMALISERS, EXTRACTORS, RAW, EXCLUDED_FNS, READABLE
from src.extract.extract import get_extractor
from src.merge.check_dataset import check_dataset
from src.merge.export_map import export_map_data
from src.merge.merge import merge
from src.merge.stats import generate_stats
from src.normalise.get_normaliser import get_normaliser
from src.normalise.old_normaliser import OldNormaliser
from src.utils import frame_it_with_errors


@click.group()
def cli():
    pass


@cli.command("add_from_index")
def add_from_index():
    add_dataset.add_from_index()


@cli.command("download_all_from_index")
def download_all_from_index():
    download_all()


@cli.command("create_extractor")
@click.argument("dataset_id", type=click.INT)
def create_extractor(dataset_id):
    if os.path.exists(
            os.path.join(EXTRACTORS, f"{dataset_id}.py")
    ):
        print(f"An extractor already exists for dataset_id {dataset_id}")
    else:
        src.add_dataset.create_extractor.create_extractor(dataset_id)


@cli.command("create_normaliser")
@click.argument("dataset_id", type=click.INT)
def create_normaliser(dataset_id):
    if os.path.exists(
            os.path.join(NORMALISERS, f"{dataset_id}.py")
    ):
        print(f"A normaliser already exists for dataset_id {dataset_id}")
    else:
        src.add_dataset.add_dataset.add_normaliser(dataset_id)


@cli.command("normalise")
@click.argument("dataset_id", type=click.INT)
def normalise(dataset_id):
    logging.info(f"Normalising dataset {dataset_id}")
    normaliser = get_normaliser(dataset_id)
    normaliser.normalise(force=True)

    # we delete the old file if there are some
    pattern = os.path.join(NORMALISED, f"{dataset_id}__*OLD*.csv")
    if len(glob.glob(pattern)) > 0 and not isinstance(normaliser, OldNormaliser):
        fp = glob.glob(pattern)[0]
        os.remove(fp)
        logging.info(f"Removed old normalised file {fp}")
    if check_dataset(dataset_id):
        merge()


@cli.command("extract")
@click.argument("dataset_id", type=click.INT)
def extract(dataset_id):
    logging.info(f"Extract dataset {dataset_id}")
    extractor = get_extractor(dataset_id)
    extractor.extract(force=True)
    pass


@cli.command("check")
@click.argument("dataset_id", type=click.INT)
def check(dataset_id):
    check_dataset(dataset_id)


@cli.command("merge")
def merge_cmd():
    merge()


@cli.command("stats")
def stats():
    generate_stats()


@cli.command("export_map")
def export_map():
    export_map_data()


@cli.command("daily_process")
def daily_process():
    # first we extract all we can extract
    extract_all()

    # then we normalise anithing that can need it
    normalise_all()

    # Finally we merge if needed
    should_merge = False
    for fn in sorted(os.listdir(NORMALISED)):
        if fn not in EXCLUDED_FNS:
            fp = os.path.join(NORMALISED, fn)
            if datetime.datetime.fromtimestamp(os.path.getmtime(fp)).date() == datetime.datetime.now().date():
                should_merge = True

    if should_merge:
        merge()
        # upload to whatever, or write a  change log
    else:
        logging.info("No change, no need to merge")


@frame_it_with_errors
def extract_all():
    for fn in sorted(os.listdir(RAW)):
        if fn not in EXCLUDED_FNS:
            dataset_id = int(fn.split('__')[0])
            try:
                extractor = get_extractor(dataset_id)
                extractor.extract(force=False)
            except Exception as e:
                msg = f"Error during extracting [{dataset_id}]: {e}"
                logging.error(msg)
                yield msg[:98]


@frame_it_with_errors
def normalise_all():
    for fn in sorted(os.listdir(READABLE)):
        if fn not in EXCLUDED_FNS:
            dataset_id = int(fn.split('__')[0])
            try:
                normaliser = get_normaliser(dataset_id)
                normaliser.normalise(force=False)
            except Exception as e:
                msg = f"Error during normalising [{dataset_id}]: {e}"
                logging.error(msg)
                yield msg[:98]


@cli.command("epsg")
@click.argument("dataset_id", type=click.INT, default=0)
def epsg(dataset_id):
    guess_epsg_id(dataset_id)


if __name__ == "__main__":
    cli()

import os
from collections import Counter

from src.constants.csv import EXTRACTORS, NORMALISERS, NORMALISED, RAW, READABLE, EXCLUDED_FNS
from src.extract.extract import Extractor
from src.normalise.get_normaliser import get_normaliser
from src.normalise.normaliser import Normaliser
from src.normalise.old_normaliser import OldNormaliser


def test_structure():
    # for all files in RAW, NORMALISED, READABLE, the id should be unique in the folder
    for folder in [RAW, READABLE, NORMALISED]:
        ids = []
        for fn in os.listdir(folder):
            if fn not in EXCLUDED_FNS:
                if folder in [READABLE, NORMALISED]:
                    assert fn.endswith('.csv')
                id = int(fn.split('__')[0])
                ids.append(id)
        repeated_ids = [k for k, v in Counter(ids).items() if v > 1]
        assert len(repeated_ids) == 0, f'Multiple files for ids {repeated_ids} in folder {folder}'

    # check that in the folder str/extract/extractors there are only files named x.py, where x is a number.
    # each of these files should contain an Extractor class with the correct name
    for fn in os.listdir(EXTRACTORS):
        if fn not in EXCLUDED_FNS:
            id_str = fn[:-3]
            assert id_str.isdecimal(), f"Unexpected filename: {fn}"
            id = int(id_str)
            mod = __import__(
                f"{EXTRACTORS.replace(os.path.sep, '.')}.{id}",
                fromlist=[f"Extractor_{id}"]
            )
            extractor = getattr(mod, f"Extractor_{id}")(id)
            assert isinstance(extractor, Extractor)

    # same for normalisers
    for fn in os.listdir(NORMALISERS):
        if fn not in EXCLUDED_FNS:
            id_str = fn[:-3]
            assert id_str.isdecimal(), f"Unexpected filename: {fn}"
            id = int(id_str)
            mod = __import__(
                f"{NORMALISERS.replace(os.path.sep, '.')}.{id}",
                fromlist=[f"Normaliser_{id}"]
            )
            normaliser = getattr(mod, f"Normaliser_{id}")()
            assert isinstance(normaliser, Normaliser)

    # all files in data/normalised should be prefixed with id__ where id is a number,
    # and this number should correspond to a normaliser
    for fn in os.listdir(NORMALISED):
        if fn not in EXCLUDED_FNS:
            id = int(fn.split('__')[0])
            dataset_name = fn.split('__')[1]
            normaliser = get_normaliser(id, return_old_normaliser=True)
            if dataset_name.startswith('[OLD]'):
                assert isinstance(normaliser, OldNormaliser)
            else:
                assert not isinstance(normaliser, OldNormaliser)
                assert isinstance(normaliser, Normaliser)
